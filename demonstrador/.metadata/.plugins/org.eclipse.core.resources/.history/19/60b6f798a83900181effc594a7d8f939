package pt.unl.fct.di.apdc.firstwebapp.resources;

import java.util.Date;
import java.util.List;

import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PropertyProjection;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.appengine.repackaged.com.google.api.client.util.store.DataStore;
import com.google.appengine.repackaged.com.google.datastore.v1.Projection;
import com.google.appengine.repackaged.org.apache.commons.codec.digest.DigestUtils;
import com.google.gson.Gson;

import pt.unl.fct.di.apdc.firstwebapp.util.AcessUser;
import pt.unl.fct.di.apdc.firstwebapp.util.AuthToken;
import pt.unl.fct.di.apdc.firstwebapp.util.LoginData;
import pt.unl.fct.di.apdc.firstwebapp.util.Marker;
import pt.unl.fct.di.apdc.firstwebapp.util.RemoveUser;
import pt.unl.fct.di.apdc.firstwebapp.util.UserInfo;
import pt.unl.fct.di.apdc.firstwebapp.util.Random;

@Path("/user")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class UserInfoResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(LoginResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	public UserInfoResource() {
	} // Nothing to be done here...

	@POST
	@Path("/info")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doInfoRequest(AcessUser acess) {
		LOG.fine("Attempt to login user: " + acess.tokenID);
		LOG.warning("Attempt to login user: " + acess.tokenID);
		Key userKey = KeyFactory.createKey("User", acess.username);
		Query tokensQuery = new Query("UserToken").setAncestor(userKey);
		List<Entity> tokens = datastore.prepare(tokensQuery).asList(FetchOptions.Builder.withDefaults());

		if (tokens.isEmpty() || !tokens.get(0).getProperty("token").equals(acess.tokenID)) {
			LOG.warning("result: " + tokens.get(0).getProperty("token"));
			LOG.warning("given: " + acess.tokenID);
			return Response.status(Status.BAD_REQUEST).build();
		}
		UserInfo info;
		String name;
		String role;
		String cc;
		String nif;
		String cellphone;
		String phone;
		String email;
		String street = "";
		String city = "";
		String zipcode1 = "";
		String zipcode2 = "";
		try {
			Entity user = datastore.get(userKey);
			name = (String) user.getProperty("user_name");
			email = (String) user.getProperty("user_email");
			role = (String) user.getProperty("user_role");
			cc = (String) user.getProperty("user_cc");
			nif = (String) user.getProperty("user_nif");
			cellphone = (String) user.getProperty("user_cellphone");
			phone = (String) user.getProperty("user_phone");
			Query localQuery = new Query("UserLocal").setAncestor(userKey);
			List<Entity> locals = datastore.prepare(localQuery).asList(FetchOptions.Builder.withDefaults());
			if (!locals.isEmpty()) {
				Entity local = locals.get(0);
				street = (String) local.getProperty("local_street");
				city = (String) local.getProperty("local_city");
				zipcode1 = (String) local.getProperty("local_zipcode1");
				zipcode2 = (String) local.getProperty("local_zipcode2");
			}
			info = new UserInfo(acess.username, role, cc, nif, cellphone, phone, name, email, street, city, zipcode1,
					zipcode2);
			return Response.ok(g.toJson(info)).build();
		} catch (EntityNotFoundException e) {
			return Response.status(Status.FORBIDDEN).build();
		}

	}

	@POST
	@Path("/listusers")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doListUsers(AcessUser acess) {
		LOG.warning("Attempt to list users: " + acess.tokenID);
		Key userKey = KeyFactory.createKey("User", acess.username);
		Query tokensQuery = new Query("UserToken").setAncestor(userKey);
		List<Entity> tokens = datastore.prepare(tokensQuery).asList(FetchOptions.Builder.withDefaults());

		if (tokens.isEmpty() || !tokens.get(0).getProperty("token").equals(acess.tokenID)) {
			LOG.warning("result: " + tokens.get(0).getProperty("token"));
			return Response.status(Status.FORBIDDEN).build();
		}

		Query usersQuery = new Query("User");
		List<Entity> users = datastore.prepare(usersQuery).asList(FetchOptions.Builder.withDefaults());
		LOG.warning("Attempt to list users: " + users);
		return Response.ok(g.toJson(users)).build();

	}

	@POST
	@Path("/removeuser")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doRemoveUser(RemoveUser acess) {
		LOG.warning("Attempt to list users: " + acess.token.tokenID);
		Key userKey = KeyFactory.createKey("User", acess.token.username);
		Query tokensQuery = new Query("UserToken").setAncestor(userKey);
		List<Entity> tokens = datastore.prepare(tokensQuery).asList(FetchOptions.Builder.withDefaults());

		if (tokens.isEmpty() || !tokens.get(0).getProperty("token").equals(acess.token.tokenID)) {
			LOG.warning("result: " + tokens.get(0).getProperty("token"));
			return Response.status(Status.FORBIDDEN).build();
		}
		Transaction txn = datastore.beginTransaction();
		Key usertoremoveKey = KeyFactory.createKey("User", acess.userToRemove);
		try {
			Entity user = datastore.get(usertoremoveKey);
			datastore.delete(txn, user.getKey());
			txn.commit();
			LOG.info("User '" + acess.userToRemove + "' removed sucessfully.");
			return Response.ok().build();

		} catch (

		EntityNotFoundException e) {
			// Username does not exist
			LOG.warning("Failed remove attempt for username: " + acess.userToRemove);
			return Response.status(Status.FORBIDDEN).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		}

	}

	@POST
	@Path("/addMarker")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doAddMarkers(Marker marker) {
		LOG.warning("Attempt to addMarker users: " + marker.username);
		Transaction txn = datastore.beginTransaction();
		Key userKey = KeyFactory.createKey("User", marker.username);
		try {
			Entity eMarker = new Entity("UserMarker", userKey);
			eMarker.setProperty("latitude", marker.latitude);
			eMarker.setProperty("longitude", marker.longitude);
			eMarker.setProperty("content", marker.content);
			eMarker.setProperty("id", Random.key64());
			datastore.put(txn, eMarker);
			txn.commit();
			Query markersQuery = new Query("User_Marker").setAncestor(userKey);
			List<Entity> markers = datastore.prepare(markersQuery).asList(FetchOptions.Builder.withDefaults());
			return Response.ok(g.toJson(markers)).build();
			// Username does not exist
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		}

	}

	@POST
	@Path("/changeContent")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doChangeMarker(Marker marker) {
		LOG.warning("Attempt to addMarker users: " + marker.username);
		Transaction txn = datastore.beginTransaction();
		Key userKey = KeyFactory.createKey("User", marker.username);
		try {
			Filter propertyFilter = new FilterPredicate("id", FilterOperator.EQUAL,
					marker.id);
			Query ctrQuery = new Query("UserMarker").setAncestor(userKey)
					.setFilter(propertyFilter);
			Query markersQuery = new Query("User_Marker").setAncestor(userKey);
			List<Entity> markers = datastore.prepare(markersQuery).asList(FetchOptions.Builder.withDefaults());
			Entity eMarkers = null;
			if (!markers.isEmpty()) {
				eMarkers = markers.get(0);
				LOG.info("Marker" + eMarkers);
				eMarkers.setProperty("content", marker.content);
			}
			txn.commit();
			
			return Response.ok(g.toJson(markers)).build();
			// Username does not exist
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		}

	}
	@GET
	@Path("/markers")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doGetMarkers() {
		LOG.warning("Attempt to getMarker");
		Query markersQuery = new Query("UserMarker");
		List<Entity> markers = datastore.prepare(markersQuery).asList(FetchOptions.Builder.withDefaults());
		return Response.ok(g.toJson(markers)).build();
		// Username does not exist

	}

}
