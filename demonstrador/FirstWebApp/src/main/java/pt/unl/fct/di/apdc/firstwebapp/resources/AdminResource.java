package pt.unl.fct.di.apdc.firstwebapp.resources;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.gson.Gson;

import pt.unl.fct.di.apdc.firstwebapp.resources.BasePermissionsResource.PermissionEnum;
import pt.unl.fct.di.apdc.firstwebapp.util.AccessUser;
import pt.unl.fct.di.apdc.firstwebapp.util.Random;
import pt.unl.fct.di.apdc.firstwebapp.util.Utils;
import pt.unl.fct.di.apdc.firstwebapp.util.news.NewsData;
import pt.unl.fct.di.apdc.firstwebapp.util.news.NewsEdit;
import pt.unl.fct.di.apdc.firstwebapp.util.news.NewsInfo;
import pt.unl.fct.di.apdc.firstwebapp.util.user.UserInfo;

@Path("/admin")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class AdminResource {

	private static final String EVENT_IS_PUBLIC = "event_isPublic";

	private static final String USER_IS_ADMIN = "admin";
	private static final String USER_ROLE = "user_role";

	private static final String NEWS_IMGTYPE = "news_imgtype";
	private static final String NEWS_CREATION_TIME = "news_creation_time";
	private static final String NEWS_TEXT = "news_text";
	private static final String NEWS_SUBTITLE = "news_subtitle";
	private static final String NEWS_TITLE = "news_title";
	private static final String NEWS_ID = "news_id";
	private static final String NEWS_USERNAME = "news_username";

	private static final String EVENT_QUERY = "Event";
	private static final String NEWS_QUERY = "News";
	private static final String USER_QUERY = "User";

	private static final String INVALID_ID = "Invalid Id";
	private static final String USER_404 = "User not exist.";

	private static final Logger LOG = Logger.getLogger(AdminResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	public AdminResource() {
	} // Nothing to be done here...

	@POST
	@Path("/news/add")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response addNews(NewsData data) {
		LOG.fine("Attempt to register news by user: " + data.acess.username);
		if (!data.validRegistration()) {
			return Response.status(Status.BAD_REQUEST).entity("Missing or wrong parameter.").build();
		}
		Key userKey = KeyFactory.createKey(USER_QUERY, data.acess.username);// temos que verificar se o user e admin ?
		if (!Utils.checkToken(data.acess, userKey) || !PermissionsResource
				.hasPermission(PermissionEnum.PERMISSION_NEWS_ADD, data.acess.username)) {
			return Response.status(Status.BAD_REQUEST).build();
		}

		Transaction txn = datastore.beginTransaction();
		try {
			// If the entity does not exist an Exception is thrown. Otherwise,

			Entity user = datastore.get(userKey);
			Entity news = new Entity(NEWS_QUERY, user.getKey());
			String id = Random.key64();
			news.setProperty(NEWS_ID, id);
			news.setProperty(NEWS_TITLE, data.title);
			news.setProperty(NEWS_SUBTITLE, data.subtitle);
			news.setProperty(NEWS_TEXT, data.text);
			news.setUnindexedProperty(NEWS_CREATION_TIME, Utils.getCreationTime());

			List<Entity> toPut = Arrays.asList(news);
			datastore.put(txn, toPut);
			LOG.info("news by admin " + data.acess.username + " registered with id " + id);
			txn.commit();
			return Response.ok(id).build();

		} catch (EntityNotFoundException e) {
			txn.rollback();
			return Response.status(Status.BAD_REQUEST).entity(USER_404).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}

	}

	@DELETE
	@Path("/{news}/remove")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doRemoveNews(@PathParam("news") String newsToRemove, AccessUser acess) {
		LOG.warning("ROLE");
		Key userKey = KeyFactory.createKey(USER_QUERY, acess.username);
		if (!Utils.checkToken(acess, userKey) || !PermissionsResource
				.hasPermission(PermissionEnum.PERMISSION_NEWS_DELETE, acess.username)) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		LOG.warning("ROLE");

		Filter propertyFilter = new FilterPredicate(NEWS_ID, FilterOperator.EQUAL, newsToRemove);
		Query reportQuery = new Query(NEWS_QUERY).setFilter(propertyFilter);
		List<Entity> newsQuery = datastore.prepare(reportQuery).asList(FetchOptions.Builder.withDefaults());
		LOG.warning("ROLE1");
		if (newsQuery.isEmpty())
			return Response.status(Status.BAD_REQUEST).entity(INVALID_ID).build();
		LOG.warning("ROLE2");
		Entity news = newsQuery.get(0);
		Transaction txn = datastore.beginTransaction();
		try {
			Entity user = datastore.get(userKey);
			LOG.warning("ROLE" + (String) user.getProperty(USER_ROLE));
			if (((String) user.getProperty(USER_ROLE)).equals(USER_IS_ADMIN)) {

				LOG.info(news.getKey().toString());
				datastore.delete(txn, news.getKey());
				txn.commit();
				return Response.ok().build();
			}
			return Response.status(Status.CONFLICT).build();
		} catch (
		EntityNotFoundException e) {
			// Username does not exist
			LOG.warning("Failed remove attempt for news: " + newsToRemove);
			return Response.status(Status.FORBIDDEN).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		}

	}

	@POST
	@Path("/news/{news}/edit") // TODO: nao edita nada
	@Consumes(MediaType.APPLICATION_JSON)
	public Response infoReport(AccessUser acess, @PathParam("news") String news) {
		Key userKey = KeyFactory.createKey(USER_QUERY, acess.username);
		if (!Utils.checkToken(acess, userKey) || !PermissionsResource
				.hasPermission(PermissionEnum.PERMISSION_NEWS_UPDATE, acess.username)) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		String title;
		String subtitle;
		String username;
		String text;
		String imgType;

		Filter propertyFilter = new FilterPredicate(NEWS_ID, FilterOperator.EQUAL, news);
		Query reportQuery = new Query("News").setAncestor(userKey).setFilter(propertyFilter);
		List<Entity> newsQuery = datastore.prepare(reportQuery).asList(FetchOptions.Builder.withDefaults());
		if (newsQuery.isEmpty())
			return Response.status(Status.BAD_REQUEST).entity(INVALID_ID).build();
		Entity newsToEdit = newsQuery.get(0);

		username = (String) newsToEdit.getProperty(NEWS_USERNAME);
		title = (String) newsToEdit.getProperty(NEWS_TITLE);
		subtitle = (String) newsToEdit.getProperty(NEWS_SUBTITLE);
		text = (String) newsToEdit.getProperty(NEWS_TEXT);
		imgType = (String) newsToEdit.getProperty(NEWS_IMGTYPE);

		NewsInfo info = new NewsInfo(title, subtitle, text, username, imgType);
		return Response.ok(g.toJson(info)).build();
	}

	@POST
	@Path("/news/{news}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response infoNews(AccessUser acess, @PathParam("news") String news) {
		Key userKey = KeyFactory.createKey(USER_QUERY, acess.username);
		if (!Utils.checkToken(acess, userKey)) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		String title;
		String subtitle;
		String username;
		String text;
		String imgType;

		Filter propertyFilter = new FilterPredicate(NEWS_ID, FilterOperator.EQUAL, news);
		Query reportQuery = new Query("News").setAncestor(userKey).setFilter(propertyFilter);
		List<Entity> newsQuery = datastore.prepare(reportQuery).asList(FetchOptions.Builder.withDefaults());
		if (newsQuery.isEmpty())
			return Response.status(Status.BAD_REQUEST).entity(INVALID_ID).build();
		Entity newsToEdit = newsQuery.get(0);
		Entity userParent;
		try {
			userParent = datastore.get(newsToEdit.getParent());
			username = (String) userParent.getKey().getName();
			title = (String) newsToEdit.getProperty(NEWS_TITLE);
			subtitle = (String) newsToEdit.getProperty(NEWS_SUBTITLE);
			text = (String) newsToEdit.getProperty(NEWS_TEXT);
			imgType = (String) newsToEdit.getProperty(NEWS_IMGTYPE);

			NewsInfo info = new NewsInfo(title, subtitle, text, username, imgType);
			return Response.ok(g.toJson(info)).build();
		} catch (EntityNotFoundException e) {
			return Response.status(Status.BAD_REQUEST).build();
		}
	}
	
	//TODO: ??????????????

	@POST
	@Path("/news/{news}/edit/confirmation")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response editNewsConfirmation(NewsEdit edit, @PathParam("news") String newsID) {
		LOG.fine("Attempt to register report by user: " + edit.acess.username);

		Key userKey = KeyFactory.createKey(USER_QUERY, edit.acess.username);
		if (!Utils.checkToken(edit.acess, userKey)) {
			return Response.status(Status.BAD_REQUEST).build();
		}

		Transaction txn = datastore.beginTransaction();

		// If the entity does not exist an Exception is thrown. Otherwise,

		Filter propertyFilter = new FilterPredicate(NEWS_ID, FilterOperator.EQUAL, newsID);
		Query reportQuery = new Query(NEWS_QUERY).setAncestor(userKey).setFilter(propertyFilter);
		List<Entity> newsQuery = datastore.prepare(reportQuery).asList(FetchOptions.Builder.withDefaults());

		if (newsQuery.isEmpty())
			return Response.status(Status.BAD_REQUEST).entity(INVALID_ID).build();
		Entity newsToEdit = newsQuery.get(0);

		if (edit.nonEmptyField(edit.title)) {
			newsToEdit.setProperty(NEWS_TITLE, edit.title);
		}
		if (edit.nonEmptyField(edit.subtitle)) {
			newsToEdit.setProperty(NEWS_SUBTITLE, edit.subtitle);
		}
		if (edit.nonEmptyField(edit.text)) {
			newsToEdit.setProperty(NEWS_TEXT, edit.text);
		}
		newsToEdit.setUnindexedProperty(NEWS_CREATION_TIME, Utils.getCreationTime());

		List<Entity> toPut = Arrays.asList(newsToEdit);
		datastore.put(txn, toPut);
		LOG.info("news edited by admin " + edit.acess.username);
		txn.commit();
		return Response.ok().build();

	}

}
