package pt.unl.fct.di.apdc.firstwebapp.resources;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.TransactionOptions;
import com.google.gson.Gson;

import pt.unl.fct.di.apdc.firstwebapp.util.AccessUser;
import pt.unl.fct.di.apdc.firstwebapp.util.Utils;
import pt.unl.fct.di.apdc.firstwebapp.util.permissions.AddRole;
import pt.unl.fct.di.apdc.firstwebapp.util.permissions.Permission;
import pt.unl.fct.di.apdc.firstwebapp.util.permissions.Permissions;

@Path("/permission/base")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class BasePermissionsResource {

	// STATES
	public static final int NEVER = -1;
	public static final int NOT_HAVE = 0;
	public static final int HAVE = 1;
	public static final int HAVE_ALWAYS = 2;

	private static final String PASSWORD_INIT_ROLES = "letStartRoles!";

	/** PERMISSION TABLES */
	public static final String BASE_PERMISSIONS_KIND = "BasePermissions";

	public static enum PermissionEnum {
		// REPORT
		PERMISSION_REPORT_C0_ADD("permission_report_c0_add"), PERMISSION_REPORT_C0_UPDATE_STATE(
				"permission_report_c0_update_state"), PERMISSION_REPORT_C0_DELETE(
						"permission_report_c0_delete"), PERMISSION_REPORT_C1_ADD(
								"permission_report_c1_add"), PERMISSION_REPORT_C1_UPDATE_STATE(
										"permission_report_c1_update_state"), PERMISSION_REPORT_C1_DELETE(
												"permission_report_c1_delete"), PERMISSION_REPORT_C2_ADD(
														"permission_report_c2_add"), PERMISSION_REPORT_C2_UPDATE_STATE(
																"permission_report_c2_update_state"), PERMISSION_REPORT_C2_DELETE(
																		"permission_report_c2_delete"), PERMISSION_REPORT_CONFIRM(
																				"permission_report_confirm"),

		// EVENT
		PERMISSION_EVENT_ADD("permission_event_add"), PERMISSION_EVENT_VERIFY(
				"permission_event_verify"), PERMISSION_EVENT_COMMENT(
						"permission_event_comment"), PERMISSION_EVENT_LIKE(
								"permission_event_like"), PERMISSION_EVENT_DELETE("permission_event_delete"),

		// NEWS
		PERMISSION_NEWS_ADD("permission_news_add"), PERMISSION_NEWS_DELETE(
				"permission_news_delete"), PERMISSION_NEWS_UPDATE("permission_news_update"),

		// ADMISTRATIVE
		PERMISSION_ADMINISTRATIVE_ADD_ROLE(
				"permission_administrative_add_role"), PERMISSION_ADMINISTRATIVE_UPGRADE_USER(
						"permission_administrative_upgrade_user"), PERMISSION_ADMINISTRATIVE_UPDATE_PERMISSIONS(
								"permission_administrative_update_permissions"), PERMISSION_ADMINISTRATIVE_LIST_USERS(
										"permission_administrative_list_users"), PERMISSION_ADMINISTRATIVE_REMOVE_USERS(
												"permission_administrative_remove_users");

		private final String perm;

		PermissionEnum(String perm) {
			this.perm = perm;
		}

		public String toString() {
			return this.perm;
		}
	}

	/** ROLES TABLES */
	public static final String ROLES_KIND = "Roles";
	public static final String ROLE_LEVEL = "level";

	// STANDARD ROLES
	public static final String USER_ROLE = "user";
	public static final String WORKER_ROLE = "worker";
	public static final String ENTITY_ROLE = "entity";
	public static final String ADMIN_ROLE = "admin";

	// LEVELS
	private static final int USER_LEVEL = 6;
	private static final int WORKER_LEVEL = 4;
	private static final int ENTITY_LEVEL = 2;
	private static final int ADMIN_LEVEL = 0;

	private static final Logger LOG = Logger.getLogger(BasePermissionsResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	public BasePermissionsResource() {
	}

	@POST
	@Path("/initRoles")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response inicialRoles(String pass) {
		LOG.info("initToles: " + pass);
		if (!pass.equals(PASSWORD_INIT_ROLES))
			return Response.status(Status.FORBIDDEN).build();

		Entity admin = new Entity(ROLES_KIND, ADMIN_ROLE);
		try {
			admin = datastore.get(admin.getKey());
		} catch (EntityNotFoundException e) {
			e.printStackTrace();
		}
		admin.setProperty(ROLE_LEVEL, ADMIN_LEVEL);
		Entity entity = new Entity(ROLES_KIND, ENTITY_ROLE);
		try {
			entity = datastore.get(entity.getKey());
		} catch (EntityNotFoundException e) {

		}
		entity.setProperty(ROLE_LEVEL, ENTITY_LEVEL);
		Entity worker = new Entity(ROLES_KIND, WORKER_ROLE);
		try {
			worker = datastore.get(worker.getKey());
		} catch (EntityNotFoundException e) {

		}
		worker.setProperty(ROLE_LEVEL, WORKER_LEVEL);
		Entity user = new Entity(ROLES_KIND, USER_ROLE);
		try {
			user = datastore.get(user.getKey());
		} catch (EntityNotFoundException e) {

		}
		user.setProperty(ROLE_LEVEL, USER_LEVEL);

		Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withXG(true));
		List<Entity> toPut = Arrays.asList(admin, entity, worker, user, initAdminBasePermissions(admin.getKey()),
				initEntityBasePermissions(entity.getKey()), initWorkerBasePermissions(worker.getKey()),
				initUserBasePermissions(user.getKey()));
		datastore.put(txn, toPut);
		txn.commit();
		return Response.ok().build();
	}

	@POST
	@Path("/addRole")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addRole(AddRole newRole) {
		Key userKey = KeyFactory.createKey("User", newRole.acess.username);
		LOG.info("AddRole");
		if (!Utils.checkToken(newRole.acess, userKey) || !PermissionsResource
				.hasPermission(PermissionEnum.PERMISSION_ADMINISTRATIVE_ADD_ROLE, newRole.acess.username))
			return Response.status(Status.BAD_REQUEST).build();

		Transaction txn = datastore.beginTransaction();
		Key newRoleKey = KeyFactory.createKey(ROLES_KIND, newRole.newRole.role);
		try {
			datastore.get(newRoleKey);
			return Response.status(Status.BAD_REQUEST).build();
		} catch (EntityNotFoundException e) {

			Entity newRoleEntity = new Entity(ROLES_KIND, newRole.newRole.role);
			newRoleEntity.setProperty(ROLE_LEVEL, newRole.newRole.level);
			Entity newRoleBasePermission = new Entity(BASE_PERMISSIONS_KIND, newRole.newRole.role, newRoleKey);

			boolean validEntity = false;
			for (int i = 0; i < newRole.permissions.length; i++) {
				Permission current = newRole.permissions[i];
				String permission = current.permission;
				int value = current.value;
				if (Utils.nonEmptyField(permission) && value >= NEVER && value <= HAVE_ALWAYS) {
					try {
						newRoleBasePermission.setProperty(PermissionEnum.valueOf(permission.toUpperCase()).toString(),
								value);
						validEntity = true;
					} catch (Exception perm) {
						e.printStackTrace();
					}
				}

			}
			if (validEntity) {
				datastore.put(txn, newRoleEntity);
				txn.commit();
			} else {
				txn.rollback();
				return Response.status(Status.BAD_REQUEST).entity("permissoes invalidas").build();
			}
			return Response.ok().build();
		}

	}

	@POST
	@Path("/typePerm")

	public Response getTypePermissions(AccessUser acess) {
		if (!Utils.checkToken(acess, KeyFactory.createKey("User", acess.username))) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		return Response.ok(g.toJson(PermissionEnum.values())).build();

	}

	private Entity initAdminBasePermissions(Key adminRoleKey) {
		Entity adminBasePermissions = new Entity(BASE_PERMISSIONS_KIND, ADMIN_ROLE, adminRoleKey);

		adminBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C0_ADD.toString(), HAVE_ALWAYS);
		adminBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C0_DELETE.toString(), HAVE_ALWAYS);
		adminBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C0_UPDATE_STATE.toString(), HAVE_ALWAYS);

		adminBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C1_ADD.toString(), HAVE_ALWAYS);
		adminBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C1_DELETE.toString(), HAVE_ALWAYS);
		adminBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C1_UPDATE_STATE.toString(), HAVE_ALWAYS);

		adminBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C2_ADD.toString(), HAVE_ALWAYS);
		adminBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_CONFIRM.toString(), HAVE_ALWAYS);
		adminBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C2_DELETE.toString(), HAVE_ALWAYS);
		adminBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C2_UPDATE_STATE.toString(), HAVE_ALWAYS);

		adminBasePermissions.setProperty(PermissionEnum.PERMISSION_EVENT_ADD.toString(), HAVE_ALWAYS);
		adminBasePermissions.setProperty(PermissionEnum.PERMISSION_EVENT_LIKE.toString(), HAVE_ALWAYS);
		adminBasePermissions.setProperty(PermissionEnum.PERMISSION_EVENT_COMMENT.toString(), HAVE_ALWAYS);
		adminBasePermissions.setProperty(PermissionEnum.PERMISSION_EVENT_DELETE.toString(), HAVE_ALWAYS);
		adminBasePermissions.setProperty(PermissionEnum.PERMISSION_EVENT_VERIFY.toString(), HAVE_ALWAYS);

		adminBasePermissions.setProperty(PermissionEnum.PERMISSION_NEWS_ADD.toString(), HAVE_ALWAYS);
		adminBasePermissions.setProperty(PermissionEnum.PERMISSION_NEWS_DELETE.toString(), HAVE_ALWAYS);
		adminBasePermissions.setProperty(PermissionEnum.PERMISSION_NEWS_UPDATE.toString(), HAVE_ALWAYS);

		adminBasePermissions.setProperty(PermissionEnum.PERMISSION_ADMINISTRATIVE_ADD_ROLE.toString(), HAVE_ALWAYS);
		adminBasePermissions.setProperty(PermissionEnum.PERMISSION_ADMINISTRATIVE_UPDATE_PERMISSIONS.toString(),
				HAVE_ALWAYS);
		adminBasePermissions.setProperty(PermissionEnum.PERMISSION_ADMINISTRATIVE_UPGRADE_USER.toString(), HAVE_ALWAYS);
		adminBasePermissions.setProperty(PermissionEnum.PERMISSION_ADMINISTRATIVE_LIST_USERS.toString(), HAVE_ALWAYS);
		adminBasePermissions.setProperty(PermissionEnum.PERMISSION_ADMINISTRATIVE_REMOVE_USERS.toString(), HAVE_ALWAYS);

		return adminBasePermissions;
	}

	private Entity initEntityBasePermissions(Key entityRoleKey) {
		Entity entityBasePermissions = new Entity(BASE_PERMISSIONS_KIND, ENTITY_ROLE, entityRoleKey);

		entityBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C0_ADD.toString(), HAVE);
		entityBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C0_DELETE.toString(), HAVE);
		entityBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C0_UPDATE_STATE.toString(), HAVE);

		entityBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C1_ADD.toString(), HAVE);
		entityBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C1_DELETE.toString(), HAVE);
		entityBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C1_UPDATE_STATE.toString(), HAVE);

		entityBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C2_ADD.toString(), HAVE);
		entityBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_CONFIRM.toString(), HAVE);
		entityBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C2_DELETE.toString(), HAVE);
		entityBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C2_UPDATE_STATE.toString(), HAVE);

		entityBasePermissions.setProperty(PermissionEnum.PERMISSION_EVENT_ADD.toString(), HAVE);
		entityBasePermissions.setProperty(PermissionEnum.PERMISSION_EVENT_LIKE.toString(), HAVE);
		entityBasePermissions.setProperty(PermissionEnum.PERMISSION_EVENT_COMMENT.toString(), HAVE);
		entityBasePermissions.setProperty(PermissionEnum.PERMISSION_EVENT_DELETE.toString(), HAVE);
		entityBasePermissions.setProperty(PermissionEnum.PERMISSION_EVENT_VERIFY.toString(), HAVE);

		entityBasePermissions.setProperty(PermissionEnum.PERMISSION_NEWS_ADD.toString(), HAVE);
		entityBasePermissions.setProperty(PermissionEnum.PERMISSION_NEWS_DELETE.toString(), HAVE);
		entityBasePermissions.setProperty(PermissionEnum.PERMISSION_NEWS_UPDATE.toString(), HAVE);

		entityBasePermissions.setProperty(PermissionEnum.PERMISSION_ADMINISTRATIVE_ADD_ROLE.toString(), NEVER);
		entityBasePermissions.setProperty(PermissionEnum.PERMISSION_ADMINISTRATIVE_UPDATE_PERMISSIONS.toString(), HAVE);
		entityBasePermissions.setProperty(PermissionEnum.PERMISSION_ADMINISTRATIVE_UPGRADE_USER.toString(), HAVE);
		entityBasePermissions.setProperty(PermissionEnum.PERMISSION_ADMINISTRATIVE_LIST_USERS.toString(), NEVER);
		entityBasePermissions.setProperty(PermissionEnum.PERMISSION_ADMINISTRATIVE_REMOVE_USERS.toString(), NEVER);

		return entityBasePermissions;
	}

	private Entity initWorkerBasePermissions(Key workerRoleKey) {
		Entity workerBasePermissions = new Entity(BASE_PERMISSIONS_KIND, WORKER_ROLE, workerRoleKey);

		workerBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C0_ADD.toString(), HAVE);
		workerBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C0_DELETE.toString(), HAVE);
		workerBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C0_UPDATE_STATE.toString(), HAVE);

		workerBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C1_ADD.toString(), HAVE);
		workerBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C1_DELETE.toString(), HAVE);
		workerBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C1_UPDATE_STATE.toString(), HAVE);

		workerBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C2_ADD.toString(), HAVE);
		workerBasePermissions.setProperty(PermissionEnum.PERMISSION_EVENT_LIKE.toString(), HAVE);
		workerBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_CONFIRM.toString(), HAVE);
		workerBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C2_DELETE.toString(), HAVE);
		workerBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C2_UPDATE_STATE.toString(), HAVE);

		workerBasePermissions.setProperty(PermissionEnum.PERMISSION_EVENT_ADD.toString(), HAVE);
		workerBasePermissions.setProperty(PermissionEnum.PERMISSION_EVENT_COMMENT.toString(), HAVE);
		workerBasePermissions.setProperty(PermissionEnum.PERMISSION_EVENT_DELETE.toString(), HAVE);
		workerBasePermissions.setProperty(PermissionEnum.PERMISSION_EVENT_VERIFY.toString(), NEVER);

		workerBasePermissions.setProperty(PermissionEnum.PERMISSION_NEWS_ADD.toString(), HAVE);
		workerBasePermissions.setProperty(PermissionEnum.PERMISSION_NEWS_DELETE.toString(), HAVE);
		workerBasePermissions.setProperty(PermissionEnum.PERMISSION_NEWS_UPDATE.toString(), HAVE);

		workerBasePermissions.setProperty(PermissionEnum.PERMISSION_ADMINISTRATIVE_ADD_ROLE.toString(), NEVER);
		workerBasePermissions.setProperty(PermissionEnum.PERMISSION_ADMINISTRATIVE_UPDATE_PERMISSIONS.toString(),
				NEVER);
		workerBasePermissions.setProperty(PermissionEnum.PERMISSION_ADMINISTRATIVE_UPGRADE_USER.toString(), NEVER);
		workerBasePermissions.setProperty(PermissionEnum.PERMISSION_ADMINISTRATIVE_LIST_USERS.toString(), NEVER);
		workerBasePermissions.setProperty(PermissionEnum.PERMISSION_ADMINISTRATIVE_REMOVE_USERS.toString(), NEVER);

		return workerBasePermissions;
	}

	private Entity initUserBasePermissions(Key userRoleKey) {
		Entity userBasePermissions = new Entity(BASE_PERMISSIONS_KIND, USER_ROLE, userRoleKey);

		userBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C0_ADD.toString(), HAVE);
		userBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C0_DELETE.toString(), HAVE);
		userBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C0_UPDATE_STATE.toString(), NEVER);

		userBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C1_ADD.toString(), HAVE);
		userBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C1_DELETE.toString(), HAVE);
		userBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C1_UPDATE_STATE.toString(), NEVER);

		userBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C2_ADD.toString(), HAVE);
		userBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_CONFIRM.toString(), HAVE);
		userBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C2_DELETE.toString(), HAVE);
		userBasePermissions.setProperty(PermissionEnum.PERMISSION_REPORT_C2_UPDATE_STATE.toString(), NEVER);

		userBasePermissions.setProperty(PermissionEnum.PERMISSION_EVENT_ADD.toString(), HAVE);
		userBasePermissions.setProperty(PermissionEnum.PERMISSION_EVENT_COMMENT.toString(), HAVE);
		userBasePermissions.setProperty(PermissionEnum.PERMISSION_EVENT_LIKE.toString(), HAVE);
		userBasePermissions.setProperty(PermissionEnum.PERMISSION_EVENT_DELETE.toString(), HAVE);
		userBasePermissions.setProperty(PermissionEnum.PERMISSION_EVENT_VERIFY.toString(), NEVER);

		userBasePermissions.setProperty(PermissionEnum.PERMISSION_NEWS_ADD.toString(), NEVER);
		userBasePermissions.setProperty(PermissionEnum.PERMISSION_NEWS_DELETE.toString(), NEVER);
		userBasePermissions.setProperty(PermissionEnum.PERMISSION_NEWS_UPDATE.toString(), NEVER);

		userBasePermissions.setProperty(PermissionEnum.PERMISSION_ADMINISTRATIVE_ADD_ROLE.toString(), NEVER);
		userBasePermissions.setProperty(PermissionEnum.PERMISSION_ADMINISTRATIVE_UPDATE_PERMISSIONS.toString(), NEVER);
		userBasePermissions.setProperty(PermissionEnum.PERMISSION_ADMINISTRATIVE_UPGRADE_USER.toString(), NEVER);
		userBasePermissions.setProperty(PermissionEnum.PERMISSION_ADMINISTRATIVE_LIST_USERS.toString(), NEVER);
		userBasePermissions.setProperty(PermissionEnum.PERMISSION_ADMINISTRATIVE_REMOVE_USERS.toString(), NEVER);

		return userBasePermissions;
	}

}
