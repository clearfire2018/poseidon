package pt.unl.fct.di.apdc.firstwebapp.resources;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.appengine.api.datastore.Transaction;
import com.google.gson.Gson;

import pt.unl.fct.di.apdc.firstwebapp.resources.BasePermissionsResource.PermissionEnum;
import pt.unl.fct.di.apdc.firstwebapp.util.AccessUser;
import pt.unl.fct.di.apdc.firstwebapp.util.Random;
import pt.unl.fct.di.apdc.firstwebapp.util.Utils;
import pt.unl.fct.di.apdc.firstwebapp.util.event.AddCommentInEvent;
import pt.unl.fct.di.apdc.firstwebapp.util.user.UserInfo;

@Path("/comment")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class EventCommentResource {
	
	private static final String EVENT_QUERY = "Event";
	private static final String COMMENT_CREATION_TIME = "comment_creation_time";
	private static final String COMMENT_TEXT = "comment_text";
	private static final String COMMENT_EVENT = "comment_event";
	private static final String COMMENT_USERNAME = "comment_username";

	private static final String COMMENT_QUERY = "Comment";
	private static final String USER_QUERY = "User";
	

	private static final Logger LOG = Logger.getLogger(UserInfo.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	
	public EventCommentResource() {
	}
	
	@POST
	@Path("/{idEvent}/post")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response addComment(@PathParam("idEvent") String idEvent, AddCommentInEvent commentAcess) {
		LOG.fine("Attempt to register comment by user: " + commentAcess.acess.username);
		if (!commentAcess.validComment()) {
			return Response.status(Status.BAD_REQUEST).entity("Missing or wrong parameter.").build();
		}
		Key userKey = KeyFactory.createKey(USER_QUERY, commentAcess.acess.username);
		if (!Utils.checkToken(commentAcess.acess, userKey) || !PermissionsResource
				.hasPermission(PermissionEnum.PERMISSION_EVENT_COMMENT, commentAcess.acess.username)) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		Transaction txn = datastore.beginTransaction();
		try {
			Key eventKey = KeyFactory.createKey(EVENT_QUERY, idEvent);
			String id = Random.key64();
			Entity comment = new Entity(COMMENT_QUERY, id + "_"+commentAcess.acess.username, eventKey);
			
			comment.setProperty(COMMENT_USERNAME, commentAcess.acess.username);
			comment.setProperty(COMMENT_EVENT, idEvent);
			comment.setProperty(COMMENT_TEXT, commentAcess.comment);
			
			comment.setProperty(COMMENT_CREATION_TIME, Utils.getCreationTime());

			List<Entity> toPut = Arrays.asList(comment);
			datastore.put(txn, toPut);
			LOG.info("Comment by user " + commentAcess.acess.username + " registered.");
			txn.commit();
			return Response.ok(g.toJson(comment.getKey())).build();

		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}

	}

	@POST
	@Path("/{idEvent}/list")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doListCommentInEvent(@PathParam("idEvent") String idEvent, AccessUser acess) {
		LOG.warning("Attempt to list events: " + acess.tokenID);
		Key userKey = KeyFactory.createKey(USER_QUERY, acess.username);
		Key eventKey = KeyFactory.createKey(EVENT_QUERY, idEvent);
		if (!Utils.checkToken(acess, userKey)) {
			return Response.status(Status.BAD_REQUEST).build();
		}

		//Filter eventFilter = eventFilter(idEvent);
		Query commentsQuery = new Query(COMMENT_QUERY).setAncestor(eventKey);//.setFilter(eventFilter);
		List<Entity> comments = datastore.prepare(commentsQuery).asList(FetchOptions.Builder.withDefaults());
		return Response.ok(g.toJson(comments)).build();
	}
	
	/**private Filter eventFilter(String idEvent) {
		return new FilterPredicate(COMMENT_EVENT, FilterOperator.EQUAL, idEvent);
	}
*/
	
}
