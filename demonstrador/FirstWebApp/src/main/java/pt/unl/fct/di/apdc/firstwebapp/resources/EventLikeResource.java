package pt.unl.fct.di.apdc.firstwebapp.resources;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.hk2.api.messaging.Topic;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.TransactionOptions;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.gson.Gson;

import pt.unl.fct.di.apdc.firstwebapp.resources.BasePermissionsResource.PermissionEnum;
import pt.unl.fct.di.apdc.firstwebapp.util.AccessUser;
import pt.unl.fct.di.apdc.firstwebapp.util.Utils;
import pt.unl.fct.di.apdc.firstwebapp.util.event.Top;

@Path("/events/like")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class EventLikeResource {
	private static final int MAX_NUM_TOP_LIKED = 4;
	private static final String TOP_LIKED_EVENT = "event";
	private static final String TOTAL_LIKES = "total_likes";
	public static final String TOP_LIKED_KIND = "TopLiked";
	private static final String LIKED = "liked";
	private static final String LIKE_QUERY = "Like";
	private static final String EVENT_QUERY = "Event";

	private static final String EVENT_NUMLIKE = "event_numlike";
	private static final String EVENT_ID = "event_id";

	private static final String LIKE_CREATION_TIME = "like_creation_time";
	private static final String LIKE_USER = "like_user";
	private static final String LIKE_EVENT = "like_event";

	private static final Logger LOG = Logger.getLogger(EventResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	public EventLikeResource() {
	} // Nothing to be done here...

	@POST
	@Path("/{idEvent}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response likeEvent(AccessUser acess, @PathParam("idEvent") String idEvent) {
		LOG.fine("Attempt to like event " + idEvent + " by user: " + acess.username);

		Key userKey = KeyFactory.createKey("User", acess.username);
		Key eventKey = KeyFactory.createKey(EVENT_QUERY, idEvent);

		if (!Utils.checkToken(acess, userKey)) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		try {
			datastore.get(eventKey);
		} catch (EntityNotFoundException e1) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		Transaction txn = datastore.beginTransaction();
		Key likeKey = KeyFactory.createKey(eventKey, LIKE_QUERY, acess.username);
		Entity like;
		boolean liked = true;
		try {
			like = datastore.get(likeKey);
			liked = (boolean) like.getProperty(LIKED);
			liked = !liked;
			like.setProperty(LIKED, liked);
		} catch (EntityNotFoundException e) {
			like = new Entity(LIKE_QUERY, acess.username, eventKey);
			like.setProperty(LIKED, liked);
		}
		like.setProperty(LIKE_CREATION_TIME, Utils.getCreationTime());
		LOG.info("Liked: " + liked);
		datastore.put(txn, like);
		txn.commit();
		int numLikes = numLikes(idEvent);
		LOG.info("num top: " + numLikes);
		// verificar se o utilizador pode por um like

		return Response.ok(numLikes).build();

	}

	@GET
	@Path("/updateTopLikes")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response updateTopLikes() {
		boolean[] changed = new boolean[MAX_NUM_TOP_LIKED];
		PriorityQueue<Top> queue = new PriorityQueue<>(3);
		Top[] topLiked = new Top[MAX_NUM_TOP_LIKED];

		Date now = new Date();
		Calendar cal = Calendar.getInstance();
		// remove next line if you're always using the current time.
		cal.setTime(now);
		cal.add(Calendar.MINUTE, -3);
		Date threeMinutesBack = cal.getTime();
		Filter propertyFilter = new FilterPredicate(LIKE_CREATION_TIME, FilterOperator.GREATER_THAN,
				threeMinutesBack.getTime());
		Query topLikedCheckQuery = new Query(TOP_LIKED_KIND).addSort("__key__", SortDirection.DESCENDING);
		List<Entity> listTopLiked = datastore.prepare(topLikedCheckQuery)
				.asList(FetchOptions.Builder.withLimit(MAX_NUM_TOP_LIKED));

		Query eventsToUpdateQuery = new Query(LIKE_QUERY).setFilter(propertyFilter);
		List<Entity> eventsToUpdate = datastore.prepare(eventsToUpdateQuery)
				.asList(FetchOptions.Builder.withDefaults());
		Map<String, Top> toAdd = new HashMap<>();
		LOG.info("num:" + eventsToUpdate.size());
		int size = listTopLiked.size();
		for (int i = 0; i < size && i < MAX_NUM_TOP_LIKED; i++) {
			changed[i] = false;
			Entity e = listTopLiked.get(i);
			topLiked[i] = new Top(e.getProperty(TOP_LIKED_EVENT).toString(),
					Integer.parseInt(e.getProperty(TOTAL_LIKES).toString()));
		}

		for (Entity e : eventsToUpdate) {
			String name = e.getParent().getName();
			Top newTop = new Top(name, numLikes(name));
			if (toAdd.put(name, newTop) == null)
				queue.add(newTop);
			LOG.warning(newTop.toString());
			for (int i = 0; i < size && i < MAX_NUM_TOP_LIKED; i++) {
				if (!changed[i] && topLiked[i].equals(newTop))
					changed[i] = true;
			}
		}
		for (int i = 0; i < size && i < MAX_NUM_TOP_LIKED; i++) {
			if (!changed[i]) {
				queue.add(topLiked[i]);
				LOG.warning(topLiked[i].toString());
			}
		}
		LOG.info(queue.toString());
		List<Entity> toPut = new LinkedList<>();
		for (int i = 0; i < MAX_NUM_TOP_LIKED; i++) {
			Entity topEntity = new Entity(TOP_LIKED_KIND, i + 1);
			Top top = queue.poll();
			if (top == null)
				break;
			topEntity.setProperty(TOP_LIKED_EVENT, top.getId());
			topEntity.setProperty(TOTAL_LIKES, top.getNumLikes());
			toPut.add(topEntity);
		}
		if (!toPut.isEmpty())
			datastore.put(toPut);
		return Response.ok().build();
	}

	/**
	 * public static void updateTopLikes(String idEvent, int numLikes) { Transaction
	 * txn;
	 * 
	 * // query para saber se já existem os 3 lugares na tabela Query
	 * topLikedCheckQuery = new Query(TOP_LIKED).addSort("__key__",
	 * SortDirection.DESCENDING); List<Entity> listTopLiked =
	 * datastore.prepare(topLikedCheckQuery).asList(FetchOptions.Builder.withDefaults());
	 * Filter equalsId = new FilterPredicate(TOP_LIKED_EVENT, FilterOperator.EQUAL,
	 * idEvent); Query isInTopQuery = new Query(TOP_LIKED).setFilter(equalsId);
	 * boolean isInTop = datastore.prepare(isInTopQuery).asSingleEntity() != null;
	 * int size = listTopLiked.size();
	 * 
	 * List<Entity> toPut = new LinkedList<>(); if (isInTop) { LOG.info("IF!!!!!");
	 * boolean founded = false; Long id = MAX_NUM_TOP_LIKED +1; Entity lastEntity =
	 * new Entity(TOP_LIKED, 1); lastEntity.setProperty(TOP_LIKED_EVENT, idEvent);
	 * lastEntity.setProperty(TOTAL_LIKES, numLikes); for (Entity e : listTopLiked)
	 * { LOG.info("!for...: "+ e.toString()); if(!founded &&
	 * ((Long)e.getProperty(TOTAL_LIKES) > numLikes)){ Entity newEntity = new
	 * Entity(TOP_LIKED, e.getKey().getId()); newEntity.setProperty(TOP_LIKED_EVENT,
	 * lastEntity.getProperty(TOP_LIKED_EVENT)); newEntity.setProperty(TOTAL_LIKES,
	 * lastEntity.getProperty(TOTAL_LIKES)); toPut.add(newEntity); lastEntity = e;
	 * LOG.info("!founded...: "+ e.toString());
	 * 
	 * }
	 * 
	 * else if(founded && ((Long)e.getProperty(TOTAL_LIKES) <= numLikes)) { Entity
	 * newEntity = new Entity(TOP_LIKED, e.getKey().getId()+1);
	 * newEntity.setProperty(TOP_LIKED_EVENT, e.getProperty(TOP_LIKED_EVENT));
	 * newEntity.setProperty(TOTAL_LIKES, e.getProperty(TOTAL_LIKES));
	 * toPut.add(newEntity); if(id > e.getKey().getId()) { id = e.getKey().getId();
	 * } LOG.info("founded...: "+ newEntity); LOG.info("founded...id: "+ id);
	 * 
	 * } if(e.getProperty(TOP_LIKED_EVENT).equals(idEvent)) { founded = true;
	 * if(!lastEntity.getProperty(TOP_LIKED_EVENT).equals(idEvent) && id >
	 * e.getKey().getId()) { id = e.getKey().getId(); } } }
	 * 
	 * if (id != MAX_NUM_TOP_LIKED +1) { Entity updateTop = new Entity(TOP_LIKED,
	 * id); updateTop.setProperty(TOP_LIKED_EVENT, idEvent);
	 * updateTop.setProperty(TOTAL_LIKES, numLikes); toPut.add(updateTop);
	 * LOG.info("UPDATE : "+ id); } LOG.info("Else : "+ numLikes);
	 * 
	 * } else { LOG.info("Else!!!!!"); int count = 0; for (Entity e : listTopLiked)
	 * { if (((Long) e.getProperty(TOTAL_LIKES)) < numLikes) { if
	 * (e.getKey().getId() < MAX_NUM_TOP_LIKED) { Entity decrease = new
	 * Entity(TOP_LIKED, e.getKey().getId() + 1L); decrease.setProperty(TOTAL_LIKES,
	 * e.getProperty(TOTAL_LIKES)); decrease.setProperty(TOP_LIKED_EVENT,
	 * e.getProperty(TOP_LIKED_EVENT)); toPut.add(decrease); } count++; } } int id =
	 * size+1 - count; if(id <= MAX_NUM_TOP_LIKED) { Entity newTop = new
	 * Entity(TOP_LIKED, id); newTop.setProperty(TOTAL_LIKES, numLikes);
	 * newTop.setProperty(TOP_LIKED_EVENT, idEvent); toPut.add(newTop);
	 * LOG.info("Else : "+ numLikes); } }
	 * 
	 * if(!toPut.isEmpty()) { txn =
	 * datastore.beginTransaction(TransactionOptions.Builder.withXG(true));
	 * datastore.put(txn, toPut); txn.commit(); } }
	 */

	@SuppressWarnings("deprecation")
	public static int numLikes(String id) {
		Key eventKey = KeyFactory.createKey(EVENT_QUERY, id);
		Filter propertyFilter = new FilterPredicate(LIKED, FilterOperator.EQUAL, true);
		Query numLikesQuery = new Query(LIKE_QUERY).setAncestor(eventKey).setFilter(propertyFilter);
		return datastore.prepare(numLikesQuery).countEntities();
	}
}
