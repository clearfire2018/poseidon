package pt.unl.fct.di.apdc.firstwebapp.resources;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.PropertyProjection;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.QueryResultList;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.appengine.repackaged.com.google.datastore.v1.Projection;
import com.google.appengine.repackaged.com.google.datastore.v1.ProjectionOrBuilder;
import com.google.appengine.repackaged.com.google.datastore.v1.PropertyReference;
import com.google.appengine.repackaged.com.google.datastore.v1.PropertyReferenceOrBuilder;
import com.google.appengine.repackaged.com.google.protobuf.Message;
import com.google.appengine.repackaged.com.google.protobuf.UnknownFieldSet;
import com.google.appengine.repackaged.com.google.protobuf.Descriptors.Descriptor;
import com.google.appengine.repackaged.com.google.protobuf.Descriptors.FieldDescriptor;
import com.google.appengine.repackaged.com.google.protobuf.Descriptors.OneofDescriptor;
import com.google.gson.Gson;

import pt.unl.fct.di.apdc.firstwebapp.util.AccessUser;
import pt.unl.fct.di.apdc.firstwebapp.util.Utils;
import pt.unl.fct.di.apdc.firstwebapp.util.event.ListEventsKey;
import pt.unl.fct.di.apdc.firstwebapp.util.report.ListReports;

@Path("/events/list")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class EventListResource {
	private static final String EVENT_QUERY = "Event";
	private static final String USER_QUERY = "User";

	private static final String EVENT_VERIFIED = "event_isverified";
	private static final String EVENT_CITY = "event_city";
	private static final String EVENT_DATE_BEGIN = "event_begin";
	private static final String EVENT_DATE_END = "event_end";
	private static final String EVENT_SIGNUPS = "event_numsign";
	private static final String EVENT_LIKES = "event_numlike";
	private static final int PAGE_SIZE = 5;


	private static final Logger LOG = Logger.getLogger(EventResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	public EventListResource() {
	} // Nothing to be done here...

	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doListEventsKeysOnly(@QueryParam("cursor") String cursor, AccessUser acess) {
		LOG.warning("Attempt to list events: " + acess.tokenID);
		Key userKey = KeyFactory.createKey(USER_QUERY, acess.username);
		if (!Utils.checkToken(acess, userKey)) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		Query eventsQuery = new Query(EVENT_QUERY).addSort(EVENT_DATE_BEGIN, SortDirection.DESCENDING).setKeysOnly();
		FetchOptions fetchOptions = FetchOptions.Builder.withLimit(PAGE_SIZE);
		if (cursor != null) {
			LOG.warning("cursor: " + cursor);
			fetchOptions = fetchOptions.startCursor(Cursor.fromWebSafeString(cursor));
		}
		PreparedQuery pq = datastore.prepare(eventsQuery);
		QueryResultList<Entity> events = pq.asQueryResultList(fetchOptions);
		String newcursor = events.getCursor().toWebSafeString();
		LOG.warning("newcursor: " + newcursor);
		ListEventsKey list = new ListEventsKey(events, newcursor);
		return Response.ok(g.toJson(list)).build();
	}
	@POST
	@Path("/info")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doListEvents(@QueryParam("cursor") String cursor, AccessUser acess) {
		LOG.warning("Attempt to list events: " + acess.tokenID);
		Key userKey = KeyFactory.createKey(USER_QUERY, acess.username);
		if (!Utils.checkToken(acess, userKey)) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		Query eventsQuery = new Query(EVENT_QUERY).addSort(EVENT_DATE_BEGIN, SortDirection.DESCENDING);
		FetchOptions fetchOptions = FetchOptions.Builder.withLimit(PAGE_SIZE);
		if (cursor != null) {
			LOG.warning("cursor: " + cursor);
			fetchOptions = fetchOptions.startCursor(Cursor.fromWebSafeString(cursor));
		}
		PreparedQuery pq = datastore.prepare(eventsQuery);
		QueryResultList<Entity> events = pq.asQueryResultList(fetchOptions);
		String newcursor = events.getCursor().toWebSafeString();
		LOG.warning("newcursor: " + newcursor);
		
		for(Entity e : events) {
			e.setProperty("event_numLikes", EventLikeResource.numLikes(e.getKey().getName()));
			e.setProperty("event_numSignups", EventSignupResource.numSignup(e.getKey().getName()));
			Key likeKey = KeyFactory.createKey(e.getKey(), "Like", acess.username);
			Key signupKey = KeyFactory.createKey(e.getKey(), "Signup", acess.username);
			try {
				Entity like = datastore.get(likeKey);
				e.setProperty("event_like",like.getProperty("liked"));
			} catch (EntityNotFoundException e1) {
				e.setProperty("event_like", false);
			}
			try {
				Entity signup = datastore.get(signupKey);
				e.setProperty("event_signup",signup.getProperty("signed"));
			} catch (EntityNotFoundException e1) {
				e.setProperty("event_signup", false);
			}
			
		}
		
		ListEventsKey list = new ListEventsKey(events, newcursor);
		return Response.ok(g.toJson(list)).build();
	}

	@POST
	@Path("/listTopLiked")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doListTopLikedEvents(AccessUser acess) {
		LOG.warning("Attempt to list events: " + acess.tokenID);
		Key userKey = KeyFactory.createKey(USER_QUERY, acess.username);
		if (!Utils.checkToken(acess, userKey)) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		Query eventsQuery = new Query(EventLikeResource.TOP_LIKED_KIND);
		eventsQuery.addSort("__key__", SortDirection.ASCENDING);
		List<Entity> events = datastore.prepare(eventsQuery).asList(FetchOptions.Builder.withLimit(3));
		LOG.warning("Attempt to list events: " + events);
		for(Entity e : events) {
			String id =  (String)e.getProperty("event");
			e.setProperty("event_numLikes", EventLikeResource.numLikes(id));
			e.setProperty("event_numSignups", EventSignupResource.numSignup(id));
			Key likeKey = KeyFactory.createKey(KeyFactory.createKey("Event", id), "Like", acess.username);
			Key signupKey = KeyFactory.createKey(KeyFactory.createKey("Event", id), "Signup", acess.username);
			try {
				Entity like = datastore.get(likeKey);
				e.setProperty("event_like",like.getProperty("liked"));
			} catch (EntityNotFoundException e1) {
				e.setProperty("event_like", false);
			}
			try {
				Entity signup = datastore.get(signupKey);
				e.setProperty("event_signup",signup.getProperty("signed"));
			} catch (EntityNotFoundException e1) {
				e.setProperty("event_signup", false);
			}
			
		}
		return Response.ok(g.toJson(events)).build();
	}

	@POST
	@Path("/listTopSign")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doListTopSignEvents(AccessUser acess) {
		LOG.warning("Attempt to list events: " + acess.tokenID);
		Key userKey = KeyFactory.createKey(USER_QUERY, acess.username);
		if (!Utils.checkToken(acess, userKey)) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		Query listTopSign = new Query(EventSignupResource.TOP_SIGNUP).addSort("__key__", SortDirection.ASCENDING);
		List<Entity> events = datastore.prepare(listTopSign).asList(FetchOptions.Builder.withLimit(3));
		for(Entity e : events) {
			String id =  (String)e.getProperty("event");
			e.setProperty("event_numLikes", EventLikeResource.numLikes(id));
			e.setProperty("event_numSignups", EventSignupResource.numSignup(id));
			Key likeKey = KeyFactory.createKey(KeyFactory.createKey("Event", id), "Like", acess.username);
			Key signupKey = KeyFactory.createKey(KeyFactory.createKey("Event", id), "Signup", acess.username);
			try {
				Entity like = datastore.get(likeKey);
				e.setProperty("event_like",like.getProperty("liked"));
			} catch (EntityNotFoundException e1) {
				e.setProperty("event_like", false);
			}
			try {
				Entity signup = datastore.get(signupKey);
				e.setProperty("event_signup",signup.getProperty("signed"));
			} catch (EntityNotFoundException e1) {
				e.setProperty("event_signup", false);
			}
			
		}
		return Response.ok(g.toJson(events)).build();
	}

	@POST
	@Path("/listByRegion={region}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doListRegionEvents(AccessUser acess, @PathParam("region") String region) {
		LOG.warning("Attempt to list events: " + acess.tokenID);
		Key userKey = KeyFactory.createKey(USER_QUERY, acess.username);
		if (!Utils.checkToken(acess, userKey)) {
			return Response.status(Status.BAD_REQUEST).build();
		}

		Filter propertyFilter = new FilterPredicate(EVENT_CITY, FilterOperator.EQUAL, region);
		Query eventsQuery = new Query(EVENT_QUERY);
		eventsQuery.setFilter(propertyFilter);
		List<Entity> events = datastore.prepare(eventsQuery).asList(FetchOptions.Builder.withDefaults());
		LOG.warning("Attempt to list events: " + events);
		return Response.ok(g.toJson(events)).build();
	}

	@POST
	@Path("/listAfterDate={date}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doListDateEvents(AccessUser acess, @PathParam("date") String date) {
		LOG.warning("Attempt to list events: " + acess.tokenID);
		Key userKey = KeyFactory.createKey(USER_QUERY, acess.username);
		if (!Utils.checkToken(acess, userKey)) {
			return Response.status(Status.BAD_REQUEST).build();
		}

		Filter propertyFilter = new FilterPredicate(EVENT_DATE_END, FilterOperator.GREATER_THAN_OR_EQUAL, date);
		Query eventsQuery = new Query(EVENT_QUERY);
		eventsQuery.setFilter(propertyFilter);
		List<Entity> events = datastore.prepare(eventsQuery).asList(FetchOptions.Builder.withDefaults());
		LOG.warning("Attempt to list events: " + events);
		return Response.ok(g.toJson(events)).build();
	}

	@POST
	@Path("/listInDate={date}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doListDateEqualEvents(AccessUser acess, @PathParam("date") String date) {
		LOG.warning("Attempt to list events: " + acess.tokenID);
		Key userKey = KeyFactory.createKey(USER_QUERY, acess.username);
		if (!Utils.checkToken(acess, userKey)) {
			return Response.status(Status.BAD_REQUEST).build();
		}

		Filter propertyFilter = new FilterPredicate(EVENT_DATE_BEGIN, FilterOperator.EQUAL, date);
		Query eventsQuery = new Query(EVENT_QUERY);
		eventsQuery.setFilter(propertyFilter);
		List<Entity> events = datastore.prepare(eventsQuery).asList(FetchOptions.Builder.withDefaults());
		LOG.warning("Attempt to list events: " + events);
		return Response.ok(g.toJson(events)).build();
	}

	@POST
	@Path("/verified")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response listEventsVerified(AccessUser acess) {
		LOG.warning("Attempt to list events: " + acess.tokenID);
		Key userKey = KeyFactory.createKey(USER_QUERY, acess.username);
		if (!Utils.checkToken(acess, userKey)) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		Query eventsQuery = new Query(EVENT_QUERY);
		eventsQuery.setFilter(verified());
		List<Entity> events = datastore.prepare(eventsQuery).asList(FetchOptions.Builder.withDefaults());
		LOG.warning("Attempt to list events: " + events);
		return Response.ok(g.toJson(events)).build();
	}

	@POST
	@Path("/unverified")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response listEventsUnverified(AccessUser acess) {
		LOG.warning("Attempt to list events: " + acess.tokenID);
		Key userKey = KeyFactory.createKey(USER_QUERY, acess.username);
		if (!Utils.checkToken(acess, userKey)) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		Query eventsQuery = new Query(EVENT_QUERY);
		eventsQuery.setFilter(unverified());
		List<Entity> events = datastore.prepare(eventsQuery).asList(FetchOptions.Builder.withDefaults());
		LOG.warning("Attempt to list events: " + events);
		return Response.ok(g.toJson(events)).build();
	}

	// no need for a user to be logged in
	@POST
	@Path("/allVerified")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response listAllEventsVerified() {
		Query eventsQuery = new Query(EVENT_QUERY);
		eventsQuery.setFilter(verified());
		List<Entity> events = datastore.prepare(eventsQuery).asList(FetchOptions.Builder.withDefaults());
		LOG.warning("Attempt to list events: " + events);
		return Response.ok(g.toJson(events)).build();
	}

	private Filter notExpired() {
		Long currentTime = System.currentTimeMillis();
		return new FilterPredicate(EVENT_DATE_END, FilterOperator.GREATER_THAN_OR_EQUAL, currentTime);
	}

	private Filter unverified() {
		return new FilterPredicate(EVENT_VERIFIED, FilterOperator.EQUAL, "false");
	}

	private Filter verified() {
		return new FilterPredicate(EVENT_VERIFIED, FilterOperator.EQUAL, "true");
	}
}
