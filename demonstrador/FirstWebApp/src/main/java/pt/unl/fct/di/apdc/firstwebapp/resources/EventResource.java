package pt.unl.fct.di.apdc.firstwebapp.resources;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Transaction;
import com.google.gson.Gson;

import pt.unl.fct.di.apdc.firstwebapp.resources.BasePermissionsResource.PermissionEnum;
import pt.unl.fct.di.apdc.firstwebapp.util.AccessUser;
import pt.unl.fct.di.apdc.firstwebapp.util.Random;
import pt.unl.fct.di.apdc.firstwebapp.util.Utils;
import pt.unl.fct.di.apdc.firstwebapp.util.event.EventData;
import pt.unl.fct.di.apdc.firstwebapp.util.event.EventInfo;

@Path("/events")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class EventResource {

	private static final String EVENT_QUERY = "Event";
	private static final String USER_QUERY = "User";

	private static final String EVENT_VERIFIED = "event_isverified";
	private static final String EVENT_PARENT_USER = "event_user";
	private static final String CREATION_TIME = "event_creation_time";
	private static final String CITY = "event_city";
	private static final String DATE_END = "event_end";
	private static final String DATE_BEGIN = "event_begin";
	private static final String SIGN_UPS = "event_numsign";
	private static final String LIKES = "event_numlike";
	private static final String DESCRIPTION = "event_description";
	private static final String LOCATION = "event_location";
	private static final String NAME = "event_name";
	private static final String AUTHOR = "event_author";

	private static final Logger LOG = Logger.getLogger(EventResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	public EventResource() {
	} // Nothing to be done here...

	@POST
	@Path("/register")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response registerEvent(EventData data) {
		LOG.warning("Attempt to register event: " + data.name);

		if (!data.validRegistration() || !PermissionsResource
				.hasPermission(PermissionEnum.PERMISSION_EVENT_ADD, data.acess.username)) {
			return Response.status(Status.BAD_REQUEST).entity("Missing or wrong parameter.").build();
		}
		LOG.warning("Attempt to token event: " + data.acess.tokenID);
		Key userKey = KeyFactory.createKey(USER_QUERY, data.acess.username);

		if (!Utils.checkToken(data.acess, userKey)) {
			return Response.status(Status.BAD_REQUEST).build();
		}

		Transaction txn = datastore.beginTransaction();

		String id = Random.key64();

		Entity event = new Entity(EVENT_QUERY, id);
		event.setProperty(EVENT_VERIFIED, false);
		event.setProperty(NAME, data.name);
		event.setProperty(LOCATION, data.location);
		event.setProperty(DESCRIPTION, data.description);
		event.setProperty(DATE_BEGIN, data.beginDate);
		event.setProperty(DATE_END, data.endDate);
		event.setProperty(CITY, data.city);
		event.setProperty(EVENT_PARENT_USER, data.acess.username);
		event.setUnindexedProperty(CREATION_TIME, Utils.getCreationTime());

		datastore.put(txn, event);
		LOG.info("Report by user " + data.acess.username + " registered with id " + id);
		txn.commit();
		LOG.warning("Attempt to reponse:" + id);
		return Response.ok(id).build();
	}

	@POST
	@Path("/{idEvent}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response infoEvent(AccessUser acess, @PathParam("idEvent") String idEvent) {
		Key userKey = KeyFactory.createKey(USER_QUERY, acess.username);
		if (!Utils.checkToken(acess, userKey)) {
			return Response.status(Status.BAD_REQUEST).build();
		}

		EventInfo info;
		String name;
		String description;
		String city = "";
		String location = "";
		long beginDate;
		long endDate;
		int numLikes = EventLikeResource.numLikes(idEvent);
		int numSigns = EventSignupResource.numSignup(idEvent);
		String usernameAuthor;
		boolean verify = false;
		Key eventKey = KeyFactory.createKey(EVENT_QUERY, idEvent);
		Entity event;
		try {
			event = datastore.get(eventKey);

			name = (String) event.getProperty(NAME);
			location = (String) event.getProperty(LOCATION);
			description = (String) event.getProperty(DESCRIPTION);
			try {
				beginDate = Long.parseLong((String) event.getProperty(DATE_BEGIN));
			} catch (NumberFormatException e) {
				beginDate = -1;
			}
			try {
				endDate = Long.parseLong((String) event.getProperty(DATE_END));
			} catch (NumberFormatException e) {
				endDate = -1;
			}
			usernameAuthor = (String) event.getProperty(EVENT_PARENT_USER);
			try {
				verify = (Boolean) event.getProperty(EVENT_VERIFIED);
			} catch (Exception e) {

			}
			city = (String) event.getProperty(CITY);
			info = new EventInfo(idEvent, usernameAuthor, name, description, location, city, numLikes, numSigns,
					beginDate, endDate, verify); 
			return Response.ok(g.toJson(info)).build();

		} catch (EntityNotFoundException e1) {
			return Response.status(Status.BAD_REQUEST).build();
		}

	}

	@DELETE
	@Path("/{event}/remove")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doRemoveEvent(@PathParam("event") String eventToRemove, AccessUser acess) {
		// TODO: verificar se o user tem permissao
		Key userKey = KeyFactory.createKey(USER_QUERY, acess.username);
		if (!Utils.checkToken(acess, userKey)|| !PermissionsResource
				.hasPermission(PermissionEnum.PERMISSION_EVENT_DELETE, acess.username) ) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		Transaction txn = datastore.beginTransaction();
		Key eventKey = KeyFactory.createKey(EVENT_QUERY, eventToRemove);
		Entity event;
		try {
			event = datastore.get(eventKey);
			datastore.delete(txn, event.getKey());
			txn.commit();
			LOG.info("Event '" + eventToRemove + "' removed sucessfully.");
			return Response.ok().build();
		} catch (EntityNotFoundException e1) {
			return Response.status(Status.BAD_REQUEST).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		}
	}

	@POST
	@Path("/{idEvent}/verify")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response verifyEvent(@PathParam("idEvent") String idEvent, AccessUser acess) {
		Key userKey = KeyFactory.createKey(USER_QUERY, acess.username);
		if (!Utils.checkToken(acess, userKey)|| !PermissionsResource
				.hasPermission(PermissionEnum.PERMISSION_EVENT_VERIFY, acess.username) ) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		
		Transaction txn = datastore.beginTransaction();
		Key eventKey = KeyFactory.createKey(EVENT_QUERY, idEvent);
		Entity event;
		try {
			event = datastore.get(eventKey);
			if (!(boolean) event.getProperty(EVENT_VERIFIED)) {
				event.setProperty(EVENT_VERIFIED, true);
				datastore.put(txn, event);
				txn.commit();
			}
			return Response.ok().build();
		} catch (EntityNotFoundException e1) {
			return Response.status(Status.BAD_REQUEST).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		}
	}
	
	public static int numEventByUser(String username) {
		Filter filterUser = new FilterPredicate(EVENT_PARENT_USER, FilterOperator.EQUAL, username);

		Query reportQuery = new Query(EVENT_QUERY).setFilter(filterUser);
		int conf = datastore.prepare(reportQuery).countEntities();
		LOG.info("" + conf);
		return conf;
	}

}
