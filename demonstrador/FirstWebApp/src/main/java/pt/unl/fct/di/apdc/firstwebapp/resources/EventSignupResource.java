package pt.unl.fct.di.apdc.firstwebapp.resources;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.TransactionOptions;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.gson.Gson;

import pt.unl.fct.di.apdc.firstwebapp.util.AccessUser;
import pt.unl.fct.di.apdc.firstwebapp.util.Utils;
import pt.unl.fct.di.apdc.firstwebapp.util.event.Top;

@Path("events/signup")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class EventSignupResource {
	private static final String LIKE_CREATION_TIME = "like_creation_time";
	private static final String SIGNED = "signed";
	private static final String TOTAL_SIGUPS = "total_sigups";
	private static final int MAX_NUM_TOP_SIGNUP = 4;
	private static final String TOP_SIGNUP_EVENT = "top_signup_event";
	public static final String TOP_SIGNUP = "TopSignup";
	private static final String EVENT_QUERY = "Event";
	private static final String SIGNUP_QUERY = "Signup";

	private static final String EVENT_NUMSIGN = "event_numsign";
	private static final String EVENT_ID = "event_id";

	private static final String SIGNUP_TIME = "signup_time";
	private static final String SIGNUP_USER = "signup_user";
	private static final String SIGNUP_EVENT = "signup_event";

	private static final Logger LOG = Logger.getLogger(EventResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	public EventSignupResource() {
	} // Nothing to be done here...

	@POST
	@Path("/{idEvent}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response signupEvent(AccessUser acess, @PathParam("idEvent") String idEvent) {
		LOG.fine("Attempt to signup event " + idEvent + " by user: " + acess.username);

		Key userKey = KeyFactory.createKey("User", acess.username);
		Key eventKey = KeyFactory.createKey(EVENT_QUERY, idEvent);

		if (!Utils.checkToken(acess, userKey)) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		try {
			datastore.get(eventKey);
		} catch (EntityNotFoundException e1) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		Transaction txn = datastore.beginTransaction();
		Key signupKey = KeyFactory.createKey(eventKey, SIGNUP_QUERY, acess.username);
		Entity signup;
		boolean signed = true;
		try {
			signup = datastore.get(signupKey);
			signed = (boolean) signup.getProperty(SIGNED);
			signed = !signed;
			signup.setProperty(SIGNED, signed);
		} catch (EntityNotFoundException e) {
			signup = new Entity(SIGNUP_QUERY, acess.username, eventKey);

			signup.setProperty(SIGNED, signed);
		}
		signup.setProperty(SIGNUP_USER, acess.username);
		signup.setProperty(LIKE_CREATION_TIME, Utils.getCreationTime());
		LOG.info("Signup: " + signed);
		datastore.put(txn, signup);
		txn.commit();
		int numSignups = numSignup(idEvent);
		LOG.info("num top: " + numSignups);

		return Response.ok(numSignups).build();

	}

	@GET
	@Path("/updateTopSignups")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response updateTopSignup() {
		boolean[] changed = new boolean[MAX_NUM_TOP_SIGNUP];
		PriorityQueue<Top> queue = new PriorityQueue<>(3);
		Top[] topLiked = new Top[MAX_NUM_TOP_SIGNUP];

		Date now = new Date();
		Calendar cal = Calendar.getInstance();
		// remove next line if you're always using the current time.
		cal.setTime(now);
		cal.add(Calendar.MINUTE, -3);
		Date threeMinutesBack = cal.getTime();
		Filter propertyFilter = new FilterPredicate(LIKE_CREATION_TIME, FilterOperator.GREATER_THAN,
				threeMinutesBack.getTime());
		Query topLikedCheckQuery = new Query(TOP_SIGNUP).addSort("__key__", SortDirection.DESCENDING);
		List<Entity> listTopLiked = datastore.prepare(topLikedCheckQuery)
				.asList(FetchOptions.Builder.withLimit(MAX_NUM_TOP_SIGNUP));

		Query eventsToUpdateQuery = new Query(SIGNUP_QUERY).setFilter(propertyFilter);
		List<Entity> eventsToUpdate = datastore.prepare(eventsToUpdateQuery)
				.asList(FetchOptions.Builder.withDefaults());
		Map<String, Top> toAdd = new HashMap<>();
		LOG.info("num:" + eventsToUpdate.size());
		int size = listTopLiked.size();
		for (int i = 0; i < size && i < MAX_NUM_TOP_SIGNUP; i++) {
			changed[i] = false;
			Entity e = listTopLiked.get(i);
			topLiked[i] = new Top(e.getProperty(TOP_SIGNUP_EVENT).toString(),
					Integer.parseInt(e.getProperty(TOTAL_SIGUPS).toString()));
		}

		for (Entity e : eventsToUpdate) {
			String name = e.getParent().getName();
			Top newTop = new Top(name, numSignup(name));
			if (toAdd.put(name, newTop) == null)
				queue.add(newTop);
			for (int i = 0; i < size && i < MAX_NUM_TOP_SIGNUP; i++) {
				if (!changed[i] && topLiked[i].equals(newTop))
					changed[i] = true;
			}
		}
		for (int i = 0; i < size && i < MAX_NUM_TOP_SIGNUP; i++) {
			if (!changed[i])
				queue.add(topLiked[i]);
		}
		LOG.info(queue.toString());
		List<Entity> toPut = new LinkedList<>();
		for (int i = 0; i < MAX_NUM_TOP_SIGNUP; i++) {
			Entity topEntity = new Entity(TOP_SIGNUP, i + 1);
			Top top = queue.poll();
			if (top == null)
				break;
			topEntity.setProperty(TOP_SIGNUP_EVENT, top.getId());
			topEntity.setProperty(TOTAL_SIGUPS, top.getNumLikes());
			toPut.add(topEntity);
		}
		if (!toPut.isEmpty())
			datastore.put(toPut);
		return Response.ok().build();
	}

	public Filter userHasSignUp(String username) {
		return new FilterPredicate(SIGNUP_USER, FilterOperator.EQUAL, username);
	}

	@SuppressWarnings("deprecation")
	public static int numSignup(String id) {
		Key eventKey = KeyFactory.createKey(EVENT_QUERY, id);
		Filter propertyFilter = new FilterPredicate(SIGNED, FilterOperator.EQUAL, true);
		Query numSignupsQuery = new Query(SIGNUP_QUERY).setAncestor(eventKey).setFilter(propertyFilter);
		return datastore.prepare(numSignupsQuery).countEntities();
	}

	
	public static int numSignupByUser(String username) {
		Key userKey = KeyFactory.createKey("User", username);
		Filter userFilter = new FilterPredicate(SIGNUP_USER, FilterOperator.EQUAL, username);
		Filter propertyFilter = new FilterPredicate(SIGNED, FilterOperator.EQUAL, true);
		Filter filter = CompositeFilterOperator.and(propertyFilter, userFilter );
		Query numSignupsQuery = new Query(SIGNUP_QUERY).setFilter(filter);
		return datastore.prepare(numSignupsQuery).countEntities();
	}

	// TODO:List users signedup para o criador do evento
}
