package pt.unl.fct.di.apdc.firstwebapp.resources;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.gson.Gson;

import pt.unl.fct.di.apdc.firstwebapp.util.AccessUser;
import pt.unl.fct.di.apdc.firstwebapp.util.Random;
import pt.unl.fct.di.apdc.firstwebapp.util.Utils;


@Path("/awards")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class GamificationResource {


	private static final String AWARDS_EVENT = "Awards_Event";
	private static final String AWARDS_REPORT = "Awards_Report";
	private static final String AWARDS_CONFIRMATION = "Awards_Confirm";
	private static final String AWARDS_SIGNUP = "Awards_Signup";
	private static final Logger LOG = Logger.getLogger(EventResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	public GamificationResource() {
	} // Nothing to be done here...

	@POST
	@Path("/badges")
	public Response doListBadges(AccessUser acess) {
		Transaction txn = datastore.beginTransaction();

		int numReports = ReportResource.numReportByUser(acess.username); 
		int numEvents = EventResource.numEventByUser(acess.username);
		int numConfirmations = ReportConfirmResource.numConfirmedByUser(acess.username);
		int numSignups = EventSignupResource.numSignupByUser(acess.username);


		LinkedList<Entity> toPut = new LinkedList<Entity>();;
		Entity reportAw = getReportAwards(acess.username, numReports);
		if(reportAw != null)
			toPut.add(reportAw); 
		Entity eventAw = getEventAwards(acess.username, numEvents);
		if(eventAw != null)
			toPut.add(eventAw); 
		Entity signAw = getSignupAwards(acess.username, numSignups);
		if(signAw != null)
			toPut.add(signAw); 
		Entity confirmationsAw = getConfirmationsAwards(acess.username, numConfirmations);
		if(confirmationsAw != null)
			toPut.add(confirmationsAw); 
		datastore.put(txn, toPut);
		LOG.info("Awards from user " + acess.username + " are loaded.");
		LOG.warning(txn.isActive()+ "2");

		txn.commit();

		List<Entity> list = new ArrayList<Entity>(4);
		Key userKey = KeyFactory.createKey("User", acess.username);
		Query awards_event = new Query(AWARDS_EVENT).setAncestor(userKey);
		Query awards_report = new Query(AWARDS_REPORT).setAncestor(userKey);
		Query awards_confirmation = new Query(AWARDS_CONFIRMATION).setAncestor(userKey);
		Query awards_signup = new Query(AWARDS_SIGNUP).setAncestor(userKey);
		List<Entity> awardsList = datastore.prepare(awards_event).asList(FetchOptions.Builder.withDefaults());
		if(!awardsList.isEmpty())
			list.add(awardsList.get(0));
		List<Entity> awardsList2 = datastore.prepare(awards_report).asList(FetchOptions.Builder.withDefaults());
		if(!awardsList2.isEmpty())
			list.add(awardsList2.get(0));
		List<Entity> awardsList3 = datastore.prepare(awards_confirmation).asList(FetchOptions.Builder.withDefaults());
		if(!awardsList3.isEmpty())
			list.add(awardsList3.get(0));
		List<Entity> awardsList4 = datastore.prepare(awards_signup).asList(FetchOptions.Builder.withDefaults());
		if(!awardsList4.isEmpty())
			list.add(awardsList4.get(0));
		return Response.ok(g.toJson(list)).build();


	}

	private Entity getConfirmationsAwards(String username, int numConfirmations) {
		Key userKey = KeyFactory.createKey("User", username);
		Key awardKey = KeyFactory.createKey(userKey, AWARDS_CONFIRMATION, username);

		try {
			Entity award = datastore.get(awardKey);
			String num = (String)award.getProperty("award_num");

			if(num.equals("10")) {
				if(numConfirmations > 25) {
					award.setProperty("award_user", username);
					award.setProperty("award_source", "Confirmation");
					award.setProperty("award_name", "SUPER-CHECKER");
					award.setProperty("award_num", "25");
					award.setUnindexedProperty("award_creation_time", Utils.getCreationTime()); 
					return award;
				}else 
					return award;

			}
		} catch (EntityNotFoundException e1) {

			if(numConfirmations > 25) {
				Entity award = new Entity(AWARDS_CONFIRMATION, awardKey);
				award.setProperty("award_user", username);
				award.setProperty("award_source", "Confirmation");
				award.setProperty("award_name", "SUPER-CHECKER");
				award.setProperty("award_num", "25");
				award.setUnindexedProperty("award_creation_time", Utils.getCreationTime()); 
				return award;
			}else if(numConfirmations > 10) {
				Entity award = new Entity(AWARDS_CONFIRMATION, awardKey);
				LOG.info("numSignups hallo" );
				award.setProperty("award_user", username);
				award.setProperty("award_source", "Confirmation");
				award.setProperty("award_name", "CHECKER");
				award.setProperty("award_num", "10");
				award.setUnindexedProperty("award_creation_time", Utils.getCreationTime()); 
				return award;
			}else 
				return null;
		}
		return null;
	}

	private Entity getSignupAwards(String username, int numSignups) {
		Key userKey = KeyFactory.createKey("User", username);
		Key awardKey = KeyFactory.createKey(userKey, AWARDS_SIGNUP, username);

		try {
			Entity award = datastore.get(awardKey);
			String num = (String)award.getProperty("award_num");

			if(num.equals("3")) {
				if(numSignups > 15) {
					award.setProperty("award_user", username);
					award.setProperty("award_source", "Signup");
					award.setProperty("award_name", "SUPER-PARTICIPATOR");
					award.setProperty("award_num", "15");
					award.setUnindexedProperty("award_creation_time", Utils.getCreationTime()); 
					return award;
				}else 
					return award;

			}
		} catch (EntityNotFoundException e1) {

			if(numSignups > 15) {
				Entity award = new Entity(AWARDS_SIGNUP, awardKey);
				award.setProperty("award_user", username);
				award.setProperty("award_source", "Signup");
				award.setProperty("award_name", "SUPER-PARTICIPATOR");
				award.setProperty("award_num", "15");
				award.setUnindexedProperty("award_creation_time", Utils.getCreationTime()); 
				return award;
			}else if(numSignups > 3) {
				Entity award = new Entity(AWARDS_SIGNUP, awardKey);
				LOG.info("numSignups hallo" );
				award.setProperty("award_user", username);
				award.setProperty("award_source", "Signup");
				award.setProperty("award_name", "PARTICIPATOR");
				award.setProperty("award_num", "3");
				award.setUnindexedProperty("award_creation_time", Utils.getCreationTime()); 
				return award;
			}else 
				return null;
		}
		return null;
	}

	private Entity getEventAwards(String username, int numEvents) {
		Key userKey = KeyFactory.createKey("User", username);
		Key awardKey = KeyFactory.createKey(userKey, AWARDS_EVENT, username);

		try {
			Entity award = datastore.get(awardKey);
			String num = (String)award.getProperty("award_num");

			if(num.equals("5")) {
				if(numEvents > 15) {
					award.setProperty("award_user", username);
					award.setProperty("award_source", "Event");
					award.setProperty("award_name", "SUPER-CREATOR");
					award.setProperty("award_num", "15");
					award.setUnindexedProperty("award_creation_time", Utils.getCreationTime()); 
					return award;
				}else 
					return award;

			}
		} catch (EntityNotFoundException e1) {

			if(numEvents > 15) {
				Entity award = new Entity(AWARDS_EVENT, awardKey);
				award.setProperty("award_user", username);
				award.setProperty("award_source", "Event");
				award.setProperty("award_name", "SUPER-CREATOR");
				award.setProperty("award_num", "15");
				award.setUnindexedProperty("award_creation_time", Utils.getCreationTime()); 
				return award;
			}else if(numEvents > 0) {
				Entity award = new Entity(AWARDS_EVENT, awardKey);
				award.setProperty("award_user", username);
				award.setProperty("award_source", "Event");
				award.setProperty("award_name", "CREATOR");
				award.setProperty("award_num", "5");
				award.setUnindexedProperty("award_creation_time", Utils.getCreationTime()); 
				return award;
			}else 
				return null;
		}
		return null;
	}


	private Entity getReportAwards(String username, int numReports) {
		Key userKey = KeyFactory.createKey("User", username);
		Key awardKey = KeyFactory.createKey(userKey, AWARDS_REPORT, username);

		try {
			Entity award = datastore.get(awardKey);
			String num = (String)award.getProperty("award_num");

			if(num.equals("5")) {
				if(numReports > 15) {
					award.setProperty("award_user", username);
					award.setProperty("award_source", "Report");
					award.setProperty("award_name", "SUPER-REPORTER");
					award.setProperty("award_num", "15");
					award.setUnindexedProperty("award_creation_time", Utils.getCreationTime()); 
					return award;
				}else 
					return award;

			}
		} catch (EntityNotFoundException e1) {

			if(numReports > 15) {
				Entity award = new Entity(AWARDS_REPORT, awardKey);
				award.setProperty("award_user", username);
				award.setProperty("award_source", "Report");
				award.setProperty("award_name", "SUPER-REPORTER");
				award.setProperty("award_num", "15");
				award.setUnindexedProperty("award_creation_time", Utils.getCreationTime()); 
				return award;
			}else if(numReports > 5) {
				Entity award = new Entity(AWARDS_REPORT, awardKey);
				LOG.info("numSignups hallo" );
				award.setProperty("award_user", username);
				award.setProperty("award_source", "Report");
				award.setProperty("award_name", "REPORTER");
				award.setProperty("award_num", "5");
				award.setUnindexedProperty("award_creation_time", Utils.getCreationTime()); 
				return award;
			}else 
				return null;
		}
		return null;
	}



}
