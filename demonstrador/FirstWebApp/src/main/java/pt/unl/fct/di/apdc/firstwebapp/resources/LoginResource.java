package pt.unl.fct.di.apdc.firstwebapp.resources;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PropertyProjection;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.appengine.repackaged.org.apache.commons.codec.digest.DigestUtils;
import com.google.gson.Gson;

import pt.unl.fct.di.apdc.firstwebapp.util.AccessUser;
import pt.unl.fct.di.apdc.firstwebapp.util.AuthToken;
import pt.unl.fct.di.apdc.firstwebapp.util.LoginData;
import pt.unl.fct.di.apdc.firstwebapp.util.Utils;

@Path("/login")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class LoginResource {

	public static final String TOKEN_EXPIRATION = "token_exp";
	public static final String TOKEN = "token";
	public static final String STATS_LAST = "user_stats_last";
	public static final String TIME = "user_login_time";
	public static final String CONTRY = "user_login_country";
	public static final String CITY = "user_login_city";
	public static final String LATLON = "user_login_latlon";
	public static final String STATS_FAILED = "user_stats_failed";
	public static final String STATS_LOGIN = "user_stats_logins";
	public static final String STATS_ENTITY = "UserStats";
	public static final String HOST = "user_login_host";
	public static final String IP = "user_login_ip";
	public static final String USER_LOG_KEY = "UserLog";
	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(LoginResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	public LoginResource() {
	} // Nothing to be done here...

	@GET
	@Path("/{username}")
	public Response checkUsernameAvailable(@PathParam("username") String username) {
		Key userKey = KeyFactory.createKey("User", username);
		try {
			Entity user = datastore.get(userKey);
			return Response.ok().entity(g.toJson(true)).build();
		} catch (EntityNotFoundException e) {
			return Response.ok().entity(g.toJson(false)).build();
		}
	}

	/**
	 * Realiza o login na base de dados através de um user e a pass
	 * 
	 * @param data
	 * @param request
	 * @param headers
	 * @return token para aceder ao resto das funcionalidades
	 */
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response doLoginV2(LoginData data, @Context HttpServletRequest request, @Context HttpHeaders headers) {
		LOG.info("Attempt to login user: " + data.username);

		Transaction txn = datastore.beginTransaction();
		Key userKey = KeyFactory.createKey("User", data.username);
		try {
			Entity user = datastore.get(userKey);
			// Obtain the user login statistics
			Query ctrQuery = new Query(STATS_ENTITY).setAncestor(userKey);
			List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withDefaults());
			Entity ustats = null;
			if (results.isEmpty()) {
				ustats = new Entity(STATS_ENTITY, user.getKey());
				ustats.setProperty(STATS_LOGIN, 0L);
				ustats.setProperty(STATS_FAILED, 0L);
			} else {
				ustats = results.get(0);
			}

			String hashedPWD = (String) user.getProperty("user_pwd");
			if (hashedPWD.equals(DigestUtils.sha512Hex(data.password))) {
				// Password correct

				// Construct the logs
				Entity log = new Entity(USER_LOG_KEY, user.getKey());
				log.setProperty(IP, request.getRemoteAddr());
				log.setProperty(HOST, request.getRemoteHost());
				log.setProperty(LATLON, headers.getHeaderString("X-AppEngine-CityLatLong"));
				log.setProperty(CITY, headers.getHeaderString("X-AppEngine-City"));
				log.setProperty(CONTRY, headers.getHeaderString("X-AppEngine-Country"));
				log.setProperty(TIME, new Date());
				// Get the user statistics and updates it
				ustats.setProperty(STATS_LOGIN, 1L + (long) ustats.getProperty(STATS_LOGIN));
				ustats.setProperty(STATS_FAILED, 0L);
				ustats.setProperty(STATS_LAST, new Date());

				// create token
				
				AuthToken token = new AuthToken(data.username, UserInfoResource.getUserInfo(data.username));
				Entity etoken = new Entity("UserToken", user.getKey());
				
				etoken.setProperty(TOKEN, token.tokenID);
				etoken.setProperty(TOKEN_EXPIRATION, token.expirationData);
				// Batch operation
				List<Entity> logs = Arrays.asList(log, ustats, etoken);
				datastore.put(txn, logs);
				txn.commit();

				// Return token
				LOG.info("User '" + data.username + "' logged in sucessfully.");
				return Response.ok(g.toJson(token)).build();
			} else {
				// Incorrect password
				ustats.setProperty(STATS_FAILED, 1L + (long) ustats.getProperty(STATS_FAILED));
				datastore.put(txn, ustats);
				txn.commit();

				LOG.warning("Wrong password for username: " + data.username);
				return Response.status(Status.FORBIDDEN).build();
			}
		} catch (EntityNotFoundException e) {
			// Username does not exist
			LOG.warning("Failed login attempt for username: " + data.username);
			return Response.status(Status.FORBIDDEN).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		}

	}

	@POST
	@Path("/user")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response checkUsernameAvailable(LoginData data) {

		Key userKey = KeyFactory.createKey("User", data.username);
		try {
			Entity user = datastore.get(userKey);
			String hashedPWD = (String) user.getProperty("user_pwd");
			if (hashedPWD.equals(DigestUtils.sha512Hex(data.password))) {

				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DATE, -1);
				Date yesterday = cal.getTime();

				// Obtain the user login statistics
				Filter propertyFilter = new FilterPredicate(TIME, FilterOperator.GREATER_THAN_OR_EQUAL,
						yesterday);
				Query ctrQuery = new Query(USER_LOG_KEY).setAncestor(KeyFactory.createKey("User", data.username))
						.setFilter(propertyFilter).addSort(TIME, SortDirection.DESCENDING);
				ctrQuery.addProjection(new PropertyProjection(TIME, Date.class));
				ctrQuery.addProjection(new PropertyProjection(IP, String.class));
				List<Entity> results = datastore.prepare(ctrQuery).asList(FetchOptions.Builder.withLimit(3));

				/*
				 * List<Date> loginDates = new ArrayList(); for(Entity userlog:results) {
				 * loginDates.add((Date) userlog.getProperty("user_login_time")); }
				 */ return Response.ok(g.toJson(results)).build();

			} else {
				LOG.warning("Wrong password for username: " + data.username);
				return Response.status(Status.FORBIDDEN).build();
			}
		} catch (EntityNotFoundException e) {
			// Username does not exist
			LOG.warning("Failed login attempt for username: " + data.username);
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@DELETE
	@Path("/logout")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response doLogOut(AccessUser acess) {
		LOG.info("Attempt to logout user: " + acess.username);
		Key userKey = KeyFactory.createKey("User", acess.username);
		
		Transaction txn = datastore.beginTransaction();
		try {
			Entity user = datastore.get(userKey);
			Filter propertyFilter = new FilterPredicate(TOKEN, FilterOperator.EQUAL,
					acess.tokenID);
			Query tokensQuery = new Query("UserToken").setAncestor(userKey).setFilter(propertyFilter);
			List<Entity> tokens = datastore.prepare(tokensQuery).asList(FetchOptions.Builder.withDefaults());
			if (tokens.isEmpty()) {
				return Response.status(Status.BAD_REQUEST).build();
			}
			Entity etoken = tokens.get(0);
			datastore.delete(txn, etoken.getKey());
			txn.commit();
			// Return token
			LOG.info("User '" + acess.username + "' logged out sucessfully.");
			return Response.ok().build();

		} catch (

		EntityNotFoundException e) {
			// Username does not exist
			LOG.warning("Failed login attempt for username: " + acess.username);
			return Response.status(Status.FORBIDDEN).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		}

	}

}
