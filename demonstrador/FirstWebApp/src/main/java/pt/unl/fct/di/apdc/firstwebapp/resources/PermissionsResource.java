package pt.unl.fct.di.apdc.firstwebapp.resources;

import java.util.logging.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.api.client.json.Json;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.TransactionOptions;
import com.google.gson.Gson;

import pt.unl.fct.di.apdc.firstwebapp.resources.BasePermissionsResource.PermissionEnum;
import pt.unl.fct.di.apdc.firstwebapp.util.AccessUser;
import pt.unl.fct.di.apdc.firstwebapp.util.Utils;
import pt.unl.fct.di.apdc.firstwebapp.util.permissions.Perm;
import pt.unl.fct.di.apdc.firstwebapp.util.permissions.Permission;
import pt.unl.fct.di.apdc.firstwebapp.util.permissions.SetPermissionsRequest;

@Path("/permissions")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class PermissionsResource {

	private static final String PERMISSIONS_KIND = "Permissions";
	private static final Logger LOG = Logger.getLogger(PermissionsResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	public PermissionsResource() {
	}

	@POST
	@Path("/{permOfUser}")
	public Response getPermissions(@PathParam("permOfUser") String permOfUser, AccessUser acess) {
		LOG.info("Getting permissions of " + permOfUser);
		Key accessUserKey = KeyFactory.createKey("User", acess.username);
		if (!Utils.checkToken(acess, accessUserKey)) {
			LOG.warning("TOKEN INVALIDO!!!");
			return Response.status(Status.BAD_REQUEST).build();
		}
		try {
			
			Object[] values = getPerm(permOfUser).getProperties().values().toArray();
			Object[] key = getPerm(permOfUser).getProperties().keySet().toArray();
			
			
			return Response.ok(g.toJson(new Perm(values, key))).build();
		} catch (EntityNotFoundException e) {
			LOG.warning("O username permissions invalido!!!");
			return Response.status(Status.BAD_REQUEST).build();
		}

	}

	public static Entity getPerm(String permOfUser) throws EntityNotFoundException {
		try {
			Key userPermKey = KeyFactory.createKey(PERMISSIONS_KIND, permOfUser);
			Entity permissions = datastore.get(userPermKey);
			return permissions;
		} catch (EntityNotFoundException e) {
			LOG.warning("Ainda nao tem permissoes");
			Entity user = datastore.get(KeyFactory.createKey("User", permOfUser));
			String role = (String) user.getProperty(RegisterResource.ROLE);
			LOG.warning("Role: "+ role);
			if (!Utils.nonEmptyField(role))
				return null;
			LOG.warning("Not NULL");
			Key roleKey = KeyFactory.createKey(BasePermissionsResource.ROLES_KIND, role);
			Key basePermKey = KeyFactory.createKey(roleKey, BasePermissionsResource.BASE_PERMISSIONS_KIND, role);
			Entity basePermissions = datastore.get(basePermKey);
			return basePermissions;
		}
	}

	@POST
	@Path("/changePermissions/{permOfUser}")
	public Response setPermissions(@PathParam("permOfUser") String permOfUser, SetPermissionsRequest perm) {
		LOG.info("Setting permissions of " + permOfUser);
		Key accessUserKey = KeyFactory.createKey("User", perm.acess.username);
		if (!Utils.checkToken(perm.acess, accessUserKey) || !perm.isValidRequest()) {
			LOG.warning("TOKEN INVALIDO!!!");
			return Response.status(Status.BAD_REQUEST).build();
		}
		if(!hasPermission(PermissionEnum.PERMISSION_ADMINISTRATIVE_UPDATE_PERMISSIONS, perm.acess.username)) {
			return Response.status(Status.FORBIDDEN).build();
		}
		
		Transaction txn = datastore.beginTransaction(TransactionOptions.Builder.withXG(true));
		Key permUserKey = KeyFactory.createKey(PERMISSIONS_KIND, perm.acess.username);
		Entity permUser;

		try {
			permUser = datastore.get(permUserKey);
		} catch (EntityNotFoundException e) {
			permUser = new Entity(permUserKey);
		}

		String role;
		Entity basePerm;
		try {
			Entity user = datastore.get(KeyFactory.createKey("User", perm.acess.username));
			role = (String) user.getProperty(RegisterResource.ROLE);
			basePerm = datastore.get(KeyFactory.createKey(BasePermissionsResource.BASE_PERMISSIONS_KIND, role));

		} catch (EntityNotFoundException e) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		boolean validChange = false;
		for (int i = 0; i < perm.key.length; i++) {
			;
			String permission = perm.key[i];
			int value = perm.values[i];
			if (Utils.nonEmptyField(permission) && (value == 0 || value == 1)) {
				if (basePerm.hasProperty(permission)
						&& Integer.parseInt((String) basePerm.getProperty(permission)) == 1) {
					permUser.setProperty(permission, value);
					validChange = true;
					LOG.info(permission + " : " + value + 1);
				}
			}
		}
		if (!validChange)
			return Response.status(Status.BAD_REQUEST).build();
		datastore.put(txn, permUser);
		txn.commit();
		return Response.ok().build();

	}
	
	public static boolean hasPermission(PermissionEnum perm, String username) {
		try {
			Entity permissions = getPerm(username);
			return Integer.parseInt(permissions.getProperty(perm.toString()).toString()) >= BasePermissionsResource.HAVE;
		} catch (EntityNotFoundException e) {
			LOG.info("ERRO");
			return false;
		}
	}

}
