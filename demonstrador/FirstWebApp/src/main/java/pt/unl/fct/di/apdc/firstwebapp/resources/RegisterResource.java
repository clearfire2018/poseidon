package pt.unl.fct.di.apdc.firstwebapp.resources;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.repackaged.org.apache.commons.codec.digest.DigestUtils;

import pt.unl.fct.di.apdc.firstwebapp.resources.BasePermissionsResource.PermissionEnum;
import pt.unl.fct.di.apdc.firstwebapp.util.RegisterData;
import pt.unl.fct.di.apdc.firstwebapp.util.Utils;
import pt.unl.fct.di.apdc.firstwebapp.util.user.RegistUserWithRole;

@Path("/register")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class RegisterResource {

	public static final String CREATION_TIME = "user_creation_time";
	public static final String CITY = "local_city";
	public static final String CONTACT = "user_contact";
	public static final String ROLE = "user_role";
	public static final String EMAIL = "user_email";
	public static final String PASSWORD = "user_pwd";
	public static final String NAME = "user_name";
	public static final String USER_KIND = "User";
	public static final Logger LOG = Logger.getLogger(RegisterResource.class.getName());
	public static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	
	
	public RegisterResource() { } //Nothing to be done here...
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response register(RegisterData data) {
		LOG.fine("Attempt to register user: " + data.username);
		
		if( ! data.validRegistration() ) {
			return Response.status(Status.BAD_REQUEST).entity("Missing or wrong parameter.").build();
		}
		
		Transaction txn = datastore.beginTransaction();
		try {
			// If the entity does not exist an Exception is thrown. Otherwise,
			Key userKey = KeyFactory.createKey(USER_KIND, data.username);
			Entity user = datastore.get(userKey);
			txn.rollback();
			return Response.status(Status.BAD_REQUEST).entity("User already exists.").build(); 
		} catch (EntityNotFoundException e) {
			Entity user = new Entity(USER_KIND, data.username);
			user.setProperty(NAME, data.name);
			user.setProperty(PASSWORD, DigestUtils.sha512Hex(data.password));
			user.setProperty(EMAIL, data.email);
			user.setProperty(ROLE, BasePermissionsResource.USER_ROLE);
			user.setUnindexedProperty(CONTACT, data.contact);
			user.setProperty(CITY, data.city);
			user.setUnindexedProperty(CREATION_TIME, Utils.getCreationTime());
			/**if(data.role == "admin") {
				Entity permissions = new Entity("Permission", data.username);
				setAllPermitionsToTrue(permissions);*/
			datastore.put(txn, user);
			LOG.info("User registered " + data.username);
			txn.commit();
			return Response.ok().build();
		} finally {
			if (txn.isActive() ) {
				txn.rollback();
			}
		}
	}
	
	@POST
	@Path("/withrole")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response registerUserWithRole(RegistUserWithRole  data) {
		LOG.fine("Attempt to register user: " + data.data.username);
	
		Key userKey = KeyFactory.createKey("User", data.access.username);
		if( ! data.validRequest() ) {
			return Response.status(Status.BAD_REQUEST).entity("Missing or wrong parameter.").build();
		}
		if (!PermissionsResource.hasPermission(PermissionEnum.PERMISSION_ADMINISTRATIVE_UPGRADE_USER,
				data.access.username)) {
			return Response.status(Status.FORBIDDEN).build();
		}
		try {
			Entity userAccess = datastore.get(userKey);
			String roleAccess = (String) userAccess.getProperty(RegisterResource.ROLE);
			Entity roleAccessEntity = datastore.get(KeyFactory.createKey("Role", roleAccess));
			Entity roleWantedEntity = datastore.get(KeyFactory.createKey("Role", data.data.role));
			if (Integer.parseInt(roleAccessEntity.getProperty(BasePermissionsResource.ROLE_LEVEL).toString()) <= Integer
					.parseInt(roleWantedEntity.getProperty(BasePermissionsResource.ROLE_LEVEL).toString())) {
				return Response.status(Status.FORBIDDEN).build();
			}
		}catch (Exception e) {
			return Response.status(Status.BAD_REQUEST).build(); 
		}
		
		Transaction txn = datastore.beginTransaction();
		try {
			Entity user = datastore.get(userKey);
			txn.rollback();
			return Response.status(Status.BAD_REQUEST).entity("User already exists.").build(); 
		} catch (EntityNotFoundException e) {
			Entity user = new Entity(USER_KIND, data.data.username);
			user.setProperty(NAME, data.data.name);
			user.setProperty(PASSWORD, DigestUtils.sha512Hex(data.data.password));
			user.setProperty(EMAIL, data.data.email);
			user.setProperty(ROLE, data.data.role.toLowerCase());
			user.setUnindexedProperty(CONTACT, data.data.contact);
			user.setProperty(CITY, data.data.city);
			user.setUnindexedProperty(CREATION_TIME, Utils.getCreationTime());
			/**if(data.role == "admin") {
				Entity permissions = new Entity("Permission", data.username);
				setAllPermitionsToTrue(permissions);*/
			datastore.put(txn, user);
			LOG.info("User registered " + data.data.username);
			txn.commit();
			return Response.ok().build();
		} finally {
			if (txn.isActive() ) {
				txn.rollback();
			}
		}
	}
	
}
