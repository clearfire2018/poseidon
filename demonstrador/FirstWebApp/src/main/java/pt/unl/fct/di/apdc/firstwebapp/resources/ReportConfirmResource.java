package pt.unl.fct.di.apdc.firstwebapp.resources;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.gson.Gson;

import pt.unl.fct.di.apdc.firstwebapp.resources.BasePermissionsResource.PermissionEnum;
import pt.unl.fct.di.apdc.firstwebapp.util.AccessUser;
import pt.unl.fct.di.apdc.firstwebapp.util.Utils;

@Path("/reports/confirm")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class ReportConfirmResource {

	private static final String CONFIRMED = "confirmed";
	private static final String REPORT_CONFIRMATION = "report_confirmation";
	private static final String REPORT_ID = "report_id";

	private static final String CONFIRMATION_CREATION_TIME = "confirmation_creation_time";
	private static final String CONFIRMATION_REPORT = "confirmation_report";
	private static final String CONFIRMATION_USER = "confirmation_user";

	private static final String CONFIRMATION_QUERY = "Confirmation";
	private static final String REPORT_QUERY = "Report";
	private static final String USER_QUERY = "User";

	private static final Logger LOG = Logger.getLogger(EventResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	public ReportConfirmResource() {
	}

	@POST
	@Path("/{idReport}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response likeEvent(AccessUser acess, @PathParam("idReport") String idReport) {
		LOG.fine("Attempt to confirm report " + idReport + " by user: " + acess.username);

		Key userKey = KeyFactory.createKey(USER_QUERY, acess.username);

		if (!Utils.checkToken(acess, userKey)|| !PermissionsResource
				.hasPermission(PermissionEnum.PERMISSION_REPORT_CONFIRM, acess.username)) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		
		Key reportKey = KeyFactory.createKey(REPORT_QUERY, idReport);
		try {
			datastore.get(reportKey);
		}catch (EntityNotFoundException e) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		Transaction txn = datastore.beginTransaction();
		
		Key confirmationKey = KeyFactory.createKey(reportKey, CONFIRMATION_QUERY, acess.username);
		Entity confirmation;
		boolean confirmed = true;
		try {
			confirmation = datastore.get(confirmationKey);
			confirmed = (boolean) confirmation.getProperty(CONFIRMED);
			confirmed = !confirmed;
			confirmation.setProperty(CONFIRMED, confirmed);
			confirmation.setProperty(CONFIRMATION_USER, acess.username);
			LOG.info("Confirmation status: " + confirmed);
		} catch (EntityNotFoundException e) {
			confirmation = new Entity(CONFIRMATION_QUERY, acess.username, reportKey);
			confirmation.setProperty(CONFIRMED, confirmed);
			confirmation.setProperty(CONFIRMATION_USER, acess.username);
			confirmation.setUnindexedProperty(CONFIRMATION_CREATION_TIME, Utils.getCreationTime());
			// List<Entity> toPutConfirmation = Arrays.asList(confirmation);
			LOG.info("Confirmation status" + true);
		}
		datastore.put(txn, confirmation);
		txn.commit();
		return Response.ok(confirmed).build();
	}

	@GET
	@Path("/{idReport}/num")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response numConfirmedReportRequest(@PathParam("idReport")String idReport) {
		return Response.ok(numConfirmedReport(idReport)).build();
	}

	@SuppressWarnings("deprecation")
	public static int numConfirmedReport(String idReport) {
		LOG.info("id: " + idReport);
		Key reportKey = KeyFactory.createKey(REPORT_QUERY, idReport);
		Filter filterConfirmedTrue = new FilterPredicate(CONFIRMED, FilterOperator.EQUAL, true);
		Query reportQuery = new Query(CONFIRMATION_QUERY).setAncestor(reportKey).setFilter(filterConfirmedTrue);
		// só pode ter no maximo 1.000 resultados
		int conf = datastore.prepare(reportQuery).countEntities();
		LOG.info("" + conf);
		return conf;
	}
	
public static int numConfirmedByUser(String username) {
		
		Filter filterConfirmedTrue = new FilterPredicate(CONFIRMED, FilterOperator.EQUAL, true);
		Filter userFilter = new FilterPredicate(CONFIRMATION_USER, FilterOperator.EQUAL, username);
		Filter filter = CompositeFilterOperator.and(filterConfirmedTrue, userFilter );
		Query reportQuery = new Query(CONFIRMATION_QUERY).setFilter(filter);
		int conf = datastore.prepare(reportQuery).countEntities();
		LOG.info("" + conf);
		return conf;
	}

}
