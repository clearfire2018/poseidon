package pt.unl.fct.di.apdc.firstwebapp.resources;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.logging.Log;

import com.google.api.services.storage.model.Bucket.Logging;
import com.google.appengine.api.datastore.Cursor;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Query.SortDirection;
import com.google.appengine.api.datastore.QueryResultList;
import com.google.gson.Gson;

import pt.unl.fct.di.apdc.firstwebapp.resources.BasePermissionsResource.PermissionEnum;
import pt.unl.fct.di.apdc.firstwebapp.util.AccessUser;
import pt.unl.fct.di.apdc.firstwebapp.util.Random;
import pt.unl.fct.di.apdc.firstwebapp.util.Utils;
import pt.unl.fct.di.apdc.firstwebapp.util.report.ReportData;
import pt.unl.fct.di.apdc.firstwebapp.util.report.ReportInfo;
import pt.unl.fct.di.apdc.firstwebapp.util.report.ListReports;

@Path("/reports")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class ReportResource {
	private static final String LEVEL = "report_level";
	private static final String RADIUS = "report_radius";
	/** Types */
	public static final int STATE_NOT_SOLVE = 0;
	public static final int STATE_IN_RESULTION = 1;
	public static final int STATE_SOLVED = 2;

	public static final int TYPE_FIRE = 0;
	public static final int TYPE_PERVENTION = 1;
	public static final int TYPE_TRASH = 2;

	private static final String STATE = "report_state";
	private static final String PUBPRIV = "report_pubpriv";
	private static final String USER_QUERY = "User";
	private static final String REPORT_QUERY = "Report";

	private static final String USER_404 = "User not exist.";
	private static final String INVALID_ID = "Invalid Id";

	private static final String CREATION_TIME = "report_creation_time";
	private static final String LONGITUDE = "report_longitude";
	private static final String LATITUDE = "report_latitude";
	private static final String ID = "report_id";
	private static final String CATEGORY = "report_category";
	private static final String DESCRIPTION = "report_description";

	private static final String DATE = "report_date";
	private static final String USERNAME = "report_user";
	private static final int PAGE_SIZE = 5;

	private static final Logger LOG = Logger.getLogger(ReportResource.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	public ReportResource() {
	} // Nothing to be done here...

	@POST
	@Path("/register")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response registerEvent(ReportData data) {
		LOG.warning("Attempt to register report by user: " + data.acess.username);
		// if (!data.validRegistration()) {
		// return Response.status(Status.BAD_REQUEST).entity("Missing or wrong
		// parameter.").build();
		// }
		LOG.warning("Attempt to token report by user: " + data.acess.tokenID);

		Key userKey = KeyFactory.createKey(USER_QUERY, data.acess.username);
		if (!Utils.checkToken(data.acess, userKey)) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		switch (Integer.parseInt(data.category)) {
		case TYPE_FIRE:
			if (!PermissionsResource.hasPermission(PermissionEnum.PERMISSION_REPORT_C0_ADD, data.acess.username))
				return Response.status(Status.FORBIDDEN).build();
		case TYPE_PERVENTION:
			if (!PermissionsResource.hasPermission(PermissionEnum.PERMISSION_REPORT_C1_ADD, data.acess.username))
				return Response.status(Status.FORBIDDEN).build();
		case TYPE_TRASH:
			if (!PermissionsResource.hasPermission(PermissionEnum.PERMISSION_REPORT_C2_ADD, data.acess.username))
				return Response.status(Status.FORBIDDEN).build();
		}

		LOG.warning("Attempt to regist report by user: " + data.category);

		Transaction txn = datastore.beginTransaction();

		// If the entity does not exist an Exception is thrown. Otherwise,
		String id = Random.key64();
		Entity report = new Entity(REPORT_QUERY, id);
		report.setProperty(USERNAME, data.acess.username);
		report.setProperty(CATEGORY, data.category);
		report.setProperty(DESCRIPTION, data.description);
		report.setProperty(DATE, Utils.getCreationTime());
		report.setProperty(LONGITUDE, data.marker.longitude);
		report.setProperty(LATITUDE, data.marker.latitude);
		report.setProperty(PUBPRIV, data.publicPrivate);

		report.setProperty(LEVEL, data.level == 0 ? 1 : data.level);
		if (data.state >0 && data.state <=2)
			report.setProperty(STATE, data.state);
		else
			report.setProperty(STATE, STATE_NOT_SOLVE);
		report.setProperty(RADIUS, data.radius);
		List<Entity> toPut = Arrays.asList(report);
		datastore.put(txn, toPut);
		LOG.info("Report by user " + data.acess.username + " registered with id " + id);
		txn.commit();
		return Response.ok(id).build();
	}

	@POST
	@Path("/{report}/change-state/{newState}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response registerEvent(@PathParam("report") String reportId, @PathParam("newState") String state,
			AccessUser acess) {
		LOG.warning("Attempt to register new state in report " + reportId + " by user: " + acess.username);

		Key userKey = KeyFactory.createKey(USER_QUERY, acess.username);
		if (!Utils.checkToken(acess, userKey)) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		
		try {
			Key reportKey = KeyFactory.createKey(REPORT_QUERY, reportId);
			Entity report = datastore.get(reportKey);
			switch (Integer.parseInt((String) report.getProperty(CATEGORY))) {
			case TYPE_FIRE:
				LOG.warning("Fogo");
				if (!PermissionsResource.hasPermission(PermissionEnum.PERMISSION_REPORT_C0_UPDATE_STATE,
						acess.username)) {
					return Response.status(Status.FORBIDDEN).build();
				}
				break;
			case TYPE_PERVENTION:
				LOG.warning("Fogo");
				if (!PermissionsResource.hasPermission(PermissionEnum.PERMISSION_REPORT_C1_UPDATE_STATE,
						acess.username)) {
					return Response.status(Status.FORBIDDEN).build();
				}
				break;
			case TYPE_TRASH:
				LOG.warning("Fogo");
				if (!PermissionsResource.hasPermission(PermissionEnum.PERMISSION_REPORT_C2_UPDATE_STATE,
						acess.username)) {
					
					return Response.status(Status.FORBIDDEN).build();
				}
				break;
			}
			Transaction txn = datastore.beginTransaction();
			report.setProperty(STATE, Integer.parseInt(state));

			datastore.put(txn, report);
			LOG.info("Report " + reportId + " registered new state " + state);
			txn.commit();
			return Response.ok(report).build();

		} catch (EntityNotFoundException u) {
			return Response.status(Status.BAD_REQUEST).entity(USER_404).build();
		}

	}

	@POST
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response infoReport(AccessUser acess, @PathParam("id") String id) {
		Key userKey = KeyFactory.createKey(USER_QUERY, acess.username);
		if (!Utils.checkToken(acess, userKey)) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		String username;
		String date;
		String description;
		String category;
		String latitude;
		String longitude;
		int numConfirmations;
		long state;
		String publicPrivate;
		long level;
		String radius;
		Key reportKey = KeyFactory.createKey(REPORT_QUERY, id);

		Entity report;
		try {
			report = datastore.get(reportKey);

			username = (String) report.getProperty(USERNAME);
			date = (String) report.getProperty(DATE);
			description = (String) report.getProperty(DESCRIPTION);
			category = (String) report.getProperty(CATEGORY);
			latitude = (String) report.getProperty(LATITUDE);
			longitude = (String) report.getProperty(LONGITUDE);
			state = (long) report.getProperty(STATE);
			publicPrivate = (String) report.getProperty(PUBPRIV);
			radius = (String) report.getProperty(RADIUS);
			level = (long) report.getProperty(LEVEL);
			numConfirmations = ReportConfirmResource.numConfirmedReport(id);
			ReportInfo info = new ReportInfo(id, username, date, description, latitude, longitude, category,
					numConfirmations, state, publicPrivate, radius, level);
			return Response.ok(g.toJson(info)).build();
		} catch (EntityNotFoundException e) {
			return Response.status(Status.BAD_REQUEST).build();
		}
	}

	@POST
	@Path("/listAll")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response allReports(@QueryParam("cursor") String cursor) {
		// LOG.warning("cursor: "+cursor);
		Query reportsQuery = new Query(REPORT_QUERY).addSort(DATE, SortDirection.DESCENDING);
		FetchOptions fetchOptions = FetchOptions.Builder.withLimit(PAGE_SIZE);
		if (cursor != null) {
			LOG.warning("cursor: " + cursor);
			fetchOptions = fetchOptions.startCursor(Cursor.fromWebSafeString(cursor));
		}
		PreparedQuery pq = datastore.prepare(reportsQuery);
		QueryResultList<Entity> reports = pq.asQueryResultList(fetchOptions);
		String newcursor = reports.getCursor().toWebSafeString();
		LOG.warning("newcursor: " + newcursor);

		for (Entity e : reports) {
			e.setProperty("report_numConfirm", ReportConfirmResource.numConfirmedReport(e.getKey().getName()));
		}
		ListReports list = new ListReports(reports, newcursor);
		return Response.ok(g.toJson(list)).build();
	}

	@DELETE
	@Path("/{report}/remove")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doRemoveReport(@PathParam("report") String reportToRemove, AccessUser acess) {
		Key userKey = KeyFactory.createKey(USER_QUERY, acess.username);
		if (!Utils.checkToken(acess, userKey)) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		Transaction txn = datastore.beginTransaction();
		Key reportKey = KeyFactory.createKey(REPORT_QUERY, reportToRemove);
		Entity report;
		try {
			report = datastore.get(reportKey);
			switch (Integer.parseInt((String) report.getProperty(CATEGORY))) {
			case TYPE_FIRE:
				if (!PermissionsResource.hasPermission(PermissionEnum.PERMISSION_REPORT_C0_DELETE, acess.username))
					return Response.status(Status.FORBIDDEN).build();
			case TYPE_PERVENTION:
				if (!PermissionsResource.hasPermission(PermissionEnum.PERMISSION_REPORT_C1_DELETE, acess.username))
					return Response.status(Status.FORBIDDEN).build();
			case TYPE_TRASH:
				if (!PermissionsResource.hasPermission(PermissionEnum.PERMISSION_REPORT_C2_DELETE, acess.username))
					return Response.status(Status.FORBIDDEN).build();
			}
			datastore.delete(txn, report.getKey());
			txn.commit();
			LOG.info("Report '" + reportToRemove + "' removed sucessfully.");
			return Response.ok().build();
		} catch (EntityNotFoundException e) {
			txn.rollback();
			return Response.status(Status.BAD_REQUEST).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		}

	}

	@POST
	@Path("/listUnsolvedState")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response listReportNotSolved(@QueryParam("cursor") String cursor) {
		Filter propertyFilter = new FilterPredicate(STATE, FilterOperator.LESS_THAN, STATE_SOLVED);
		Query reportsQuery = new Query(REPORT_QUERY).setFilter(propertyFilter);
		FetchOptions fetchOptions = FetchOptions.Builder.withLimit(PAGE_SIZE);
		if (cursor != null) {
			LOG.warning("cursor: " + cursor);
			fetchOptions = fetchOptions.startCursor(Cursor.fromWebSafeString(cursor));
		}
		PreparedQuery pq = datastore.prepare(reportsQuery);
		QueryResultList<Entity> reports = pq.asQueryResultList(fetchOptions);
		String newcursor = reports.getCursor().toWebSafeString();
		LOG.warning("newcursor: " + newcursor);
		ListReports list = new ListReports(reports, newcursor);
		return Response.ok(g.toJson(list)).build();
	}

	
	public static int numReportByUser(String username) {
		Filter filterUser = new FilterPredicate(USERNAME, FilterOperator.EQUAL, username);

		Query reportQuery = new Query(REPORT_QUERY).setFilter(filterUser);
		int conf = datastore.prepare(reportQuery).countEntities();
		LOG.info("" + conf);
		return conf;
	}
}
