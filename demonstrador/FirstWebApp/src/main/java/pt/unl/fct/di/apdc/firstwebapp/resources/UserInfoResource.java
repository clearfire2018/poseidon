package pt.unl.fct.di.apdc.firstwebapp.resources;

import java.util.List;

import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.repackaged.org.apache.commons.codec.digest.DigestUtils;
import com.google.gson.Gson;

import pt.unl.fct.di.apdc.firstwebapp.resources.BasePermissionsResource.PermissionEnum;
import pt.unl.fct.di.apdc.firstwebapp.util.AccessUser;
import pt.unl.fct.di.apdc.firstwebapp.util.Utils;
import pt.unl.fct.di.apdc.firstwebapp.util.user.ChangePassword;
import pt.unl.fct.di.apdc.firstwebapp.util.user.RemoveUser;
import pt.unl.fct.di.apdc.firstwebapp.util.user.UserInfo;

@Path("/user")
@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
public class UserInfoResource {

	private static final String PASSWORD = "user_pwd";
	private static final String USER_KEY = "User";
	private static final String LOCAL_QUERY = "UserLocal";
	private static final String CITY = "local_city";
	private static final String CONTACT = "user_contact";
	private static final String ROLE = "user_role";
	private static final String EMAIL = "user_email";
	private static final String NAME = "user_name";
	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(UserInfo.class.getName());
	private final Gson g = new Gson();
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	public UserInfoResource() {
	} // Nothing to be done here...

	@POST
	@Path("/info")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doInfoRequest(AccessUser acess) {
		LOG.info("Attempt to get Info of " + acess.username);
		Key userKey = KeyFactory.createKey(USER_KEY, acess.username);
		if (!Utils.checkToken(acess, userKey)) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		UserInfo info;
		String name;
		String role;
		String contact;
		String email;
		String city = "";
		try {
			Entity user = datastore.get(userKey);
			name = (String) user.getProperty(NAME);
			email = (String) user.getProperty(EMAIL);
			role = (String) user.getProperty(ROLE);
			contact = (String) user.getProperty(CONTACT);
			Query localQuery = new Query(LOCAL_QUERY).setAncestor(userKey);
			List<Entity> locals = datastore.prepare(localQuery).asList(FetchOptions.Builder.withDefaults());
			if (!locals.isEmpty()) {
				Entity local = locals.get(0);
				city = (String) local.getProperty(CITY);
			}
			info = new UserInfo(acess.username, role, contact, name, email, city);
			return Response.ok(g.toJson(info)).build();
		} catch (EntityNotFoundException e) {
			return Response.status(Status.FORBIDDEN).build();
		}

	}
	public static UserInfo getUserInfo(String username) {
		Key userKey = KeyFactory.createKey(USER_KEY, username);
		String name;
		String role;
		String contact;
		String email;
		String city = "";
		try {
			Entity user = datastore.get(userKey);
			name = (String) user.getProperty(NAME);
			email = (String) user.getProperty(EMAIL);
			role = (String) user.getProperty(ROLE);
			contact = (String) user.getProperty(CONTACT);
			Query localQuery = new Query(LOCAL_QUERY).setAncestor(userKey);
			List<Entity> locals = datastore.prepare(localQuery).asList(FetchOptions.Builder.withDefaults());
			if (!locals.isEmpty()) {
				Entity local = locals.get(0);
				city = (String) local.getProperty(CITY);
			}
			return new UserInfo(username, role, contact, name, email, city);
		} catch (EntityNotFoundException e) {
			return null;
		}
	}
	@POST
	@Path("/info/{username}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doInfoRequestV2(AccessUser acess, @PathParam("username") String username) {
		LOG.info("Attempt to get Info of " + acess.username);
		Key userKey = KeyFactory.createKey(USER_KEY, acess.username);
		if (!Utils.checkToken(acess, userKey)) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		UserInfo info;
		String name;
		String role;
		String contact;
		String email;
		String city = "";
		try {
			Key userInfoKey = KeyFactory.createKey(USER_KEY, username);
			Entity user = datastore.get(userInfoKey);
			name = (String) user.getProperty(NAME);
			email = (String) user.getProperty(EMAIL);
			role = (String) user.getProperty(ROLE);
			contact = (String) user.getProperty(CONTACT);
			Query localQuery = new Query(LOCAL_QUERY).setAncestor(userInfoKey);
			List<Entity> locals = datastore.prepare(localQuery).asList(FetchOptions.Builder.withDefaults());
			if (!locals.isEmpty()) {
				Entity local = locals.get(0);
				city = (String) local.getProperty(CITY);
			}
			info = new UserInfo(acess.username, role, contact, name, email, city);
			return Response.ok(g.toJson(info)).build();
		} catch (EntityNotFoundException e) {
			return Response.status(Status.FORBIDDEN).build();
		}

	}

	@POST
	@Path("/listusers")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doListUsers(AccessUser acess) {
		LOG.info("Attempt to list users");
		Key userKey = KeyFactory.createKey(USER_KEY, acess.username);
		if (!Utils.checkToken(acess, userKey) || !PermissionsResource
				.hasPermission(PermissionEnum.PERMISSION_ADMINISTRATIVE_LIST_USERS, acess.username)) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		Query usersQuery = new Query(USER_KEY);
		List<Entity> users = datastore.prepare(usersQuery).asList(FetchOptions.Builder.withDefaults());

		return Response.ok(g.toJson(users)).build();

	}

	@DELETE
	@Path("/removeuser/{userToRemove}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response doRemoveUser(@PathParam("userToRemove") String userToRemove, AccessUser acess) {
		Key userKey = KeyFactory.createKey(USER_KEY, acess.username);
		if (!Utils.checkToken(acess, userKey) || !PermissionsResource
				.hasPermission(PermissionEnum.PERMISSION_ADMINISTRATIVE_REMOVE_USERS, acess.username)) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		Transaction txn = datastore.beginTransaction();
		Key usertoremoveKey = KeyFactory.createKey(USER_KEY, userToRemove);
		try {
			Entity user = datastore.get(usertoremoveKey);
			datastore.delete(txn, user.getKey());
			txn.commit();
			LOG.info("User '" + userToRemove + "' removed sucessfully.");
			return Response.ok().build();

		} catch (

		EntityNotFoundException e) {
			// Username does not exist
			LOG.warning("Failed remove attempt for username: " + userToRemove);
			return Response.status(Status.FORBIDDEN).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		}

	}

	@POST
	@Path("/change-password")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response changePassword(ChangePassword change) {
		Transaction txn = datastore.beginTransaction();
		Key userKey = KeyFactory.createKey(USER_KEY, change.username);
		LOG.info("Attempt to change password: " + userKey);
		try {
			Entity user = datastore.get(userKey);
			boolean rightPassword = change.validPassword((String) user.getProperty(PASSWORD));
			if (rightPassword) {
				user.setProperty(PASSWORD, DigestUtils.sha512Hex(change.newPassword));
				LOG.info("Changed password of: " + user);
			} else
				return Response.status(Status.FORBIDDEN).build();
			datastore.put(txn, user);
			txn.commit();
			return Response.ok().build();
		} catch (EntityNotFoundException e) {
			LOG.warning("Failed change password attempt");
			return Response.status(Status.FORBIDDEN).build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
				return Response.status(Status.INTERNAL_SERVER_ERROR).build();
			}
		}

	}

	@POST
	@Path("/upgrade/{username}/{role}")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response changePassword(@PathParam("username") String username, @PathParam("role") String role,
			AccessUser access) {
		Key userAccessKey = KeyFactory.createKey("User", access.username);
		if (!Utils.checkToken(access, userAccessKey)) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		if (!PermissionsResource.hasPermission(PermissionEnum.PERMISSION_ADMINISTRATIVE_UPGRADE_USER,
				access.username)) {
			return Response.status(Status.FORBIDDEN).build();
		}
		Key userKey = KeyFactory.createKey("User", access.username);

		try {
			Entity userAccess = datastore.get(userKey);
			String roleAccess = (String) userAccess.getProperty(RegisterResource.ROLE);
			Entity roleAccessEntity = datastore.get(KeyFactory.createKey("Role", roleAccess));
			Entity roleWantedEntity = datastore.get(KeyFactory.createKey("Role", role));
			if (Integer.parseInt(roleAccessEntity.getProperty(BasePermissionsResource.ROLE_LEVEL).toString()) <= Integer
					.parseInt(roleWantedEntity.getProperty(BasePermissionsResource.ROLE_LEVEL).toString())) {
				return Response.status(Status.FORBIDDEN).build();
			}
			Transaction txn = datastore.beginTransaction();
			Entity user = datastore.get(userKey);
			user.setProperty(RegisterResource.ROLE, role);
			datastore.put(txn, user);
			txn.commit();
			return Response.ok().build();

		} catch (EntityNotFoundException e) {
			return Response.status(Status.BAD_REQUEST).build();
		}

	}
}
