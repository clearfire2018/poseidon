package pt.unl.fct.di.apdc.firstwebapp.util;

public class AccessUser {
	public String username;
	public String tokenID;
	
	public AccessUser() {
	}
	public boolean validToken(String username, long expirationData) {
		return this.username.equals(username) && expirationData > System.currentTimeMillis();
	}
}
