package pt.unl.fct.di.apdc.firstwebapp.util;

public class AcessUser {
	public String username;
	public String tokenID;
	
	public AcessUser() {
	}
	public boolean validToken(String username, long expirationData) {
		return this.username.equals(username) && expirationData > System.currentTimeMillis();
	}
}
