package pt.unl.fct.di.apdc.firstwebapp.util;

import java.util.UUID;

import pt.unl.fct.di.apdc.firstwebapp.util.user.UserInfo;

public class AuthToken {
	
	public static final long EXPIRATION_TIME = 1000*60*60*2; //2h
	
	public String username;
	public String tokenID;
	public long creationData;
	public long expirationData;
	public UserInfo user;
	
	public AuthToken() {
		this.creationData = System.currentTimeMillis();
		this.expirationData = this.creationData + AuthToken.EXPIRATION_TIME;
	
	}
	
	public AuthToken(String username, UserInfo user) {
		this.username = username;
		this.user = user;
		this.tokenID = UUID.randomUUID().toString();
		this.creationData = System.currentTimeMillis();
		this.expirationData = this.creationData + AuthToken.EXPIRATION_TIME;
	}
}
