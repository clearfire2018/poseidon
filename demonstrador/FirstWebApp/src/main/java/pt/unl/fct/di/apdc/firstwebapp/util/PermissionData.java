package pt.unl.fct.di.apdc.firstwebapp.util;

public class PermissionData {
	String[] permissions;
	int [] values;
	
	public PermissionData(String[] permissions, int[] values) {
		this.permissions = permissions;
		this.values = values;
	}

}
