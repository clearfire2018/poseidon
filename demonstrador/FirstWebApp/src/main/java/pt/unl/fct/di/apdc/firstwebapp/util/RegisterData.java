package pt.unl.fct.di.apdc.firstwebapp.util;

public class RegisterData {

	public String username;
	public String password;
	public String role;
	public String contact;
	public String name;
	public String email;
	public String city;


	public RegisterData() {

	}

	public RegisterData(String username, String password, String confirmation, String contact, String role, String name,
			String email, String country, String city) {
		this.username = username;
		this.password = password;
		this.role = role;
		this.contact = contact;
		this.name = name;
		this.email = email;
		this.city = city;

	}

	private boolean nonEmptyField(String field) {
		return field != null && !field.isEmpty();
	}

	public boolean validRegistration() {
		return nonEmptyField(username) && nonEmptyField(password)&& nonEmptyField(email);
	}
}
