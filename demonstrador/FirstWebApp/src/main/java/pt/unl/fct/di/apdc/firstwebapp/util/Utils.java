package pt.unl.fct.di.apdc.firstwebapp.util;

import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;

public class Utils {
	private static final Logger LOG = Logger.getLogger(Utils.class.getName());
	private static final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	public static boolean checkToken(AccessUser acess, Key userKey) {
		
		LOG.fine("Attempt to login user: " + acess.tokenID);
		Filter propertyFilter = new FilterPredicate("token", FilterOperator.EQUAL,
				acess.tokenID);
		Query tokensQuery = new Query("UserToken").setAncestor(userKey).setFilter(propertyFilter);
		List<Entity> tokens = datastore.prepare(tokensQuery).asList(FetchOptions.Builder.withDefaults());
		if (tokens.isEmpty())
			return false;
		LOG.fine("Attempt to login user: " + acess.tokenID);
		Transaction txn = datastore.beginTransaction();
		Entity token = tokens.get(0);
		token.setProperty("token_exp", new AuthToken().expirationData);
		datastore.put(txn, token);
		try {
			txn.commit();
		}catch (Exception e) {
			if(txn.isActive())
				txn.rollback();
		}
		return true;
	}

	public static String getCreationTime() {
		return Objects.toString(System.currentTimeMillis(), null);

	}

	public static boolean nonEmptyField(String field) {
		return field != null && !field.isEmpty();
	}
}
