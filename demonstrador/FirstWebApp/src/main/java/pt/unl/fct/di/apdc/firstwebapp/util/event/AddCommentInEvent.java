package pt.unl.fct.di.apdc.firstwebapp.util.event;

import pt.unl.fct.di.apdc.firstwebapp.util.AccessUser;
import pt.unl.fct.di.apdc.firstwebapp.util.Utils;

public class AddCommentInEvent {
	public AccessUser acess;
	public String comment;
	
	
	public boolean validComment() {
		return Utils.nonEmptyField(comment);
	}
}
