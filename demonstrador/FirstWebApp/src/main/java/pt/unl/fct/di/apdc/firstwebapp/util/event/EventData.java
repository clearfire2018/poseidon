package pt.unl.fct.di.apdc.firstwebapp.util.event;

import pt.unl.fct.di.apdc.firstwebapp.util.AccessUser;

public class EventData {

	public AccessUser acess;
	public String name;
	public String description;
	public String location;
	public String city;
	public String beginDate;
	public String endDate;


	public EventData() {

	}


	private boolean nonEmptyField(String field) {
		return field != null && !field.isEmpty();
	}
	/**
	 * In the app if no image is added -- no problem , put none
	 * If no description is added, the desc is the brief desc
	 * @return
	 */
	public boolean validRegistration() {
		return nonEmptyField(name) && nonEmptyField(description)  && nonEmptyField(acess.tokenID)
				&& nonEmptyField(acess.username); //*&& nonEmptyField(imgType);*/
		//&& nonEmptyField(latitude) && nonEmptyField(longitude);
	}





}
