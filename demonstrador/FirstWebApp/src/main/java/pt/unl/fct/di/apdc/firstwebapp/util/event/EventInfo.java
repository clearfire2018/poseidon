package pt.unl.fct.di.apdc.firstwebapp.util.event;

public class EventInfo {
	public String id;
	public String name;
	public String description;
	public String location;
	public String city;
	public long beginDate; 
	public long endDate;
	public int numLikes;
	public int numSigns;
	public String usernameAuthor;
	public boolean verify;

	
	public EventInfo(String id, String usernameAuthor, String name, String description, String location,
			String city, int numLikes, int numSigns, long beginDate, long endDate, boolean verify) {
		this.city = city;
		this.id = id;
		this.name = name;
		this.description = description;
		this.location = location;
		this.numLikes = numLikes;
		this.numSigns = numSigns;
		this.beginDate = beginDate;
		this.endDate = endDate;
		this.usernameAuthor = usernameAuthor;
		this.verify =verify;
	}
}
