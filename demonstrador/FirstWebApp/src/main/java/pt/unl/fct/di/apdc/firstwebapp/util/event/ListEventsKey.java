package pt.unl.fct.di.apdc.firstwebapp.util.event;

import java.util.List;

import com.google.appengine.api.datastore.Entity;

public class ListEventsKey {
	public List<Entity> events;
	public String cursor;
	
	public ListEventsKey(List<Entity> events, String cursor) {
		this.cursor = cursor;
		this.events = events;
	}
}