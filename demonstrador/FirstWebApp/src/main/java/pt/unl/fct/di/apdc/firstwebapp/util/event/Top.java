package pt.unl.fct.di.apdc.firstwebapp.util.event;

public class Top implements Comparable<Top>{
	
	private int numLikes;
	private String id;
	
	public Top(String id,  int numlikes) {
	this.id = id;
	this.numLikes = numlikes;
	}
	
	public int getNumLikes(){
		return numLikes;
	}
	public String getId() {
		return id;
	}
	
	
	public boolean equals(Top other) {
		return this.id.equals(other.id);
	}

	public String toString() {
		return id + " : " + numLikes;
	}
	public int compareTo(Top other) {
		return other.numLikes - this.numLikes;
	}
}
