package pt.unl.fct.di.apdc.firstwebapp.util.news;

import pt.unl.fct.di.apdc.firstwebapp.util.AccessUser;

public class NewsData {
	public AccessUser acess;
	public String title;
	public String subtitle;
	public String text;
	
	public NewsData() {
		
	}
	
	private boolean nonEmptyField(String field) {
		return field != null && !field.isEmpty();
	}
	
	public boolean validRegistration() {
		return  nonEmptyField(title) && nonEmptyField(text);
	}

}
