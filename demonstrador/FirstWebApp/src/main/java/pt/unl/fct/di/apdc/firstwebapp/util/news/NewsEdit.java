package pt.unl.fct.di.apdc.firstwebapp.util.news;

import pt.unl.fct.di.apdc.firstwebapp.util.AccessUser;

public class NewsEdit {
	public AccessUser acess;
	public String title;
	public String subtitle;
	public String text;
	
	public NewsEdit() {	
	}
	
	public boolean nonEmptyField(String field) {
		return field != null && !field.isEmpty();
	}
	
}
