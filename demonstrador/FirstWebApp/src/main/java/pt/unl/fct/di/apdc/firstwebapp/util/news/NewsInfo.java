package pt.unl.fct.di.apdc.firstwebapp.util.news;

public class NewsInfo {
	public String title;
	public String subtitle;
	public String text;
	public String imgType;
	public String username;
	

	public NewsInfo(String title, String subtitle, String text, String username, String imgType) {
		this.title = title;
		this.subtitle = subtitle;
		this.text = text;
		this.imgType = imgType;
		this.username = username;
	}
}
