package pt.unl.fct.di.apdc.firstwebapp.util.permissions;

import com.google.gson.JsonElement;

public class Perm  {
	public Object[] value;
	public Object[] keys;

	public Perm(Object[] value, Object[] keys) {
		this.value = value;
		this.keys = keys;
	}

}
