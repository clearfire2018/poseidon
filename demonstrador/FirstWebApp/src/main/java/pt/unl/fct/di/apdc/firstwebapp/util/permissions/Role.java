package pt.unl.fct.di.apdc.firstwebapp.util.permissions;

import pt.unl.fct.di.apdc.firstwebapp.util.Utils;

public class Role {
	public String role;
	public int level;
	
	public Role() {
	}
	public boolean isValidRole(int level) {
		return Utils.nonEmptyField(role) && level > 0;
	}
}
