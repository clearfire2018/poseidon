package pt.unl.fct.di.apdc.firstwebapp.util.report;

import java.util.List;

import com.google.appengine.api.datastore.Entity;

public class ListReports {
	public List<Entity> reports;
	public String cursor;
	
	public ListReports(List<Entity> reports, String cursor) {
		this.cursor = cursor;
		this.reports = reports;
	}
}
