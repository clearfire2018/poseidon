package pt.unl.fct.di.apdc.firstwebapp.util.report;

public class MarkerInfo {
	public String latitude;
	public String longitude;
	
	public MarkerInfo(String latitude,String longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}
}

