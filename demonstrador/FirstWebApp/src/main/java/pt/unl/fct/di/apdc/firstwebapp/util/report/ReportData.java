package pt.unl.fct.di.apdc.firstwebapp.util.report;

import pt.unl.fct.di.apdc.firstwebapp.util.AccessUser;

public class ReportData {
	
	public AccessUser acess;
	public String description;
	public String category;
	public int state; 
	public String publicPrivate;//private ou publ
	public String radius;//private ou publ
	public Marker marker;
	public int level;
	
	public ReportData() {

	}

	private boolean nonEmptyField(String field) {
		return field != null && !field.isEmpty();
	}
	
	

	public boolean validRegistration() {
		return  nonEmptyField(category) && marker != null;
	}

	
}
