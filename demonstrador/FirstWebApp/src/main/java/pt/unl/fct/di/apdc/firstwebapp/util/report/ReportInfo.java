package pt.unl.fct.di.apdc.firstwebapp.util.report;

public class ReportInfo {

	public String id;
	public String username;
	public String date;
	public String description;
	public String category;
	public MarkerInfo marker;
	public long state;
	public String publicPrivate;
	public int numConfirmations;
	public String radius;
	public long level;
	
	public ReportInfo(String id, String username, String date, String description,
			String latitude, String longitude, String category, int numConfirmations, long state, String publicPrivate, String radius, long level) {
		this.id = id;
		this.username = username;
		this.date = date;
		this.description = description;
		this.category = category;
		this.marker = new MarkerInfo(latitude, longitude);
		this.publicPrivate = publicPrivate;
		this.numConfirmations = numConfirmations;
		this.state = state;
		this.radius = radius;
		this.level = level;
	}
	
	
	
}
