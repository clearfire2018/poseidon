package pt.unl.fct.di.apdc.firstwebapp.util.user;
import com.google.appengine.repackaged.org.apache.commons.codec.digest.DigestUtils;

public class ChangePassword {
	public String username;
	public String oldPassword;
	public String newPassword;
	

	public ChangePassword() {
	}
	
	public boolean validPassword(String password) {
		return DigestUtils.sha512Hex(this.oldPassword).equals(password) ;
	}
	
	
}