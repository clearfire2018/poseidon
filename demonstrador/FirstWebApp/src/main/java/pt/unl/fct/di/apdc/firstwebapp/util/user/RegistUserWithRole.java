package pt.unl.fct.di.apdc.firstwebapp.util.user;

import pt.unl.fct.di.apdc.firstwebapp.util.AccessUser;
import pt.unl.fct.di.apdc.firstwebapp.util.RegisterData;
import pt.unl.fct.di.apdc.firstwebapp.util.Utils;

public class RegistUserWithRole {
	public RegisterData data;
	public AccessUser access;
	
	public boolean validRequest() {
		return data.validRegistration() && Utils.nonEmptyField(data.role);
	}
}
