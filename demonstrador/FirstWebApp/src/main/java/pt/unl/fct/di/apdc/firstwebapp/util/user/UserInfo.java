package pt.unl.fct.di.apdc.firstwebapp.util.user;

public class UserInfo {

	public String username;
	public String role;
	public String contact;
	public String name;
	public String email;
	public String city;
	
	public UserInfo(String username, String role, String contact,String name, String email, String city) {
		this.username = username;
		this.role = role;
		this.contact = contact;
		this.name = name;
		this.email = email;
		this.city = city;

	}
}