import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Route, Switch} from 'react-router-dom';
import HomePage from './components/pages/HomePage';
import LoginPage from './components/pages/user/LoginPage';
import SignupPage from './components/pages/user/SignupPage';
import DashboardPage from './components/pages/user/DashboardPage';
import UserRoute from './components/routes/UserRoute';
import GuestRoute from './components/routes/GuestRoute';
import TopNavigation from "./components/navigation/TopBar";
import EventsPage from "./components/pages/events/EventsPage";
import EventPage from "./components/pages/events/EventPage";
import CreateEventPage from "./components/pages/events/CreateEventPage";
import CreateReportPage from "./components/pages/reports/CreateReportPage";
import ReportsPage from "./components/pages/reports/ReportsPage";
import NotFound from "./components/pages/NotFound";
import ChangePassword from "./components/pages/user/ChangePassword";
import AccountDetails from "./components/pages/user/AccountDetails"
import ReportPage from "./components/pages/reports/ReportPage";
import AdminPanel from "./components/pages/admin/AdminPanel";
import NewsPage from "./components/pages/news/NewsPage";
import CreateNewsPage from "./components/pages/news/CreateNewsPage";
import axios from "axios/index";
import AdminRoute from "./components/routes/AdminRoute";
import PermissionsTable from "./components/pages/admin/PermissionsTable";


class App extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    componentWillMount() {
        axios.post('https://henrique-45707.appspot.com/rest/user/info', {
            username: localStorage.getItem("user"),
            tokenID: localStorage.getItem("token"),
        }).then((response) => {
            this.setState({
                role: response.data.role
            }, () => {
                this.setState({
                    loading: false
                })
            })
        })
    }

    render() {
        const {location} = this.props;
        const isAuthenticated = !!localStorage.getItem("token");
        const isAdmin = (this.state.role === "admin");

        if (!this.state.loading) {
            return (
                <div>
                    <Route
                        path={['/report/:id' ,'/promote_worker/:id','/dashboard', '/events', '/reports', '/create_event', '/create_report', '/event/:id', '/change_password', '/account_details', '/news']}
                        component={TopNavigation}/>
                    <Switch>
                        <Route location={location} path="/" exact component={HomePage}/>
                        <GuestRoute location={location} isAuthenticated={isAuthenticated} path="/login"
                                    component={LoginPage}/>
                        <GuestRoute location={location} isAuthenticated={isAuthenticated} path="/signup"
                                    component={SignupPage}/>
                        <UserRoute location={location} isAuthenticated={isAuthenticated} path="/dashboard"
                                   component={DashboardPage}/>
                        <Route location={location} isAuthenticated={isAuthenticated} path="/events"
                               component={EventsPage}/>
                        <Route location={location} isAuthenticated={isAuthenticated} path="/reports"
                               component={ReportsPage}/>
                        <Route location={location} isAuthenticated={isAuthenticated} path="/news"
                               component={NewsPage}/>
                        <UserRoute location={location} isAuthenticated={isAuthenticated} path="/create_event"
                                   component={CreateEventPage}/>
                        <UserRoute location={location} isAuthenticated={isAuthenticated} path="/create_report"
                                   component={CreateReportPage}/>
                        <AdminRoute location={location} isAuthenticated={isAuthenticated} isAdmin={isAdmin} path="/create_news"
                                   component={CreateNewsPage}/>
                        <Route location={location} path="/event/:id" component={EventPage}/>
                        <Route location={location} path="/report/:id" component={ReportPage}/>
                        <UserRoute location={location} path="/change_password" component={ChangePassword}
                                   isAuthenticated={isAuthenticated}/>
                        <UserRoute location={location} isAuthenticated={isAuthenticated} path="/account_details"
                                   component={AccountDetails}/>
                        <UserRoute location={location} isAuthenticated={isAuthenticated} path="/admin_panel"
                                   component={AdminPanel}/>
                        <UserRoute location={location} isAuthenticated={isAuthenticated} path="/promote_worker/:user"
                                   component={PermissionsTable}/>
                        <Route component={NotFound}/>
                    </Switch>
                </div>
            )
        } else return null;
    }

}

App.propTypes = {
    location: PropTypes.shape({
        pathname: PropTypes.string.isRequired
    }).isRequired
};

export default App;