import React from "react";
import {Form, Button, Message, Segment} from "semantic-ui-react";
import InlineError from "../messages/InlineError";

class ChangePasswordForm extends React.Component {
    state = {
        data: {
            current_password: "",
            new_password: "",
            new_password_repeat: ""
        },
        loading: false,
        errors: {}
    };

    onChange = e =>
        this.setState({
            data: {...this.state.data, [e.target.name]: e.target.value}
        });

    onSubmit = () => {
        const errors = this.validate(this.state.data);

        this.setState({errors});

        if (Object.keys(errors).length === 0) {
            this.setState({loading: true});
            this.props.submit(this.state.data);
        }
    };

    validate = data => {
        const errors = {};

        if (!data.current_password) errors.current_password = "Please enter current your password";
        if (!data.new_password) errors.new_password = "Please enter your new password";
        if (!data.new_password_repeat) errors.new_password_repeat = "Please re-enter your new password";
        if (data.new_password !== data.new_password_repeat) errors.new_password_repeat = "Passwords don't match";

        return errors;
    };

    render() {
        const {data, errors, loading} = this.state;

        return (

            <Form onSubmit={this.onSubmit} loading={loading}>
                <Segment stacked>

                    {errors.global && (
                        <Message negative>
                            <Message.Header>Something went wrong</Message.Header>
                            <p>{errors.global}</p>
                        </Message>
                    )}

                    <Form.Field error={!!errors.current_password}>
                        <Form.Input
                            fluid
                            icon='lock'
                            iconPosition='left'
                            placeholder='Current Password'
                            type='password'
                            id='current_password'
                            name='current_password'
                            value={data.current_password}
                            onChange={this.onChange}
                        />
                        {errors.current_password && <InlineError text={errors.current_password}/>}
                    </Form.Field>

                    <Form.Field error={!!errors.new_password}>
                        <Form.Input
                            fluid
                            icon='lock'
                            iconPosition='left'
                            placeholder='New Password'
                            type='password'
                            id='new_password'
                            name='new_password'
                            value={data.new_password}
                            onChange={this.onChange}
                        />
                        {errors.new_password && <InlineError text={errors.new_password}/>}
                    </Form.Field>

                    <Form.Field error={!!errors.new_password_repeat}>
                        <Form.Input
                            fluid
                            icon='lock'
                            iconPosition='left'
                            placeholder='Confirm New Password'
                            type='password'
                            id='new_password_repeat'
                            name='new_password_repeat'
                            value={data.new_password_repeat}
                            onChange={this.onChange}
                        />
                        {errors.new_password_repeat && <InlineError text={errors.new_password_repeat}/>}
                    </Form.Field>

                    <Button primary fluid size='large'>Change Password</Button>

                </Segment>
            </Form>
        );
    }
}

export default ChangePasswordForm;
