import React from "react";
import PropTypes from "prop-types";
import {Form, Button, Message, TextArea} from "semantic-ui-react";
import InlineError from "../messages/InlineError";
import Calendar from 'react-calendar';
import {RegionDropdown} from 'react-country-region-selector';


class EventForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {
                name: "",
                description: "",
                location: ""
            },
            image: null,
            date: new Date(),
            city: '',
            loading: false,
            errors: {},
            loaded: false
        };
        this.handleFileChange = this.handleFileChange.bind(this)
    }


    select_region = value => {
        this.setState({city: value});
    };

    onChange_calendar = date => {
        this.setState({date});
    };


    handleFileChange(e) {

        this.setState({
            image: e.target.files[0]
        }, () => {
            this.setState({
                loaded: true
            })
        })
    };

    onChange = e =>
        this.setState({
            ...this.state,
            data: {...this.state.data, [e.target.name]: e.target.value}
        });

    onSubmit = e => {
        e.preventDefault();
        const errors = this.validate(this.state.data);

        this.setState({errors});
        if (Object.keys(errors).length === 0) {
            
            this.setState({loading: true});
            const param = {

                data: this.state.data,
                date: this.state.date,
                city: this.state.city,
                img: this.state.image
            };
            this.props.submit(param)

        }
    };

    validate = data => {
        const errors = {};
        
        
        if(!data.name){
            
            errors.name = "Please insert a title for your event"
        }
        
        if(!data.description){
            
            errors.description = "Please insert a description"
        }
        
        if(this.state.date == null){
            
            errors.data = "Please insert a valid date";
        }
        
        return errors;
    };

    render() {
        const {data, errors, loading} = this.state;
        return (
            <Form onSubmit={this.onSubmit} loading={loading} name="input">
                <Form.Group  widths='equal'>
                    <Form.Field>
                        {errors.global && (
                            <Message negative>
                                <Message.Header>Something went wrong</Message.Header>
                                <p>{errors.global}</p>
                            </Message>
                        )}

                        <Form.Field error={!!errors.username}>
                            <label htmlFor="city">Name</label>
                            <input
                                type="text"
                                id="name"
                                name="name"
                                value={data.name}
                                onChange={this.onChange}
                            />
                            {errors.name && <InlineError text={errors.name}/>}
                        </Form.Field>

                        <Form.Field style={{paddingTop: '1em', paddingBottom: '1em'}} error={!!errors.description}>
                            <label htmlFor="description">Description</label>
                            <TextArea
                                type="text"
                                id="description"
                                name="description"
                                value={data.description}
                                onChange={this.onChange}
                                style={{maxHeight:200}}
                            />
                            {errors.description && <InlineError text={errors.description}/>}
                        </Form.Field>

                        <Form.Group widths='equal'>
                            <Form.Field error={!!errors.location}>
                                <label htmlFor="location">Location</label>
                                <input
                                    type="text"
                                    id="location"
                                    name="location"
                                    value={data.location}
                                    onChange={this.onChange}
                                />
                                {errors.location && <InlineError text={errors.location}/>}
                            </Form.Field>

                            <Form.Field>
                                <label htmlFor="city">City</label>
                                <RegionDropdown
                                    country={"Portugal"}
                                    value={this.state.city}
                                    onChange={(value) => this.select_region(value)}
                                />
                            </Form.Field>

                        </Form.Group>
                        <Form.Field>
                            <label htmlFor="image">Image</label>
                            <input type="file" onChange={this.handleFileChange}/>
                        </Form.Field>
                    </Form.Field>
                    <Form.Field>
                        <Form.Field style={{marginLeft: '20%'}}>
                            <label htmlFor="date">Date</label>
                            <Calendar
                                selectRange={true}
                                onChange={this.onChange_calendar}
                                value={this.state.date}

                            />
                            {errors.date && <InlineError text={errors.date}/>} 
                        </Form.Field>
                    </Form.Field>
                </Form.Group>




                <Button primary>Submit</Button>
            </Form>
        );
    }
}

EventForm.propTypes = {
    submit: PropTypes.func.isRequired
};

export default EventForm;