import React from "react";
import PropTypes from "prop-types";
import {Form, Button, Message, Segment} from "semantic-ui-react";
import InlineError from "../messages/InlineError";

class LoginForm extends React.Component {
    state = {
        data: {
            username: "",
            password: ""
        },
        loading: false,
        errors: {}
    };

    onChange = e =>
        this.setState({
            data: {...this.state.data, [e.target.name]: e.target.value}
        });

    onSubmit = () => {
        const errors = this.validate(this.state.data);
        
        this.setState({errors});

        if (Object.keys(errors).length === 0) {
            this.setState({loading: true});
            this.props.submit(this.state.data);
        }
    };

    validate = data => {
        const errors = {};

        if (!data.username) errors.username = "Please enter your username";
        if (!data.password) errors.password = "Please enter your password";

        return errors;
    };

    render() {
        const {data, errors, loading} = this.state;

        return (

            <Form onSubmit={this.onSubmit} loading={loading} size='large'>
                <Segment stacked>

                    {errors.global && (
                        <Message negative>
                            <Message.Header>Something went wrong</Message.Header>
                            <p>{errors.global}</p>
                        </Message>
                    )}

                    <Form.Field error={!!errors.username}>
                        <Form.Input
                            fluid
                            icon='user'
                            iconPosition='left'
                            placeholder='Username'
                            id='username'
                            name='username'
                            value={data.username}
                            onChange={this.onChange}
                        />
                        {errors.username && <InlineError text={errors.username}/>}
                    </Form.Field>

                    <Form.Field error={!!errors.password}>
                        <Form.Input
                            fluid
                            icon='lock'
                            iconPosition='left'
                            placeholder='Password'
                            type='password'
                            id='password'
                            name='password'
                            value={data.password}
                            onChange={this.onChange}
                        />
                        {errors.password && <InlineError text={errors.password}/>}
                    </Form.Field>

                    <Button primary fluid size='large'>Login</Button>

                </Segment>
            </Form>
        );
    }
}

LoginForm.propTypes = {
    submit: PropTypes.func.isRequired
};

export default LoginForm;
