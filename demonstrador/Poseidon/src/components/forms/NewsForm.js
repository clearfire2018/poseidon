import React from "react";
import {Form, Button, TextArea} from "semantic-ui-react";
import InlineError from "../messages/InlineError";

class NewsForm extends React.Component {
    state = {
        data: {
            title: "",
            subtitle: "",
            text: "",
        },
        city: "",
        loading: false,
        errors: {}
    };

    onChange = e =>
        this.setState({
            ...this.state,
            data: {...this.state.data, [e.target.name]: e.target.value}
        });

    onSubmit = e => {
        e.preventDefault();
        const errors = this.validate(this.state.data);
        this.setState({errors});
        if (Object.keys(errors).length === 0) {
            this.setState({loading: true});
            this.props
                .submit(this.state.data, this.state.city)

        }
    };

    validate = data => {
        const errors = {};

        return errors;
    };

    render() {
        const {data, errors, loading} = this.state;

        return (
            <Form onSubmit={this.onSubmit} loading={loading}>

                <Form.Group>
                    <Form.Field error={!!errors.title}>
                        <label htmlFor="title">Title</label>
                        <input
                            type="text"
                            id="title"
                            name="title"
                            value={data.title}
                            onChange={this.onChange}
                        />
                        {errors.title && <InlineError text={errors.title}/>}
                    </Form.Field>

                    <Form.Field error={!!errors.subtitle}>
                        <label htmlFor="subtitle">Subtitle</label>
                        <input
                            type="text"
                            id="subtitle"
                            name="subtitle"
                            value={data.subtitle}
                            onChange={this.onChange}/>
                        {errors.subtitle && <InlineError text={errors.subtitle}/>}
                    </Form.Field>
                </Form.Group>

                <Form.Field error={!!errors.text}>
                    <label htmlFor="text">Text</label>
                    <TextArea
                        type="text"
                        id="text"
                        name="text"
                        value={data.text}
                        onChange={this.onChange}
                    />
                    {errors.text && <InlineError text={errors.text}/>}
                </Form.Field>
                <Button primary>Add article</Button>
            </Form>
        );
    }
}


export default NewsForm;