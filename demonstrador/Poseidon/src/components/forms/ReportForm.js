import React from "react";
import {Button, Dropdown, Form, TextArea} from "semantic-ui-react";
import InlineError from "../messages/InlineError";
import ReportFormMapContainer from "../pages/map/ReportFormMapContainer";

class ReportForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {
                title: "",
                description: "",
                image: "",
                imgType: "",
                category: ""
            },
            done: false,
            radius: 0,
            dropCategory: "",
            loading: false,
            errors: {}
        }
    }

    categoryOptions = [
        {
            text: 'Prevenção',
            value: 1
        }, {
            text: 'Lixo',
            value: 2
        }, {
            text: 'Incêndio',
            value: 0
        }
    ];

    handleMarker(lat, lng) {
        this.setState({
            lat: lat, lng: lng
        }, () => {
            this.setState({
                done: true
            });
        });
    };

    fileChangedHandler = (event) => {
        const file = event.target.files[0];
        this.setState({data: {...this.state.data, image: file}});
    };

    onChange = e =>
        this.setState({
            ...this.state,
            data: {...this.state.data, [e.target.name]: e.target.value}
        });

    onChangeRadius = e => {
        const value = parseInt(e.target.value, 10);
        this.setState({
            radius: value
        })
    };

    onSubmit = e => {
        e.preventDefault();
        const errors = this.validate(this.state.data);
        this.setState({errors});
        if (Object.keys(errors).length === 0 && this.state.done === true) {
            this.setState({loading: true});
            console.log(this.state.data);
            console.log(this.state.lat + " , " + this.state.lng);
            const params = {
                data: this.state.data,
                lat: this.state.lat,
                lng: this.state.lng
            };
            this.props
                .submit(params)
        }
    };

    handleDropdown = (e, {value}) => this.setState({data: {...this.state.data, category: value}});

    validate = data => {
        const errors = {};

        return errors;
    };

    render() {
        const {data, errors, loading} = this.state;

        return (
            <Form onSubmit={this.onSubmit} loading={loading}>

                <Form.Field error={!!errors.description}>
                    <label htmlFor="description">Description</label>
                    <TextArea
                        type="text"
                        id="description"
                        name="description"
                        value={data.description}
                        onChange={this.onChange}
                    />
                    {errors.description && <InlineError text={errors.description}/>}
                </Form.Field>

                <Form.Group widths='equal'>
                    <Form.Field>
                        <label htmlFor="category">Category</label>

                        <Dropdown
                            onChange={this.handleDropdown}
                            options={this.categoryOptions}
                            placeholder='Select category'
                            selection
                            value={this.category}
                        />
                    </Form.Field>
                    <Form.Input fluid label='Latitude' readOnly value={this.state.lat}/>
                    <Form.Input fluid label='Longitude' readOnly value={this.state.lng}/>
                    <Form.Input input="range" max="2000" min="" fluid label='Radius' placeholder="Max 2000m"
                                name="radius" error={this.state.error} onChange={this.onChangeRadius}
                                value={this.state.radius}/>

                </Form.Group>

                <Form.Field>
                    <ReportFormMapContainer lat={this.state.lat} lng={this.state.lng} radius={this.state.radius}
                                            handleMarker={this.handleMarker.bind(this)}/>
                </Form.Field>
                <Button primary>Report!</Button>
            </Form>
        );
    }
}

export default ReportForm;