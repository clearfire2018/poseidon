import React from "react";
import PropTypes from "prop-types";
import { Form, Button } from "semantic-ui-react";
import isEmail from "validator/lib/isEmail";
import InlineError from "../messages/InlineError";
import {RegionDropdown} from "react-country-region-selector";

class SignupForm extends React.Component {
    state = {
        data: {
            name: "",
            username: "",
            email: "",
            password: "",
            password_repeat: "",
            contact: ""
        },
        city:"",
        loading: false,
        errors: {}
    };

    select_region = value => {
        this.setState({city: value});
    };

    onChange = e =>
        this.setState({
            ...this.state,
            data: { ...this.state.data, [e.target.name]: e.target.value }
        });

    onSubmit = e => {
        e.preventDefault();
        const errors = this.validate(this.state.data);
        this.setState({ errors });
        if (Object.keys(errors).length === 0) {
            this.setState({ loading: true });
            const params = {
                data: this.state.data,
                city: this.state.city,
            }
            this.props
                .submit(params)

        }
    };

    validate = data => {
        const errors = {};

        if (!isEmail(data.email)) errors.email = "Invalid email";
        if (!data.password) errors.password = "Can't be blank";
        if (data.password !== data.password_repeat) errors.password_repeat = "Passwords don't match";

        return errors;
    };

    render() {
        const { data, errors, loading } = this.state;

        return (
            <Form onSubmit={this.onSubmit} loading={loading}>

                <Form.Field error={!!errors.username}>
                    <label htmlFor="city">Username</label>
                    <input
                        type="text"
                        id="username"
                        name="username"
                        value={data.username}
                        onChange={this.onChange}
                    />
                    {errors.name && <InlineError text={errors.name} />}
                </Form.Field>

                <Form.Field error={!!errors.name}>
                    <label htmlFor="name">Name</label>
                    <input
                        type="text"
                        id="name"
                        name="name"
                        value={data.name}
                        onChange={this.onChange}
                    />
                    {errors.name && <InlineError text={errors.name} />}
                </Form.Field>

                <Form.Field error={!!errors.contact}>
                    <label htmlFor="contact">Contact</label>
                    <input
                        type="text"
                        id="contact"
                        name="contact"
                        value={data.contact}
                        onChange={this.onChange}
                    />
                    {errors.contact && <InlineError text={errors.contact} />}
                </Form.Field>


                <Form.Field error={!!errors.email}>
                    <label htmlFor="email">Email</label>
                    <input
                        type="email"
                        id="email"
                        name="email"
                        placeholder="example@example.com"
                        value={data.email}
                        onChange={this.onChange}
                    />
                    {errors.email && <InlineError text={errors.email} />}
                </Form.Field>
                
            <Form.Group widths='equal'>
                <Form.Field error={!!errors.password}>
                    <label htmlFor="password">Password</label>
                    <input
                        type="password"
                        id="password"
                        name="password"
                        placeholder="Enter Password"
                        value={data.password}
                        onChange={this.onChange}
                    />
                    {errors.password && <InlineError text={errors.password} />}
                </Form.Field>

                <Form.Field error={!!errors.password_repeat}>
                    <label htmlFor="password">Confirm Password</label>
                    <input
                        type="password"
                        id="password_repeat"
                        name="password_repeat"
                        placeholder="Confirm Password"
                        value={data.password_repeat}
                        onChange={this.onChange}
                    />
                    {errors.password_repeat && <InlineError text={errors.password_repeat} />}
                </Form.Field>
            </Form.Group>
            
                <Form.Field error={!!errors.city}>
                    <label htmlFor="city">City</label>
                    <RegionDropdown
                        country={"Portugal"}
                        value={this.state.city}
                        onChange={(value) => this.select_region(value)}
                    />
                    {errors.city && <InlineError text={errors.city} />}
                </Form.Field>

                <Button primary>Sign Up</Button>
            </Form>
        );
    }
}

SignupForm.propTypes = {
    submit: PropTypes.func.isRequired
};

export default SignupForm;