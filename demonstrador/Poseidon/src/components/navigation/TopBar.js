import React, {Component} from 'react'
import {Button, Container, Dropdown, Image, Menu} from 'semantic-ui-react'
import {Link} from "react-router-dom";

class FixedMenuLayout extends Component {

    constructor(props) {
        super(props);
        this.state = {
            activeItem: 'home'
        }
    }

    handleItemClick = (e, { name }) => this.setState({ activeItem: name });

    logoutTemporary = () => {
        localStorage.removeItem("token");
        localStorage.removeItem("user");
        window.location.reload();
    };

    render() {
        const auth = !!localStorage.getItem("token");
        const { activeItem } = this.state;
        return (
            <div style={{marginBottom: 15}}>
                <Menu pointing secondary>
                    <Container>
                        <Menu.Item header as={Link} to='/'>
                            <Image
                                size='small'
                                src='/images/Clearfire_logo.bmp'
                                style={{marginRight: '1.5em'}}
                            />
                        </Menu.Item>
                        {auth && <Menu.Item as={Link} to='/dashboard' name='dashboard' active={activeItem === 'dashboard'} onClick={this.handleItemClick}>Dashboard</Menu.Item>}
                        <Menu.Item as={Link} to='/events' name='events' active={activeItem === 'events'} onClick={this.handleItemClick}>Events</Menu.Item>
                        <Menu.Item as={Link} to='/reports' name='reports' active={activeItem === 'reports'} onClick={this.handleItemClick}>Reports</Menu.Item>
                        <Menu.Item as={Link} to='/news' name='news' active={activeItem === 'news'} onClick={this.handleItemClick}>News</Menu.Item>

                        {auth ? (
                            <Menu.Menu position='right'>
                                <Dropdown item simple trigger={<Image size='mini' src={'./images/user.png'}/>}>
                                    <Dropdown.Menu>
                                        <Dropdown.Header>Currently signed in as {localStorage.getItem("user")}</Dropdown.Header>
                                        <Dropdown.Divider />
                                        <Dropdown.Item as={Link} to='change_password'>Change Password</Dropdown.Item>
                                        <Dropdown.Item as={Link} to='account_details'>Account Details</Dropdown.Item>
                                        <Dropdown.Item onClick={() => this.logoutTemporary()}>Logout</Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </Menu.Menu>
                        ) : (
                            <Menu.Menu position='right'>
                                <Menu.Item position='right'>
                                    <Button as={Link} to='/login' >Log in</Button>
                                    <Button as={Link} to='/signup'  primary style={{ marginLeft: '0.5em' }}>Sign Up</Button>
                                </Menu.Item>
                            </Menu.Menu>
                        )}
                    </Container>
                </Menu>
            </div>
        )
    }
}

export default FixedMenuLayout;