import React, {Component} from "react";
import {Container, Image, Menu, Dropdown, Icon} from "semantic-ui-react";
import {Link} from "react-router-dom";
import axios from "axios";

class TopNavigation extends Component {

    logout = () => {
        const token = localStorage.getItem("token");
        const user = localStorage.getItem("user");
        console.log("user:"+user);
        axios.delete('https://henrique-45707.appspot.com/rest/login/logout', {data:{
            username: user,
            tokenID: token
        }}).then(() => {
            localStorage.removeItem("token");
            localStorage.removeItem("user");
            window.location.reload();
            console.log("logout realizado... token apagado do localStorage");
        }).catch(() => {
            console.log("Erro no logout")
        });
        axios.delete()
    };

    logoutTemporary = () => {

        localStorage.removeItem("token");
        localStorage.removeItem("user");
        window.location.reload();
    };

    render() {
        const auth = !!localStorage.getItem("token");
        return (
            <div>
                {!auth ? (
                    <div>
                        <Menu secondary pointing>

                            <Menu.Item as={Link} to="/">
                                Home
                            </Menu.Item>
                            <Menu.Item as={Link} to="/events">
                                Events
                            </Menu.Item>
                            <Menu.Item as={Link} to="/reports">
                                Reports
                            </Menu.Item>


                        </Menu>
                    </div>
                ) : (

                    <div>

                        <Menu secondary pointing>

                            <Menu.Item as={Link} to="/">
                                Home
                            </Menu.Item>
                            <Menu.Item as={Link} to="/dashboard">
                                Dashboard
                            </Menu.Item>
                            <Menu.Item as={Link} to="/events">
                                Events
                            </Menu.Item>
                            <Menu.Item as={Link} to="/reports">
                                Reports
                            </Menu.Item>

                            <Menu.Menu position="right">
                                <Dropdown trigger={<Icon name='user'/>}>
                                    <Dropdown.Menu>
                                        <Dropdown.Item onClick={() => this.logoutTemporary()}>Logout</Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </Menu.Menu>
                        </Menu>

                    </div>

                )}
            </div>

        )
    }

}

export default TopNavigation;