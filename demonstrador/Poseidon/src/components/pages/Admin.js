import React, {Component} from 'react';
import Axios from 'axios';
import "../css/Admin.css"

class AdminPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            changePage: false,
            loading: false,
            error: false,
            username: this.props.username,
            tokenID: this.props.tokenID,
            validToken: true,
            users: []
        };
        this.getInitialInfo();
    }

    getInitialInfo() {
        Axios.post('http://localhost:8080/rest/user/listusers', {
            username: this.state.username,
            tokenID: this.state.tokenID
        }).then((response) => {
            this.setState({
                users: response.data
            })

        }).catch(() => {
            this.setState({error: true});
        });

    };

    handleUser(index) {
        var userToremove = this.state.users[index].key.name;
        if (userToremove === this.state.username) {
            alert("Não pode remover a sua propria conta!!");
            return;
        }
        var confirmation = confirm("Tem a certeza que quer eliminar o utilizador " + userToremove);
        if (confirmation) {
            Axios.post('http://localhost:8080/rest/user/removeuser', {
                userToRemove: userToremove,
                token: {
                    username: this.state.username,
                    tokenID: this.state.tokenID
                }
            }).then((response) => {
                this.getInitialInfo();
            }).catch(() => {
                this.setState({error: true});
            });
        }
    }

    viewPage() {
        return <div>
            <ul><h3>Users</h3>
                {this.state.users.map((info, i) =>
                    <li key={i} onClick={() => this.handleUser(i)} className="account">
                        {info.propertyMap.user_role === "gs" && <h4>Gestor de Sistema</h4>}
                        {info.propertyMap.user_role === "user" && <h3>User</h3>}
                        <p>Name: {info.propertyMap.user_name}</p>
                        <p>Email: {info.propertyMap.user_email}</p>
                        <p>Phone: {info.propertyMap.user_phone.value}</p>
                        <p>Cellphone: {info.propertyMap.user_cellphone.value}</p>
                    </li>
                )

                }

            </ul>
        </div>;
    };

    render() {
        return (
            <div>
                <div>
                    {this.viewPage()}
                </div>
            </div>
        )

    }
}

export default AdminPage;