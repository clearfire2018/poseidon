import React from 'react';
import '../css/Header.css';

let Header = () =>
    <header>
        <div className="logo"><img src={'./images/ClearFire_logo.bmp'} alt="company logo" height={"50em"}/>
            <figcaption>Clean the forest, prevent fire!</figcaption></div>
    </header>

export default Header;
