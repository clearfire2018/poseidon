import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Slide } from 'react-slideshow-image';

import {
    Button,
    Container,
    Grid,
    Header,
    Image,
    List,
    Menu,
    Responsive,
    Segment,
    Visibility,
} from 'semantic-ui-react'
import {Link} from "react-router-dom";

/* eslint-disable react/no-multi-comp */
/* Heads up! HomepageHeading uses inline styling, however it's not the best practice. Use CSS or styled components for
 * such things.
 */


const images = [
    'images/intro.jpg',
  'images/forest.jpg',
  'images/forest2.jpg',
];

const Slideshow = () => {
    return (
        <Slide
          images={images}
          duration={5000}
          transitionDuration={1000}
        
        />
    )
}

const HomepageHeading = () => (
    <Container text>
        <Header
            as='h1'
            content='ClearFire'
            inverted
            style={{
                fontSize: '4em',
                fontWeight: 'normal',
                marginBottom: 0,
                marginTop:'3em',
            }}
        />
        <Header
            as='h2'
            content='Saving the environment one step at a time.'
            inverted
            style={{
                fontSize:'1.7em',
                fontWeight: 'normal',
                marginTop: '1.5em',
            }}
        />
{/*        <Button primary size='huge'>
            Get Started
            <Icon name='right arrow' />
        </Button>*/}
    </Container>
);

class DesktopContainer extends Component {
    state = {};

    hideFixedMenu = () => this.setState({ fixed: false });
    showFixedMenu = () => this.setState({ fixed: true });

    render() {
        const { children } = this.props;
        const { fixed } = this.state;
        const isAuth = !!localStorage.getItem("token")

        
        return (
            <Responsive {...Responsive.onlyComputer}>
                <Visibility once={false} onBottomPassed={this.showFixedMenu} onBottomPassedReverse={this.hideFixedMenu}>
                    <Segment inverted textAlign='center' style={{ minHeight: 600, padding: '1em 0em' }} vertical>
                        <Menu
                            fixed={fixed ? 'top' : null}
                            inverted={!fixed}
                            pointing={!fixed}
                            secondary={!fixed}
                            size='large'
                        >
                            <Container>
                                <Menu.Item as={Link} to="/" active>Home</Menu.Item>
                                {isAuth && <Menu.Item as={Link} to="/dashboard" >Dashboard</Menu.Item>}
                                <Menu.Item as={Link} to="/events">Events</Menu.Item>
                                <Menu.Item as={Link} to="/reports">Reports</Menu.Item>
                                {!isAuth && <Menu.Item position='right'>
                                    <Button as={Link} to="/login" inverted={!fixed}>Log in</Button>
                                    <Button as={Link} to="/signup" inverted={!fixed} primary={fixed} style={{ marginLeft: '0.5em' }}>Sign Up</Button>
                                </Menu.Item>}
                            </Container>
                        </Menu>
                        <Slideshow/>
                    </Segment>
                </Visibility>

                {children}
            </Responsive>
        )
    }
}

DesktopContainer.propTypes = {
    children: PropTypes.node,
}


const ResponsiveContainer = ({ children }) => (
    <div>
        <DesktopContainer>{children}</DesktopContainer>
    </div>
)

ResponsiveContainer.propTypes = {
    children: PropTypes.node,
}

const HomepageLayout = () => (
    <ResponsiveContainer>
        <Segment style={{ padding: '8em 0em' }} vertical>
            <Grid container stackable verticalAlign='middle'>
                <Grid.Row>
                    <Grid.Column width={8}>
                        <Header as='h3' style={{ fontSize: '2em' }}>Report a fire</Header>
                        <p style={{ fontSize: '1.33em' }}>
                            If you spot a fire make sure to report it to make the world safer for everyone!
                        </p>

                    </Grid.Column>
                    <Grid.Column floated='right' width={6}>
                        <Image
                            bordered
                            rounded
                            size='large'
                            src='/images/1.jpg'
                        />
                    </Grid.Column>
                </Grid.Row>

            </Grid>
        </Segment>

      
        <Segment inverted vertical style={{ padding: '5em 0em' }}>
            <Container>
                <Grid divided inverted stackable>
                    <Grid.Row>
                        <Grid.Column width={3}>
                            <Header inverted as='h4' content='About Us' />
                            <List link inverted>
                                <List.Item as='a'>Fire Prevention Guidelines </List.Item>
                            </List>
                        </Grid.Column>
                        <Grid.Column width={3}>
                            <Header inverted as='h4' content='Services' />
                            <List link inverted>
                                <List.Item as='a'>Reports Map</List.Item>
                                <List.Item as='a'>Events Near You</List.Item>
                                <List.Item as='a'>Register</List.Item>
                                <List.Item as='a'>Feedback</List.Item>
                            </List>
                        </Grid.Column>
                        <Grid.Column width={7}>
                            
                            <p>"In a firefight you can plan everything out to the minute, and a minute before that everything changes."</p>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Container>
        </Segment>
    </ResponsiveContainer>
)
export default HomepageLayout

