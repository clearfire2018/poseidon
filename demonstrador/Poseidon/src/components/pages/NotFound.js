import React from "react";
import TopNavigation from "../navigation/TopBar";

const NotFound = () => (
    <div >
        <TopNavigation/>
        <h3>Sorry, page not found!</h3>
    </div>
);
export default  NotFound;