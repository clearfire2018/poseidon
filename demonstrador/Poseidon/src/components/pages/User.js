import React, {Component} from 'react';
import Axios from 'axios';
import Admin from './Admin';
import ChangePassword from './user/ChangePassword';
import '../css/User.css';
import Map from './MapContainer';

class UserPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            changePage: false,
            loading: true,
            role: '',
            error: false,
            username: this.props.location.state.username,
            tokenID: this.props.location.state.token,
            center: "{lat: 38.661155, lng: -9.203555}",
            listusers: false,
            changePwd: false,
            validToken: false
        };
        this.getInitialInfo()
        //this.geocode();
        this.listUsers = this.listUsers.bind(this);
        this.changePassword = this.changePassword.bind(this);
        this.handleVoltarList = this.handleVoltarList.bind(this);
        this.handleVoltarChangePwd = this.handleVoltarChangePwd.bind(this);
    }

  
    
    getInitialInfo() {
        Axios.post('http://localhost:8080/rest/user/info', {
            username: this.state.username,
            tokenID: this.state.tokenID
        }).then((response) => {
            console.log("Token usado no getInfo:" + this.state.tokenID);
            this.setState({
                role: response.data.role,
                cc: response.data.cc,
                nif: response.data.nif,
                cellphone: response.data.cellphone,
                phone: response.data.phone,
                name: response.data.name,
                email: response.data.email,
                street: response.data.street,
                city: response.data.city,
                zipcode1: response.data.zipcode1,
                zipcode2: response.data.zipcode2,
            })

        }).catch(() => this.setState({error: true}));
    };

    geocode() {
        var location = this.state.street;
        Axios.get('http://maps.googleapis.com/maps/api/geocode/json', {
            params: {
                address: location,
                key: 'AIzaSyCNSya0gVPBSlYGTza8beTBD2sq358lRME'
            }
        })
            .then((response) => {
                console.log(response);
                var latitude = response.data.results[0].geometry.location.lat;
                var longitude = response.data.results[0].geometry.location.lng;
                var center = "lat:" + latitude + ", lng:" + longitude;
                this.setState({
                    center: center
                })})
                    .catch(function (error) {
                        console.log(error);
                    })

    }

    handleVoltarList() {
        this.setState({
            listusers: false
        })
    }

    handleVoltarChangePwd(){
        this.setState({
            changePwd: false
        })
    }

    viewValidUser() {
        if (this.state.listusers) {

            return (<div>
                <button id="voltar" onClick={this.handleVoltarList}>Voltar</button>
                <Admin username={this.state.username} tokenID={this.state.tokenID}/>

            </div>);
        }
        if(this.state.changePwd){
            return (<div>
                <button id="voltar" onClick={this.handleVoltarChangePwd}>Voltar</button>
                <ChangePassword username={this.state.username} tokenID={this.state.tokenID}
                                handleLogout={this.props.handleLogout}/>
            </div>);
        }
        return (
            <div>
                <div>
                    <h2>Bem vindo {this.state.name}</h2>

                    <ul>Detalhes da conta:
                        {this.state.cc !== "" && <li>CC: {this.state.cc}</li>}
                        {this.state.nif !== "" && <li>NIF: {this.state.nif}</li>}
                        {this.state.phone !== "" && <li>Telefone: {this.state.phone}</li>}
                        {this.state.cellphone !== "" && <li>Telemovel: {this.state.cellphone}</li>}
                        <li>Email: {this.state.email}</li>
                        <li>Morada: {this.state.street}, {this.state.city}</li>
                        <li>Codigo-Postal: {this.state.zipcode1}-{this.state.zipcode2}</li>
                    </ul>
                </div>
                <div>
                    <Map center={this.state.center} username={this.state.username}/>
                </div>
            </div>
        );
    }
    ;

    viewError() {
        return (
            <div>
                <h3 className="status">Error</h3>
            </div>
        );
    }
    ;

    viewLoading() {
        return (
            <div>
                <h3 className="status">loading...</h3>
            </div>
        );
    }

    viewInvalidLogin() {
        return (
            <div>
                <h3 className="status">Username or password incorrect!</h3>
            </div>
        );
    }

    viewPage() {
        
            return this.viewValidUser();
    }
    


    listUsers() {

        this.setState({
            listusers: true
        });
    }

    changePassword(){
        this.setState({
            changePwd : true
        });
    }


    render() {
        var labelLogout;
        if (this.state.tokenID === "")
            labelLogout = "Voltar";
        else
            labelLogout = "Logout";

        return (
            <div>
                <div id="user-form">
                    <div id="loadbutton">
                        {this.state.role === "gs" && !this.state.listusers &&
                        <button className="button" id="listusers" onClick={this.listUsers}>Listar os
                            Utilizadores</button>}
                    </div>
                    <div>
                        {this.viewPage()}
                       
                    </div>
                    <div id="changePassword">
                        {!this.state.listusers && !this.state.loading && !this.state.changePwd &&
                        <button id="changePwd" onClick={this.changePassword}>Mudar a palavra passe</button>}
                    </div>
                </div>

            </div>
        );

    }
    ;
}

export default UserPage;
