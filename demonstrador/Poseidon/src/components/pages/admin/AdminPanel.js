import React, {Component} from "react";
import axios from "axios/index";
import {Button, Table, Icon} from "semantic-ui-react";
import {Link, Redirect} from "react-router-dom";

class DashboardPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            user: null,
            events: [],
            users: [],
            loading: true,
            cursor: "",

        }
    }

    componentWillMount() {
        this.requestUserInfo();
        this.requestUsers();
    }

    requestUsers() {
        const name = localStorage.getItem("user");
        const token = localStorage.getItem("token");
        axios.post('https://henrique-45707.appspot.com/rest/user/listusers', {
            username: name,
            tokenID: token,
        }).then((response) => {
            console.log("users response", response.data)
            this.setState({
                users: response.data
            })

        }).catch((err) => {
            console.log("error in listing users", err)
        });

    };

    requestUserInfo = () => {
        const name = localStorage.getItem("user");
        const token = localStorage.getItem("token");
        axios.post('https://henrique-45707.appspot.com/rest/user/info', {
            username: name,
            tokenID: token,
        }).then((res) => {
            this.setState({
                user: res.data
            }, () => {
                this.setState({
                    loading: false
                })
                this.requestEvents();
            });
        }).catch((err) => {
            console.error("error in fetching user info", err)
        })
    };

    requestEvents = () => {
        const {cursor} = this.state;

        axios.post('https://henrique-45707.appspot.com/rest/events/list/info?cursor=' + cursor, {
            username: localStorage.getItem("user"),
            tokenID: localStorage.getItem("token")
        }).then((response) => {
            let add_event = response.data.events;
            add_event = add_event.concat(this.state.events);
            this.setState({
                events: add_event,
                newCursor: response.data.cursor
            }, () => {
                if(this.state.newCursor !== this.state.cursor)
                    this.setState({
                        cursor: this.state.newCursor
                    }, ()=> {
                        this.requestEvents();
                    })
            });
        }).catch(() => {
            console.error("error listing events")
        });

    };

    promoteWorker = (user) => {
        alert("clicked " + user);
        this.props.history.push("/promote_worker/"+ user)

    };

    removeEvent = (event) => {
        console.log("event", event)
        const name = localStorage.getItem("user");
        const token = localStorage.getItem("token");

        axios.delete('https://henrique-45707.appspot.com/rest/events/' + event.key.name + '/remove', {
            username: name,
            tokenID: token,
        }).then((res) => {
            console.log("event deleted", res)
        }).catch((err) => {
            console.error("error in deleting event", err)
        })
    };

    removeUser = (user) => {
        console.log("user", user)
        const name = localStorage.getItem("user");
        const token = localStorage.getItem("token");

        axios.delete('https://henrique-45707.appspot.com/rest/user/removeuser/' + user.key.name, {
            username: name,
            tokenID: token,
        }).then((res) => {
            console.log("user deleted", res)
        }).catch((err) => {
            console.error("error in deleting user", err)
        })
    };

    render() {
        if (!this.state.loading) {
            if (this.state.user.role !== "admin") {
                return <Redirect to="/dashboard"/>
            }
            const EventTable = (

            <Table celled structured>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell rowSpan='2'>Name</Table.HeaderCell>
                <Table.HeaderCell colSpan='3'>Delete</Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>

               {this.state.events.map((event, index) => <Table.Row key={index}>

                    <Table.Cell children= {event.propertyMap.event_name}></Table.Cell>
                    <Table.Cell onClick={ () => this.removeEvent(event) }><Icon name="ban" /></Table.Cell>

                </Table.Row>
                )}


            </Table.Body>
          </Table>
        )

            const UserTable = (

            <Table celled structured>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell rowSpan='2'>Name</Table.HeaderCell>
                <Table.HeaderCell rowSpan='2'>Type</Table.HeaderCell>
                <Table.HeaderCell rowSpan='2'>Promote</Table.HeaderCell>
                <Table.HeaderCell colSpan='3'>Delete</Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>

               {this.state.users.map((user, index) => <Table.Row key={index}>

                    <Table.Cell children={user.key.name}></Table.Cell>
                    <Table.Cell children={user.propertyMap.user_role}></Table.Cell>
                    <Table.Cell onClick={ () => this.promoteWorker(user.key.name) }><Icon name="angle double up" /></Table.Cell>
                    <Table.Cell onClick={ () => this.removeUser(user) }><Icon name="ban" /></Table.Cell>

                </Table.Row>
                )}


            </Table.Body>
          </Table>
        )

            return (
                <div className="ui container">
                    <h1>Admin Panel</h1>
                    <h2> All Events: </h2>
                    {EventTable}
                    <h2> All Users: </h2>
                    {UserTable}

                    <Button primary as={Link} to='/dashboard'>Dashboard</Button>
                </div>
            );

        } else return null;
    }
}

export default DashboardPage;