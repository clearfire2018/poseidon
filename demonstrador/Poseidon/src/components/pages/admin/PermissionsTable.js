import React, {Component} from "react";
import axios from "axios/index";
import {Button, Table, Icon, Checkbox} from "semantic-ui-react";
import {Link, Redirect} from "react-router-dom";

class PermissionsTable extends Component {

    constructor(props) {
        super(props);
        this.state = {

            username: localStorage.getItem("user"),
            tokenID: localStorage.getItem("token"),
            user: this.props.match.params.user,
            keysArray: [],
            valuesArray: [],
            finished: false,
        }
            this.handleChange = this.handleChange.bind(this);   
    }
    
    
    
    handleChange(e) {
        
        const {valuesArray} = this.state;
        const index = e.target.id;
        if (valuesArray[index] === 0) {
            let arr = valuesArray;
            arr[index] = 1;
            this.setState({
                valuesArray: arr
            });
        } else if (valuesArray[index] === 1) {
            let arr = valuesArray;
            arr[index] = 0;
            this.setState({
                valuesArray: arr
            });
        } 
    }

    componentWillMount() {
        const {username, tokenID, user} = this.state;
        axios.post('https://henrique-45707.appspot.com/rest/permissions/' + user, {
            username: username,
            tokenID: tokenID,
        }).then((res) => {
            console.log(res.data)
            this.setState({
                keysArray: res.data.keys,
                valuesArray: res.data.value,
            }, this.setState({finished: true}));
        }).catch((err) => {
            console.error("error getting user permissions, ", err)
        })
    }

    setPermissions = () => {
        console.log("VALUE 2 " + this.state.valuesArray)
        
        const {username, tokenID, user} = this.state;
        var permissions = [];
        const {keysArray, valuesArray} = this.state;
       
            axios.post('https://henrique-45707.appspot.com/rest/permissions/changePermissions/' + user, {
            acess: {
                username:username,
                tokenID:tokenID
            },
            key:keysArray,
            values:valuesArray

        }).then((res) => {
            console.log(res.data)
        }).catch((err) => {
            console.error("error getting user permissions, ", err)
        })
    };


    render() {
        const {finished} = this.state;
        const {keysArray, valuesArray} = this.state;

        if (finished && keysArray !== [] && valuesArray !== []) {

            const permissions_table = (

                <Table celled structured>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell rowSpan='2'>Permission</Table.HeaderCell>
                            <Table.HeaderCell colSpan='3'>State</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>

                        {this.state.keysArray.map((perm, index) => <Table.Row key={index}>

                                <Table.Cell>{perm}</Table.Cell>

                                <Table.Cell>
                            
                                    <Checkbox
                                        id={index}
                                        checked={(valuesArray[index] === 2 || valuesArray[index] === 1)}
                                        disabled={(valuesArray[index] === -1 || valuesArray[index] === 2)}
                                        onChange={this.handleChange}
                                    >
                                    </Checkbox>
                                    
            
                                </Table.Cell>

                            </Table.Row>
                        )}

                    </Table.Body>
                </Table>
            )

            return (
                <div className="ui container">
                    <h1>Permissions Table</h1>
                    {permissions_table}
                    <Button primary onClick={this.setPermissions}>Change Permissions</Button>
                </div>
            );

        } else return null;
    }
}

export default PermissionsTable;