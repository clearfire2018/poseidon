import React, {Component} from "react";
import axios from "axios";
import EventForm from "../../forms/EventForm";

class CreateEventPage extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    create_event = (param) => {
        const data = param.data;
        const file = param.img;
        const date = param.date;
        const city = param.city;

        const user = localStorage.getItem("user");
        const token = localStorage.getItem("token");
        const beginDate = date[0].getTime();
        const endDate = date[1].getTime();
        axios.post('https://henrique-45707.appspot.com/rest/events/register', {
            acess: {
                username: user,
                tokenID: token
            },
            name: data.name,
            description: data.description,
            location: data.location,
            city: city,
            beginDate: beginDate,
            endDate: endDate,
        }).then((res) => {
            console.log("Event created");
            console.log(res);
            if (file !== null)  {
                axios.post('https://henrique-45707.appspot.com/gcs/henrique-45707.appspot.com/' + res.data , file, {
                    headers: {
                        "Content-Type": file.type
                    }
                })
            }
            this.props.history.push("/events");
        }).catch(() => {
            console.log("Error creating event")

        });
    };

    submit = data => {
        this.create_event(data);
    };

    render() {
        return (
            <div>
                <h1>Create New Event</h1>

                <EventForm submit={this.submit}/>
            </div>
        );
    }

}

export default CreateEventPage;