import React, {Component} from 'react';
import axios from "axios/index";
import {List, Button, Comment, Form, Icon, Label} from "semantic-ui-react"

class EventPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.match.params.id,
            event: null,
            loading: true,
            likes: "",
            signs: "",
            comments: []
        };
    }

    componentWillMount() {
        const user = localStorage.getItem("user");
        const token = localStorage.getItem("token");
        const {id} = this.state;

        axios.post('https://henrique-45707.appspot.com/rest/events/' + id, {
            username: user,
            tokenID: token,
        }).then((response) => {
            this.setState({event: response.data, likes: response.data.numLikes, signs: response.data.numSigns}, () => {
                this.setState({loading: false});

                this.getComments();

            });
        });
    }


    getComments = () => {

        const user = localStorage.getItem("user");
        const token = localStorage.getItem("token");
        const {id} = this.state;

        axios.post('https://henrique-45707.appspot.com/rest/comment/' + id + '/list', {

            username: user,
            tokenID: token

        }).then((response) => {
            this.setState({comments: response.data})
        });

    }

    onChange = e =>
        this.setState({
            newcomment: e.target.value
        });

    likeEvent = () => {
        const user = localStorage.getItem("user");
        const token = localStorage.getItem("token");
        const {id} = this.state;

        axios.post('https://henrique-45707.appspot.com/rest/like/' + id + '/post', {
            username: user,
            tokenID: token

        }).then(() => {
            console.log("Event liked.");
            this.setState({likes: this.state.likes + 1})

        });
    };

    signEvent = () => {
        const user = localStorage.getItem("user");
        const token = localStorage.getItem("token");
        const {id} = this.state;

        axios.post('https://henrique-45707.appspot.com/rest/events/signup/' + id + '/post', {
            username: user,
            tokenID: token,
        }).then(() => {
            console.log("Event signed.");
            this.setState({signs: this.state.signs + 1})
        });
    };

    postComment = () => {
        const user = localStorage.getItem("user");
        const token = localStorage.getItem("token");
        const {id} = this.state;

        axios.post('https://henrique-45707.appspot.com/rest/comment/' + id + '/post', {
            acess: {
                username: user,
                tokenID: token
            },
            comment: this.state.newcomment
        }).then(() => {


            this.getComments();

            this.setState({newcomment: ""})
        });

    };

    timeSince = (date) => {

        const seconds = Math.floor((new Date() - date) / 1000);

        let interval = Math.floor(seconds / 31536000);

        if (interval > 1) {
            return interval + " years";
        }
        interval = Math.floor(seconds / 2592000);
        if (interval > 1) {
            return interval + " months";
        }
        interval = Math.floor(seconds / 86400);
        if (interval > 1) {
            return interval + " days";
        }
        interval = Math.floor(seconds / 3600);
        if (interval > 1) {
            return interval + " hours";
        }
        interval = Math.floor(seconds / 60);
        if (interval > 1) {
            return interval + " minutes";
        }
        return Math.floor(seconds) + " seconds";
    }

    render() {
        const {comments, loading, event} = this.state;
        const user = localStorage.getItem("user");

        const commentsList = comments.map((comment, index) =>
            (
                <Comment key={index}>
                    {comment.propertyMap.comment_username === 'admin' ?
                        <Comment.Avatar as='a' src='/images/admin.png' size='mini'/> :
                        <Comment.Avatar as='a' src='/images/user.png' size='mini'/>}
                    <Comment.Content>
                        <Comment.Author>{comment.propertyMap.comment_username}</Comment.Author>
                        <Comment.Metadata>
                            <div>{this.timeSince(comment.propertyMap.comment_creation_time)} ago</div>
                        </Comment.Metadata>
                        <Comment.Text>
                            <p>{comment.propertyMap.comment_text}</p>
                        </Comment.Text>
                        <Comment.Actions>
                            {comment.propertyMap.comment_username === user &&
                                <Comment.Action>Delete</Comment.Action>}
                        </Comment.Actions>
                    </Comment.Content>
                </Comment>
            )
        )

        if (!loading) {

            const begin = new Date(parseFloat(event.beginDate));
            const end = new Date(parseFloat(event.endDate));

            const beginDate = begin.getDay() +"/" + begin.getMonth() + "/" + begin.getFullYear();
            const endDate = end.getDay() +"/" + end.getMonth() + "/" + end.getFullYear();

            return (
                <div className='ui container'>
                    <h1>{event.name}</h1>

                    <img src={"https://henrique-45707.appspot.com/gcs/henrique-45707.appspot.com/" + event.id}
                         onError={(e) => {
                             e.target.src = "/images/default.png"
                         }} style={{maxHeight:500,maxWidth:500, minHeight:500, minWidth:500}} alt=""/>
                    <List>
                        <List.Item> <b>Description:</b> {event.description} </List.Item>
                        <List.Item> <b>State:</b> boasboas </List.Item>
                        <List.Item> <b>Start Date:</b> { beginDate } </List.Item>
                        <List.Item> <b>End Date:</b> { endDate } </List.Item>
                    </List>

                    <Button as='div' labelPosition='right'>
                        <Button color='red' onClick={this.likeEvent}>
                            <Icon name='heart' />
                            Like
                        </Button>
                        <Label as='a' basic color='red' pointing='left'>
                            {this.state.likes}
                        </Label>
                    </Button>

                    <Button as='div' labelPosition='right'>
                        <Button color='blue' onClick={this.signEvent}>
                            <Icon name='user'/>
                            Sign Up
                        </Button>
                        <Label as='a' basic color='blue' pointing='left'>
                            {this.state.signs}
                        </Label>
                    </Button>
                    <Comment.Group>

                        {commentsList}

                        <Form reply>
                            <Form.TextArea value={this.state.newcomment} onChange={this.onChange}/>

                            <Button content='Add Comment' labelPosition='left' icon='edit' primary
                                    onClick={this.postComment}/>


                        </Form>
                    </Comment.Group>


                </div>

            );
        } else return null;
    }


}

export default EventPage;
