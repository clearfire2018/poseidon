import React, {Component} from "react";
import axios from "axios";
import {Grid, Menu, Container, Button} from "semantic-ui-react";
import EventsList from "./list/EventsList";
import CreateEventPage from "./CreateEventPage";


class EventsPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            cursor: "",
            events: [],
            topliked_events: [],
            topsigned_events: [],
            done: false,
            onDay_events: [],
            date: new Date(),
            image: null,
            activeItem: 'events'
        };
    }
    
    requestEvents = () => {

        const {cursor} = this.state;
        
        axios.post('https://henrique-45707.appspot.com/rest/events/list/info?cursor=' + cursor, {
            username: localStorage.getItem("user"),
            tokenID: localStorage.getItem("token")
        }).then((response) => {
            let add_event = response.data.events;
            add_event = this.state.events.concat(add_event);
            this.setState({
                events: add_event,
                newCursor: response.data.cursor
            });
        }).catch(() => {
            console.error("error listing reports")
        });

    };
    
    requestMoreEvents = () => {

        const {cursor, newCursor} = this.state;

        if (newCursor !== cursor) {
            this.setState({
                cursor: newCursor
            }, () => {

                axios.post('https://henrique-45707.appspot.com/rest/events/list/info?cursor=' + newCursor, {
                    username: localStorage.getItem("user"),
                    tokenID: localStorage.getItem("token")
                }).then((response) => {
                    let add_event = response.data.events;
                    add_event = this.state.events.concat(add_event);
                    this.setState({
                        events: add_event,
                        newCursor: response.data.cursor
                    });
                }).catch(() => {
                    console.error("error listing reports")
                });
            });
        }
    };

    componentWillMount() {
      
        this.requestEvents();
        
        const user = localStorage.getItem("user");
        const token = localStorage.getItem("token");
        
            axios.post('https://henrique-45707.appspot.com/rest/events/list/listTopLiked', {

                username: user,
                tokenID: token

            }).then((response) => {
                this.setState({topliked_events: response.data});

                axios.post('https://henrique-45707.appspot.com/rest/events/list/listTopSign', {

                    username: user,
                    tokenID: token

                }).then((response) => {
                    this.setState({topsigned_events: response.data})
                });
            });

        

        //this.getEventsOnDay();
    }

    getEventsOnDay = () => {

        const user = localStorage.getItem("user");
        const token = localStorage.getItem("token");

        const selectedDate = this.state.date.getTime();

        console.log("date is " + selectedDate);

        axios.post('https://henrique-45707.appspot.com/rest/events/list/listAfterDate=' + selectedDate, {
            acess: {
                username: user,
                tokenID: token
            }
        }).then((response) => {
            this.setState({onDay_events: response.data});
        })
    }

    showCalendar = () => {
        this.setState({calendarShowing: !(this.state.calendarShowing)})
    };

    onChange = data => this.setState({date: data}, () => {
        console.log("data:" + this.state.date)
    });

    handleItemClick = (e, {name}) => this.setState({activeItem: name})

    render() {
        const {events, topliked_events, topsigned_events, activeItem} = this.state;
        return (
            <div className="ui container">

                <h1>Events</h1>


                <Grid>
                    <Grid.Column width={4}>
                        <Menu fluid vertical tabular>
                            <Menu.Item name='events' active={activeItem === 'events'}
                                       onClick={this.handleItemClick}/>
                            <Menu.Item name='top liked' active={activeItem === 'top liked'}
                                       onClick={this.handleItemClick}/>
                            <Menu.Item name='top signed' active={activeItem === 'top signed'}
                                       onClick={this.handleItemClick}/>
                            <Menu.Item style={{backgroundColor: '#3161c7', color: 'white'}} icon='plus circle' name='create event'
                                       active={activeItem === 'create event'} onClick={this.handleItemClick}/>
                        </Menu>
                  
                    </Grid.Column>

                    <Grid.Column stretched width={12}>
                        {activeItem === 'events' && <EventsList events={events}/>}
                        {activeItem === 'top liked' && <EventsList events={topliked_events}/>}
                        {activeItem === 'top signed' && <EventsList events={topsigned_events}/>}
                        {activeItem === 'create event' && <CreateEventPage/>}
            
                         <Container style={{padding: 70}}>
                                {activeItem === 'events' &&  <Button onClick={this.requestMoreEvents} style={{backgroundColor: '#3161c7', color: 'white'}} fluid >Load More</Button>}
                        </Container>
                    </Grid.Column>
                        
                </Grid>
                    

            </div>

        );
    }

}

export default EventsPage;