import React, {Component} from "react";
import {Input, Button, Card, Container, Header, Icon, Image, List, Modal, Label, Comment, Form} from "semantic-ui-react";
import axios from "axios/index";

class EventsList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false,
            comments: [],
            newcomment: ""
        };
    }

     onChange = e =>
        this.setState({
            newcomment: e.target.value
        });
        
        
      postComment = () => {
        const user = localStorage.getItem("user");
        const token = localStorage.getItem("token");
        const id = this.state.current.key.name;

        axios.post('https://henrique-45707.appspot.com/rest/comment/' + id + '/post', {
            acess: {
                username: user,
                tokenID: token
            },
            comment: this.state.newcomment
        }).then(() => {

            this.getComments(id);

            this.setState({newcomment: ""})
        });

    };
    
     timeSince = (date) => {

        const seconds = Math.floor((new Date() - date) / 1000);

        let interval = Math.floor(seconds / 31536000);

        if (interval > 1) {
            return interval + " years";
        }
        interval = Math.floor(seconds / 2592000);
        if (interval > 1) {
            return interval + " months";
        }
        interval = Math.floor(seconds / 86400);
        if (interval > 1) {
            return interval + " days";
        }
        interval = Math.floor(seconds / 3600);
        if (interval > 1) {
            return interval + " hours";
        }
        interval = Math.floor(seconds / 60);
        if (interval > 1) {
            return interval + " minutes";
        }
        return Math.floor(seconds) + " seconds";
    }
    
    getComments = (id) => {

        const user = localStorage.getItem("user");
        const token = localStorage.getItem("token");

        axios.post('https://henrique-45707.appspot.com/rest/comment/' + id + '/list', {

            username: user,
            tokenID: token

        }).then((response) => {
            this.setState({comments: response.data.reverse()})
        });

    }
    
    getDate = (dateMil) => {
        const date = new Date(parseFloat(dateMil));
        return date.getDay() + "-" + date.getMonth() + "-" + date.getFullYear();
    }
    
    
     likeEvent = () => {
        const user = localStorage.getItem("user");
        const token = localStorage.getItem("token");
        const id = this.state.current.key.name;

        axios.post('https://henrique-45707.appspot.com/rest/events/like/' + id , {
            username: user,
            tokenID: token

        }).then((response) => {
            console.log("Event liked.");
            this.setState({likes: response.data})

        });
    };

    signEvent = () => {
        const user = localStorage.getItem("user");
        const token = localStorage.getItem("token");
        const id = this.state.current.key.name;

        axios.post('https://henrique-45707.appspot.com/rest/events/signup/' + id , {
            username: user,
            tokenID: token,
        }).then((response) => {
            console.log("Event signed.");
            this.setState({signs: response.data})
        });
    };
    
    onClose = () => this.setState({open: false}, () => this.setState({modalLoaded:false}));
    onOpen = (event) => {
        console.log("event",event)
        
        this.setState({
            open: true,
            current: event,
        }, () => {
            
            this.getComments(this.state.current.key.name);    
            
            const flikes = parseFloat(this.state.current.propertyMap.event_numLikes);
            const fsigns = parseFloat(this.state.current.propertyMap.event_numSignups);

            
            this.setState({
                likes: flikes,
                signs: fsigns,
                modalLoaded: true,
            })
        
        })
    };

    render() {
        const {user} = localStorage.getItem("user");
        const {events} = this.props;
        const {current} = this.state || null;
        const {comments} = this.state;
        console.log(this.state.newcomment)
        
        const inlineStyle1 = {
            modal: {
                position: 'absolute',
                top: '25%',
                left: '50%',
                transform: 'translate(-50%, 50%)'
            }
        }
        
        const commentsList = comments.map((comment, index) =>
            (
                <Comment key={index}>
                    {comment.propertyMap.comment_username === 'admin' ?
                        <Comment.Avatar as='a' src='/images/admin.png' size='mini'/> :
                        <Comment.Avatar as='a' src='/images/user.png' size='mini'/>}
                    <Comment.Content>
                        <Comment.Author>{comment.propertyMap.comment_username}</Comment.Author>
                        <Comment.Metadata>
                            <div>{this.timeSince(comment.propertyMap.comment_creation_time)} ago</div>
                        </Comment.Metadata>
                        <Comment.Text>
                            <p>{comment.propertyMap.comment_text}</p>
                        </Comment.Text>
                        <Comment.Actions>
                            {comment.propertyMap.comment_username === user &&
                                <Comment.Action>Delete</Comment.Action>}
                        </Comment.Actions>
                    </Comment.Content>
                </Comment>
            )
        )
        return (
                <Container style={{display: 'flex', justifyContent: 'center'}}>
                    <Card.Group style={{maxWidth: '85%'}} itemsPerRow={1}>

                        {events.map((event, index) =>
                            (
                                <Card key={index} onClick={() => this.onOpen(event)}>
                                    <Card.Content>

                                        <Card.Header><Icon name="user"/>{event.propertyMap.event_user}</Card.Header>

                                    </Card.Content>
                                    <Image
                                        src={"https://henrique-45707.appspot.com/gcs/henrique-45707.appspot.com/" + event.key.name}
                                        onError={(e) => {
                                            e.target.src = "/images/default.png"
                                        }}
                                        fluid style={{maxHeight: 450}} bordered/>

                                    <Card.Content>
                                        <Card.Header>{event.propertyMap.event_name}</Card.Header>
                                        <Card.Meta>
                                        <span
                                            className='date'>From: {this.getDate(event.propertyMap.event_begin)} / To: {this.getDate(event.propertyMap.event_end)}</span>
                                        </Card.Meta>
                                        <Card.Description>{event.propertyMap.event_description}</Card.Description>
                                    </Card.Content>

                                    <Card.Content extra>
                                        <List horizontal>
                                            <List.Item>
                                                <Icon color='red' name='like'/> {event.propertyMap.event_numLikes}
                                            </List.Item>
                                            <List.Item>
                                                <Icon color='blue' name='user'/> {event.propertyMap.event_numSignups}
                                            </List.Item>
                                        </List>
                                    </Card.Content>

                                </Card>

                            )
                        )}

                    </Card.Group>

                    { this.state.modalLoaded && <Modal open={this.state.open} onClose={this.onClose} style={inlineStyle1.modal} >
                        <Modal.Header>{current.propertyMap.event_user}
                            
                        <Button as='div' labelPosition='right' style={{position:'absolute', right:20}}>
                        <Button color='red' onClick={this.likeEvent}>
                            <Icon name='heart' />
                            Like
                        </Button>
                        <Label as='a' basic color='red' pointing='left'>
                            {this.state.likes}
                        </Label>
                    </Button>

                    <Button as='div' labelPosition='right'style={{position:'absolute', right:170}}> 
                        <Button color='blue' onClick={this.signEvent}>
                            <Icon name='user'/>
                            Sign Up
                        </Button>
                        <Label as='a' basic color='blue' pointing='left'>
                            {this.state.signs}
                        </Label>
                    </Button>
                            
                        </Modal.Header>
                        <Modal.Content image scrolling>
                           <Image wrapped bordered size='medium' src={"https://henrique-45707.appspot.com/gcs/henrique-45707.appspot.com/" + current.key.name}
                         onError={(e) => {
                             e.target.src = "/images/default.png"
                         }} />
                        

                            <Modal.Description>
                                <Header>{current.propertyMap.event_name}</Header>
                                <p>{current.propertyMap.event_description}</p>
                                
                            </Modal.Description>
                        
                         <Modal.Content scrolling style={{position:'absolute', right:0, maxHeight:200, maxWidth:270, minHeight:200, minWidth:300, }} >
                            <Comment.Group>
                             
                             {commentsList}

                             </Comment.Group>
            
                        </Modal.Content>
                          
                    
                        </Modal.Content>
                        <Modal.Actions>
                    
                     <Input fluid
                        action={ <Button color='teal' icon='arrow circle up' content='Post' onClick={ this.postComment } />}
                        actionPosition='right'
                        placeholder='Comment'
                        onChange={this.onChange}
                        value={this.state.newcomment}
                      />

                        </Modal.Actions>
                    </Modal> }

            </Container>
                      
        );
    }

}

export default EventsList;