/*global google*/
import React from "react";
import {
    withGoogleMap,
    GoogleMap,
    InfoWindow,
    Marker,
} from "react-google-maps"
import SearchBox from "react-google-maps/lib/components/places/SearchBox";
import {Card} from "semantic-ui-react";
const DashboardMap = withGoogleMap(props => {

    const types = ["Incêndio","Prevenção","Lixo"];

    var category = {
        0: {
            icon: '/images/fire.png',
            type: "Incêndio"
        },
        1: {
            icon: '/images/tree.png',
            type: "Prevenção"
        },
        2: {
            icon: '/images/trash.png',
            type: "Lixo"
        }
    };
    return (
        <GoogleMap
            ref={props.onMapMounted}
            zoom={props.zoom}
            center={props.center}
            onZoomChanged={props.onZoomChanged}
            onClick={props.handleMapClick}
        >

            {props.markers.map((marker, index) => (

                <Marker
                    key={index}
                    icon={category[marker.propertyMap.report_category].icon}
                    position={{
                        lat: parseFloat(marker.propertyMap.report_latitude),
                        lng: parseFloat(marker.propertyMap.report_longitude)
                    }}
                    onClick={() => props.handleMarkerClick(marker)}
                    onRightClick={() => props.handleMarkerDblClick(marker)}
                >
                    {marker.showInfo && (
                        <InfoWindow onCloseClick={() => props.handleMarkerClick}>
                            <div>
                                <Card>
                                    <Card.Content>
                                        <Card.Header> {marker.propertyMap.report_description}</Card.Header>
                                        <Card.Description>{category[marker.propertyMap.report_category].type}</Card.Description>
                                    </Card.Content>
                                    <Card.Content extra>
                                        <div style={{textAlign:'center'}}>
                                            <a href={"/report/" + marker.propertyMap.report_id}>Click here to see report page</a>
                                        </div>
                                    </Card.Content>
                                </Card>
                            </div>
                        </InfoWindow>
                    )}

                </Marker>

            ))}

        </GoogleMap>
    )
});
export default DashboardMap;

