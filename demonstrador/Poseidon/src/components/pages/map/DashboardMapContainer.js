/*global google*/
import InitialMap from './InitialMap';
import axios from 'axios';
import React, {Component} from 'react';

const refs = {};

class DashboardMapContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            markers: [],
            bounds: null,
            center: {
                lat: 39.3999,
                lng: -8.2245
            },
            zoom: 9
        };
        this.handleMarkerClick = this.handleMarkerClick.bind(this);
        this.handleMarkerClose = this.handleMarkerClose.bind(this);
        this.onMapMounted = this.onMapMounted.bind(this);
    }

    componentWillMount() {
        this.geocode();

        this.requestMarkers();

    };

    requestMarkers = () => {
        axios.post('https://henrique-45707.appspot.com/rest/reports/listAll', {
            username: localStorage.getItem("user"),
            tokenID: localStorage.getItem("token")
        }).then((response) => {
            this.setState({
                markers: response.data.reports,
            });
        }).catch(() => {
            console.error("error listing reports")
        });
    };

    geocode() {
        const {city} = this.props;
        if (!!city) {
            axios.get('https://maps.googleapis.com/maps/api/geocode/json', {
                params: {
                    address: city,
                    key: 'AIzaSyAmRkS98qIZpwi78yl4j3wlGyFGEE-lzYs'
                }
            }).then((response) => {
                this.setState({
                    center: {
                        lat: parseFloat(response.data.results[0].geometry.location.lat),
                        lng: parseFloat(response.data.results[0].geometry.location.lng)
                    }
                });
            }).catch((error) => {
                console.error("geocode error", error);
            });
        }
    }

    onMapMounted = ref => {
        refs.map = ref;
        console.log(refs.map)
    };

    onZoomChanged = () => {
        this.setState({
            zoom: refs.map.getZoom()
        });
    };

    handleMarkerClick(targetMarker) {
        this.setState({
            markers: this.state.markers.map(marker => {
                if (marker === targetMarker) marker.showInfo = true;
                return marker;
            })
        });
    }

    handleMarkerClose(targetMarker) {
        this.setState({
            markers: this.state.markers.map(marker => {
                if (marker === targetMarker) marker.showInfo = false;
                return marker;
            })
        });
    }

    render() {
        const {center} = this.state;
        return (
            <div style={{height: "100%"}}>

                <InitialMap
                    googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmRkS98qIZpwi78yl4j3wlGyFGEE-lzYs&v=3.exp&libraries=geometry,drawing,places"
                    loadingElement={<div style={{height: `100%`}}/>}
                    containerElement={<div style={{height: !!this.props.height ? this.props.height : `900px`}}/>}
                    mapElement={<div style={{height: `100%`}}/>}
                    markers={this.state.markers}
                    zoom={this.state.zoom}
                    center={center}
                    handleMarkerClick={this.handleMarkerClick}
                    handleMarkerClose={this.handleMarkerClose}
                    bounds={this.state.bounds}
                    onMapMounted={this.onMapMounted}
                    onZoomChanged={this.onZoomChanged.bind(this)}
                />

            </div>
        )
    }
}

export default DashboardMapContainer
