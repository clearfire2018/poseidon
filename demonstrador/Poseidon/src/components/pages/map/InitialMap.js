/*global google*/
import React from "react";
import {
    withGoogleMap,
    GoogleMap,
    InfoWindow,
    Marker,
} from "react-google-maps"
import SearchBox from "react-google-maps/lib/components/places/SearchBox";
import {Card} from "semantic-ui-react";
const initialMap = withGoogleMap(props => {

    const category = {
        0: {
            icon: '/images/fire.png',
            type: "Incêndio"
        },
        1: {
            icon: '/images/tree.png',
            type: "Prevenção"
        },
        2: {
            icon: '/images/trash.png',
            type: "Lixo"
        }
    };
    
     const stages = {
        0: {
            type: "Pending"
        },
        1: {
            type: "In Progress"
        },
        2: {
            type: "Solved"
        }
    };
    
    return (
        <GoogleMap
            ref={props.onMapMounted}
            zoom={props.zoom}
            center={props.center}
            onZoomChanged={props.onZoomChanged}
            onClick={props.handleMapClick}
        >

           {/* <SearchBox
                ref={props.onSearchBoxMounted}
                bounds={props.bounds}
                controlPosition={google.maps.ControlPosition.TOP_LEFT}
                onPlacesChanged={props.onPlacesChanged}
            >
                <input
                    type="text"
                    placeholder="Customized your placeholder"
                    style={{
                        boxSizing: `border-box`,
                        border: `1px solid transparent`,
                        width: `240px`,
                        height: `32px`,
                        marginTop: `27px`,
                        padding: `0 12px`,
                        borderRadius: `3px`,
                        boxShadow: `0 2px 6px rgba(0, 0, 0, 0.3)`,
                        fontSize: `14px`,
                        outline: `none`,
                        textOverflow: `ellipses`,
                    }}
                />
            </SearchBox>*/}

            {props.markers.map((marker, index) => (

                <Marker
                    key={index}
                    icon={category[marker.propertyMap.report_category].icon}
                    position={{
                        lat: parseFloat(marker.propertyMap.report_latitude, 10),
                        lng: parseFloat(marker.propertyMap.report_longitude, 10)
                    }}
                    onClick={() => props.handleMarkerClick(marker)}
                    onRightClick={() => props.handleMarkerDblClick(marker)}
                >
                    {marker.showInfo && (
                        <InfoWindow onCloseClick={() => props.handleMarkerClick}>
                            <div>
                                <Card>
                                    <Card.Content>
                                        <Card.Header> {marker.propertyMap.report_description}</Card.Header>
                                        <Card.Description>{category[marker.propertyMap.report_category].type}</Card.Description>
                                        <Card.Description>{stages[marker.propertyMap.report_state].type}</Card.Description>
                                        

                                    </Card.Content>
                                    <Card.Content extra>
                                        <div style={{textAlign:'center'}}>
                                            <a href={"/report/" + marker.key.name}>Click here to see report page</a>
                                        </div>
                                    </Card.Content>
                                </Card>
                            </div>
                        </InfoWindow>
                    )}

                </Marker>

            ))}

        </GoogleMap>
    )
});
export default initialMap;

