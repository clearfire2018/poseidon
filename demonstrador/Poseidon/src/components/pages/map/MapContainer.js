/*global google*/
import InitialMap from './InitialMap';
import axios from 'axios';
import React, {Component} from 'react';

const refs = {};

export default class MainMap extends Component {
    constructor(props) {
        super(props);
        this.state = {
            markers: [],
            bounds: null,
            center: {
                lat: 39.3999,
                lng: -8.2245
            },
            cursor: "",
            zoom: 7
        };
        this.handleMapClick = this.handleMapClick.bind(this);
        this.handleMarkerClick = this.handleMarkerClick.bind(this);
        this.handleMarkerClose = this.handleMarkerClose.bind(this);
        this.onMapMounted = this.onMapMounted.bind(this);
    }

    componentWillMount() {
        if (this.props.show === true) {
            this.requestMarkers();
        }
    };

    requestMarkers = () => {

        const {cursor} = this.state;

            axios.post('https://henrique-45707.appspot.com/rest/reports/listAll?cursor=' + cursor , {
                username: localStorage.getItem("user"),
                tokenID: localStorage.getItem("token")
            }).then((response) => {
                let addmarker = response.data.reports;
                addmarker = addmarker.concat(this.state.markers);
                this.setState({
                    markers: addmarker,
                    all_markers: addmarker,
                    newCursor: response.data.cursor
                }, () => {
                    if(this.state.newCursor !== this.state.cursor)
                        this.setState({
                            cursor: this.state.newCursor
                        }, ()=> {
                            this.requestMarkers();
                        })
                });
            }).catch(() => {
                console.error("error listing reports")
            });

    };


    onMapMounted = ref => {
        refs.map = ref;
        console.log(refs.map)
    };

    onSearchBoxMounted = ref => {
        refs.searchBox = ref;
    };

    onZoomChanged = () => {
        this.setState({
            zoom: refs.map.getZoom()
        });
    };

    sort() {
        const temp = this.state.all_markers;
        const markers_temp = [];
        const {category} = this.props;
        if (category === 4) {
            this.setState({
                markers: this.state.all_markers
            })

        } else {
            temp.forEach((marker) => {
                if (parseInt(marker.propertyMap.report_category, 10) === this.props.category) {
                    markers_temp.push(marker);
                }
            });
            this.setState({
                markers: markers_temp
            })
        }
        //this.state.markers.map((marker) => {marker.showInfo = false});
        this.state.markers.forEach((marker) => {marker.showInfo = false});
    }

    handleMapClick = (event) => {
        const lat = event.latLng.lat();
        const lng = event.latLng.lng();
        console.log("Map clicked! " + lat + ", " + lng);
    };

    handleMarkerClick(targetMarker) {
        this.setState({
            all_markers: this.state.all_markers.map(marker => {
                if (marker === targetMarker) marker.showInfo = true;
                return marker;
            })
        })

    }

    handleMarkerClose(targetMarker) {
        this.setState({
            all_markers: this.state.all_markers.map(marker => {
                if (marker === targetMarker) marker.showInfo = false;
                return marker;
            })
        })
    }

    handleMarkerDblClick(targetMarker) {
        const lat = parseFloat(targetMarker.propertyMap.report_latitude);
        const lng = parseFloat(targetMarker.propertyMap.report_longitude);
        this.setState({
            center: {
                lat: lat,
                lng: lng
            },
            zoom: 9
        },()=>{this.setState();})
    }

    render() {
        const {center} = this.state;
        return (
            <div style={{height: "100%"}}>

                <InitialMap
                    googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmRkS98qIZpwi78yl4j3wlGyFGEE-lzYs&v=3.exp&libraries=geometry,drawing,places"
                    loadingElement={<div style={{height: `100%`}}/>}
                    containerElement={<div style={{height: !!this.props.height ? this.props.height : `900px`}}/>}
                    mapElement={<div style={{height: `100%`}}/>}
                    markers={this.state.markers}
                    lat={center.lat}
                    lng={center.lng}
                    zoom={this.state.zoom}
                    center={center}
                    handleMapClick={this.handleMapClick}
                    handleMarkerClick={this.handleMarkerClick}
                    handleMarkerClose={this.handleMarkerClose}
                    handleMarkerDblClick={this.handleMarkerDblClick.bind(this)}
                    bounds={this.state.bounds}
                    onMapMounted={this.onMapMounted}
                    onZoomChanged={this.onZoomChanged.bind(this)}
                    /* onBoundsChanged={this.onBoundsChanged.bind(this)}*/
                    onSearchBoxMounted={this.onSearchBoxMounted.bind(this)}
                   
                />

            </div>
        )
    }
}
