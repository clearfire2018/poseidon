
import React from "react";
import {
    withGoogleMap,
    GoogleMap,
    Marker,
    Circle
}from "react-google-maps"



const reportFormMap = withGoogleMap(props =>{
    return (
        <GoogleMap
            ref={props.onMapMounted}
            defaultZoom={6}
            defaultCenter={{lat: 38.7223, lng: -9.1393}}
            onClick={props.handleMapClick}
        >
                <Marker
                    position={{lat:props.lat,lng:props.lng}}
                   
                />
        
                <Circle 
                    center={{lat:props.lat,lng:props.lng}}
                    defaultVisible={true}
                    radius={props.radius}
                    onDrag={props.handleCircleDrag}

                />
                

        </GoogleMap>
    )
});

export default reportFormMap ;