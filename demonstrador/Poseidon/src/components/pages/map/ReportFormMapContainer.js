/*global google*/

import React, {Component} from 'react';
import ReportMap from "./ReportFormMap";
 
const refs = {};

export default class ReportFormMapContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lat: 38.7223,
            lng: -9.1393,

        };
        this.handleMapClick = this.handleMapClick.bind(this);
        this.handleCircleDrag = this.handleCircleDrag.bind(this);
    }
    
    onMapMounted = ref => {
    
        refs.map = ref;
    
    };
    
    
    handleMapClick(event) {
        const lat = event.latLng.lat();
        const lng = event.latLng.lng();
        
        console.log("Map clicked! " + lat + ", " + lng);
        this.props.handleMarker(lat, lng);
    }
    
     handleCircleDrag(event) {
        
         
         
    }

    render() {
            console.log("map radius", this.props.radius)
        return (
            <div style={{height: "100%"}}>

                <ReportMap
                    googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmRkS98qIZpwi78yl4j3wlGyFGEE-lzYs&v=3.exp"
                    loadingElement={<div style={{height: `100%`}}/>}
                    containerElement={<div style={{height: !!this.props.height ? this.props.height : `570px`}}/>}
                    mapElement={<div style={{height: `100%`}}/>}
                    lat={this.props.lat}
                    lng={this.props.lng}
                    handleMapClick={this.handleMapClick}
                    handleCircleDrag={this.handleCircleDrag}
                    radius={this.props.radius}
    
                />

            </div>
        )
    }
}
