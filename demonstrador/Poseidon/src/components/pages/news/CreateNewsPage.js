import React, {Component} from "react";
import NewsForm from "../../forms/NewsForm";
import axios from "axios/index";

class CreateNewsPage extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    submit = data => {
        const {username, token} = this.state;

        axios.post('https://henrique-45707.appspot.com/rest/user/info', {
            acess: {
                username: username,
                tokenID: token,
            },
           title:data.title,
            subtitle:data.subtitle,
            text:data.text,
        }).then((response) => {
            this.setState({
                user: response.data
            }, () => {
                this.setState({
                    loading: false
                })
            })
        })
    };


    render() {



        return (
            <div>
                <h1>Create News page</h1>

                <div>
                    <NewsForm submit={this.submit}/>
                </div>


            </div>
        );
    }

}

export default CreateNewsPage;