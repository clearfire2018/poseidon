import React, {Component} from "react";
import axios from "axios";
import ReportForm from "../../forms/ReportForm";

class CreateEventPage extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    create_report = (param) => {
        const user = localStorage.getItem("user");
        const token = localStorage.getItem("token");
        const {data, lat,lng} = param;

        axios.post('https://henrique-45707.appspot.com/rest/reports/register', {
            acess: {
                username: user,
                tokenID: token
            },
            description: data.description,
            category: data.category,
            state: 0,
            publicPrivate: "Pub",
            marker: {
                latitude: lat,
                longitude: lng,
            }
        }).then(() => {
            console.log("Report created");
            this.props.history.push("/reports");
        }).catch(() => {
            alert("Error in creating report")
        });
    };

    submit = data => {
        this.create_report(data);
    };

    render() {

        return (
            <div className="ui container">
                <h1>Create Report page</h1>

                <div>
                    <ReportForm submit={this.submit} username={localStorage.getItem("user")}
                                token={localStorage.getItem("token")}/>


                </div>


            </div>
        );
    }

}

export default CreateEventPage;