import React, {Component} from 'react';
import axios from "axios/index";
import {List, Button} from "semantic-ui-react";
import ReportFormMapContainer from "../map/ReportFormMapContainer";
import {Link} from "react-router-dom";


class ReportPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.match.params.id,
            report: null,
            loading: true,
        };

    }

    componentWillMount() {
        const user = localStorage.getItem("user");
        const token = localStorage.getItem("token");
        const {id} = this.state;

        axios.post('https://henrique-45707.appspot.com/rest/reports/' + id, {
            username: user,
            tokenID: token,
        }).then((response) => {
            console.log("report,",response.data)
            this.setState({report: response.data}, () => {
                this.setState({loading: false});
            });
        }).catch((err) => {
            console.error("erro report: " + err);
        });
    }

      confirmReport = () => {
        const user = localStorage.getItem("user");
        const token = localStorage.getItem("token");
        const {id} = this.state;

        axios.post('https://henrique-45707.appspot.com/rest/reports/confirm/' + id , {
            username: user,
            tokenID: token

        }).then(() => {
            console.log("report confimed");

        });
    };
    
    render() {
        const {loading, report} = this.state;
        const types = ["Incêndio","Prevenção","Lixo"];

        if (!loading) return (
            <div className="ui container">
                <h1>{report.id}</h1>
                <List>
                    <List.Item> Category: {types[report.category]} </List.Item>
                    <List.Item> Description: {report.description} </List.Item>
                    <List.Item> Location: {report.marker.latitude}, {report.marker.longitude}  </List.Item>
                    <ReportFormMapContainer lat={parseFloat(report.marker.latitude)} lng={parseFloat(report.marker.longitude)}
                                            height={`450px`}/>
                </List>
                <Button primary as={Link} to='/reports'>Voltar</Button>
                <Button secondary onClick={this.confirmReport}>Confirm Report</Button>
            </div>

        );
        else return null;
    }
}

export default ReportPage;
