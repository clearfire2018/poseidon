import React, {Component} from "react";
import {Link} from "react-router-dom";
import MainMap from "../map/MapContainer";
import {Grid, Menu, Button, Dropdown, List} from "semantic-ui-react";
import CreateReport from "../reports/CreateReportPage";


class ReportsPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            category: 4,
            activeItem: "All"
        };
    }
    
    handleItemClick = (e, {name, value}) => this.setState({activeItem: name, category: value}, () => {
            this.child.sort();
        });


    handleCreateClick = (e, {name}) => this.setState({activeItem: name} );

    render() {
        const name = localStorage.getItem("user");
        const token = localStorage.getItem("token");
        const auth = !!token;
        const user = {
            name: name,
            token: token
        };

        return (
            <div className="ui container">
                <h1>Reports Map</h1>
                
                 <Grid>
                    <Grid.Column width={4}>
                        <Menu fluid vertical tabular>
                            <Menu.Item name='All' value={4} active={this.state.activeItem === 'All'}
                                       onClick={this.handleItemClick}/>
                            <Menu.Item name='Prevention' value={1} active={this.state.activeItem === 'Prevention'}
                                       onClick={this.handleItemClick}/>
                            <Menu.Item name='Garbage' value={2} active={this.state.activeItem === 'Garbage'}
                                       onClick={this.handleItemClick}/>
                            <Menu.Item name='Fire' value={0} active={this.state.activeItem === 'Fire'}
                                       onClick={this.handleItemClick}/>
                            <Link to="/create_report">
                             <Menu.Item style={{backgroundColor: '#3161c7', color: 'white'}} icon='plus circle' name='New Report' active={this.state.activeItem === 'New Report'}
                                       onClick={this.handleCreateClick}/>
                            </Link>
                            
                        </Menu>
                      {/*  <Container>

                            <label htmlFor="date">Date</label>
                            <Calendar
                                activeStartDate={this.state.date}
                                onChange={this.onChange}
                                value={this.state.date}
                                onClickDay={() => {
                                    this.getEventsOnDay()
                                }}
                             />
                        </Container>*/}
                    </Grid.Column>

                    <Grid.Column stretched width={12}>
                                   
                    <MainMap ref={instance => {this.child = instance}} user={user} show={true} category={this.state.category} height={`700px`}/>
            
                    </Grid.Column>
                </Grid>

            </div>
        );
    }

}

export default ReportsPage;