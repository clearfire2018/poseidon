
import React, {Component} from "react";
import axios from "axios/index";
import {List} from "semantic-ui-react";

class AccountDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: localStorage.getItem("user"),
            token: localStorage.getItem("token"),
            user: null,
            loading: true
        }
    }

    componentDidMount() {
        const {username, token} = this.state;

        axios.post('https://henrique-45707.appspot.com/rest/user/info', {
            username: username,
            tokenID: token,
        }).then((response) => {
            this.setState({
                user: response.data
            }, () => {
                this.setState({
                    loading: false
                })
            })
        })
    }

    render() {

        const {user} = this.state;

        if (!this.state.loading) {

            return (
                <div className="ui container">
                    <h1>Account details</h1>

                    <List>
                        <List.Item>
                            <b>Username:</b> {user.username}
                        </List.Item>
                        <List.Item>
                            <b>Name:</b> {user.name}
                        </List.Item>
                        <List.Item>
                            <b>Role:</b> {user.role}
                        </List.Item>
                        <List.Item>
                            <b>Contact:</b> {user.contact}
                        </List.Item>
                        <List.Item>
                            <b>Email:</b> {user.email}
                        </List.Item>
                    </List>


                </div>
            );

        } else return <div>Loading...</div>;
    }
}

export default AccountDetails;