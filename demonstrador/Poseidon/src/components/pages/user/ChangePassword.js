import React, {Component} from 'react';
import Axios from 'axios';
import ChangePasswordForm from "../../forms/ChangePasswordForm";

class ChangePassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            newPassword: "",
            confirmation: "",
            success: false,
            failure: false
        };

    }

    changeUserPassword = (data) => {
            Axios.post('https://henrique-45707.appspot.com/rest/user/change-password', {
                username: localStorage.getItem("user"),
                oldPassword: data.password,
                newPassword: data.new_password,
            })
    };

    submit = data => {
        this.changeUserPassword(data);
        this.props.history.push("/dashboard");
    };

    render() {
        return (
            <div>
                <h1>Change Password</h1>
                <ChangePasswordForm submit={this.submit}/>
            </div>

        )

    }

}


export default ChangePassword;
