import React, {Component} from "react";
import axios from "axios/index";
import {Button, Icon, List} from "semantic-ui-react";
import {Link} from "react-router-dom";
import DashboardMapContainer from "../map/DashboardMapContainer";

class DashboardPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: localStorage.getItem("user"),
            token: localStorage.getItem("token"),
            events: [],
            user: null,
            loading: true
        }
    }

    componentWillMount() {
       // this.requestUserBadges();
        this.requestUserInfo();
    }

    requestUserBadges = () => {

        const {username, token} = this.state;

        axios.post('https://henrique-45707.appspot.com/rest/awards/badges', {
            username: username,
            tokenID: token,
        }).then((res) => {
            console.log("awards, " , res.data)
        })

    };

    requestUserInfo = () => {
        const {username, token} = this.state;
        axios.post('https://henrique-45707.appspot.com/rest/user/info', {
            username: username,
            tokenID: token,
        }).then((response) => {
            this.setState({
                user: response.data
            }, () => {
                this.setState({
                    loading: false
                });
                this.requestEventList();
            });
        })
    };

    requestEventList = () => {
        const {username, token} = this.state;
        axios.post('https://henrique-45707.appspot.com/rest/events/list', {
            username: username,
            tokenID: token
        }).then((response) => {
            this.setState({events: response.data}, () => {
                console.log(this.state.events)
                this.getAuthorEvents();
            });
        });
    };

    getAuthorEvents = () => {
        const {events} = this.state;
        const user_events = [];
        events.events.map((event) => {
            if (event.propertyMap.event_user === this.state.username) {
                user_events.push(event);
            }
        });
        this.setState({
            user_events: user_events
        }, () => {
            this.setState({events_loaded: true})
        });
    };

    render() {
        const user = {
            name: localStorage.getItem("user"),
            token: localStorage.getItem("token")
        };

        const {user_events} = this.state || [];

        if (!this.state.loading && this.state.events_loaded) {

            const list_events = user_events.map((event) =>
                <List>
                    <List.Item as={Link} to={'/event/' + event.key.name}>
                        <Icon name="user"/> {event.key.name}
                    </List.Item>
                </List>
            );


            return (
                <div className="ui container">
                    <h1>Dashboard page</h1>
                    {this.state.user.role === "admin" && <Button as={Link} to={'/admin_panel'}>Admin panel</Button>}
                    {this.state.user_events.length !== 0 ?
                        (
                            <div>
                                <h3>User events</h3>
                                {this.state.events_loaded && list_events}
                            </div>
                        ) : (
                            <div>

                            </div>
                        )}
                    <h3>Region Map</h3>
                    <DashboardMapContainer city={this.state.user.city} height={`400px`}/>
                </div>
            );

        } else return null;
    }
}

export default DashboardPage;