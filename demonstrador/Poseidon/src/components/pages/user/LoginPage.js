import React, {Component} from "react";
import LoginForm from "../../forms/LoginForm";
import axios from 'axios';
import {Header, Image, Message} from "semantic-ui-react";
import {Link} from "react-router-dom";


class LoginPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: "",
            token: "",
            isLogged: false,
            auth: false
        };
    }

    componentDidMount() {
        console.log(this.props)
        this.setState({
            auth: !!localStorage.getItem("token")
        }, () => {
            this.setState({
                done: true
            })
        })
    }

    populateStorage = () => {
        const {username, token} = this.state;

        localStorage.setItem("user", username);
        localStorage.setItem("token", token);
    };

    login = data => {
        axios.post('https://henrique-45707.appspot.com/rest/login', {
            username: data.username,
            password: data.password,
        })
            .then((res) => {
                console.log("Token from server: " + res.data.tokenID);
                this.setState({
                    username: data.username,
                    token: res.data.tokenID,
                    isLogged: true
                }, () => {
                    this.populateStorage();
                    this.props.history.push("/dashboard");
                })

            }).catch(() => {
                alert("Username and password do not match.")
            }
        );
    };

    submit = data => {
        this.login(data);
    };

    render() {

        if (this.state.done) {

            return (
                <div className='login-form'>
                    <Header as='h2' textAlign='center'>
                        <Image as={Link} to="/" src='/images/1.jpg' size='large'/>
                        {' '}Log-in to your account
                    </Header>
                    <LoginForm submit={this.submit}/>
                    <Message>
                        New to us? <a href='/signup'>Sign Up</a>
                    </Message>

                </div>

            );
        } else
            return <h1> Loading </h1>
    }

}

export default LoginPage;