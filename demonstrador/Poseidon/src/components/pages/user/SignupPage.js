import React, {Component} from "react";
import SignupForm from "../../forms/SignupForm";
import axios from "axios";

class SignupPage extends Component {

    signup = (data, city) => {
        axios.post('https://henrique-45707.appspot.com/rest/register', {
            username: data.username,
            password: data.password,
            role: "user",
            contact: data.contact,
            name: data.name,
            email: data.email,
            city: city
        }).then(() => {
            console.log("User registado");
            this.props.history.push("/dashboard");
        }).catch(() => {
            alert("Error in user sign up.")
        });

    };

    submit = params => {
        this.signup(params.data, params.city);
    };

    render() {
        return (
            <div className="ui container">
                <h1>Signup page</h1>
                <SignupForm submit={this.submit}/>
            </div>
        );
    }
}

export default SignupPage;