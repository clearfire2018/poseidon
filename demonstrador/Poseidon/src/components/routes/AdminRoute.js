import React from 'react';
import { Route, Redirect } from "react-router-dom";

const AdminRoute = ({ isAuthenticated, isAdmin, component: Component, ...rest }) => (

    <Route
        {...rest}
        render={props =>
            (isAuthenticated && isAdmin )? <Component {...props} /> : <Redirect to="/" />}
    />

);

export default AdminRoute;