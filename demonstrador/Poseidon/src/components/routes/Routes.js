import React from "react";
import {Route, Switch} from "react-router-dom"
import LoginPage from '../pages/user/LoginPage';
import SignupPage from '../pages/user/SignupPage';
import DashboardPage from '../pages/user/DashboardPage';
import UserRoute from './UserRoute';
import GuestRoute from './GuestRoute';
import EventPage from "../pages/events/EventPage";
import ReportsPage from "../pages/reports/ReportsPage";
import CreateEventPage from "../pages/events/CreateEventPage";
import CreateReportPage from "../pages/reports/CreateReportPage";
import EventsPage from "../pages/events/EventsPage";

export default ({ location , isAuthenticaded }) =>
    <Switch>
        <GuestRoute location={location} isAuthenticated={isAuthenticated} path="/login" exact component={LoginPage}/>
        <GuestRoute location={location} isAuthenticated={isAuthenticated} path="/signup" exact component={SignupPage}/>
        <UserRoute location={location} isAuthenticated={isAuthenticated} path="/dashboard" exact component={DashboardPage}/>
        <Route location={location} isAuthenticated={isAuthenticated} path="/events" exact component={EventsPage}/>
        <Route location={location} isAuthenticated={isAuthenticated} path="/reports" exact component={ReportsPage}/>
        <UserRoute location={location} isAuthenticated={isAuthenticated} path="/create_event" exact component={CreateEventPage}/>
        <UserRoute location={location} isAuthenticated={isAuthenticated} path="/create_report" exact component={CreateReportPage}/>
        <Route location={location} path="/event/:id" exact component={EventPage}/>
    </Switch>;