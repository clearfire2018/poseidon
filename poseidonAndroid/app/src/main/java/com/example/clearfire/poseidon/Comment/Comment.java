package com.example.clearfire.poseidon.Comment;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by henry on 14-06-2018.
 */

public class Comment {

    private String commentText;
    private String usernameComment;
    private String id;

    private static final String COMMENT_ID = "comment_id";
    private static final String COMMENT_TEXT = "comment_text";
    private static final String COMMENT_USERNAME = "comment_username";
    private static final String PROPERTYMAP = "propertyMap";

    public Comment(String commentText, String id, String username) {
        this.commentText = commentText;
        this.usernameComment = username;
        this.id = id;
    }

    public Comment(JSONObject json) {
        this.commentText = "";
        this.usernameComment = "";
        this.id = "";

        try {
            json = json.getJSONObject(PROPERTYMAP);
            convertJsonToComment(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void convertJsonToComment(JSONObject json) {
        try {

            commentText = json.getString(COMMENT_TEXT);
            usernameComment = json.getString(COMMENT_USERNAME);
            //id = json.getString(COMMENT_ID);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getCommentText() {
        return commentText;
    }

    public String getIdComment() {
        return id;
    }

    public String getUsernameComment() {
        return usernameComment;
    }

}
