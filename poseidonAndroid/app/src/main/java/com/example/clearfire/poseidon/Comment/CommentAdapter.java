package com.example.clearfire.poseidon.Comment;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.clearfire.poseidon.R;

import java.util.List;

/**
 * Created by henry on 10-06-2018.
 */

public class CommentAdapter extends ArrayAdapter<Comment> {

    private Context context;

    public CommentAdapter(Context context, int resource, List<Comment> objects) {
        super(context, resource, objects);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = ((Activity) getContext()).getLayoutInflater().inflate(R.layout.commentestructure, parent, false);
        }

        TextView user = (TextView) convertView.findViewById(R.id.userComment);
        TextView comment = (TextView) convertView.findViewById(R.id.showComment);
        TextView like = (TextView) convertView.findViewById(R.id.showGostos);
        //TextView reply = (TextView) convertView.findViewById(R.id.showResponder);

        Comment com = getItem(position);
        user.setText(com.getUsernameComment());
        comment.setText(com.getCommentText());

        return convertView;
    }
}
