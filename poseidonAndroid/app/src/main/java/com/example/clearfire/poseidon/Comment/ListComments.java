package com.example.clearfire.poseidon.Comment;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.clearfire.poseidon.Event.Event;
import com.example.clearfire.poseidon.R;
import com.example.clearfire.poseidon.Utils.Requests;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by henry on 10-06-2018.
 */

public class ListComments extends AppCompatActivity {

    private ListView mShowComment;
    private EditText mAddComment;
    private ImageView mCheckComment;
    View focusView = null;

    private JSONArray comments;
    private CommentAdapter mCommentAdapter;
    private Comment com;
    List<Comment> lisComments = new ArrayList<>();

    private listAllComments mListTask = null;
    private DoComment doCommentTask = null;

    public static final String PREFS_NAME = "AUTH";
    private String baseUrl = "https://henrique-45707.appspot.com/rest/comment/";
    private String baseUrlAdd = "https://henrique-45707.appspot.com/rest/comment/";

    private String user;
    private String tokenId;
    private SharedPreferences settings;
    private String id;
    private String c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comentarios);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        mShowComment = (ListView) findViewById(R.id.comments);
        mAddComment = (EditText) findViewById(R.id.editText);
        mCheckComment = (ImageView) findViewById(R.id.imageComment);

        settings = getSharedPreferences(PREFS_NAME, 0);
        user = settings.getString("username", "");
        tokenId = settings.getString("tokenID", "");

        Intent intent = getIntent();
        id = intent.getStringExtra("idEvent");

        mListTask = new listAllComments();
        mListTask.execute((Void) null);

        mCheckComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mAddComment.setError(null);
                c = mAddComment.getText().toString();

                if (TextUtils.isEmpty(c)) {
                    mAddComment.setError("Adicione algum texto");
                    mAddComment.requestFocus();
                } else {

                    doCommentTask = new DoComment(c);
                    doCommentTask.execute((Void) null);
                    mAddComment.setText(null);
                }
            }

        });

    }


    private void startAdapter() {

        mCommentAdapter = new CommentAdapter(this, R.layout.comentarios, lisComments);
        mShowComment.setAdapter(mCommentAdapter);
    }


    /**
     * Represents an asynchronous list comments task used to show all
     * the comments to the user
     */
    public class listAllComments extends AsyncTask<Void, Void, String> {


        listAllComments() {

        }


        //Cancel background network operation if we do not have network connectivity
        @Override
        protected void onPreExecute() {


            ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = conMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected()
                    || (networkInfo.getType() != ConnectivityManager.TYPE_WIFI &&
                    networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);
            }
        }

        @Override
        protected String doInBackground(Void... params) {

            try {

                //constroi objecto jason com credenciais e efetua pedido
                JSONObject credentialsListComments = new JSONObject();
                credentialsListComments.accumulate("username", user);
                credentialsListComments.accumulate("tokenID", tokenId);

                return Requests.doPost(new URL(baseUrl + id + "/list"), credentialsListComments);


            } catch (Exception e) {
                return null;
            }

        }

        @Override
        protected void onPostExecute(final String result) {
            if (result != null) {

                try {
                    comments = new JSONArray(result);
                    Log.v("orihvio", "   " + comments.toString());

                    for (int i = 0; i < comments.length(); i++) {
                        com = new Comment(comments.getJSONObject(i));
                        lisComments.add(com);
                    }
                    Log.v("oivhoi", "   " + comments.length());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                startAdapter();

            }
        }

        @Override
        protected void onCancelled() {
        }
    }

    /**
     * Represents an asynchronous list comments task used to show all
     * the comments to the user
     */
    public class DoComment extends AsyncTask<Void, Void, String> {

        String comment;

        DoComment(String comment) {
            this.comment = comment;
        }


        //Cancel background network operation if we do not have network connectivity
        @Override
        protected void onPreExecute() {


            ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = conMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected()
                    || (networkInfo.getType() != ConnectivityManager.TYPE_WIFI &&
                    networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);
            }
        }

        @Override
        protected String doInBackground(Void... params) {

            try {

                //constroi objecto jason com credenciais e efetua pedido
                JSONObject credentialsAddComments = new JSONObject();
                JSONObject credentialsAcess = new JSONObject();

                credentialsAcess.accumulate("username", user);
                credentialsAcess.accumulate("tokenID", tokenId);
                credentialsAddComments.accumulate("acess", credentialsAcess);
                credentialsAddComments.accumulate("comment", comment);
                Log.v("eoifnv ", "   " + comment);
                return Requests.doPost(new URL(baseUrlAdd + id + "/post"), credentialsAddComments);


            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(final String result) {
            if (result != null) {
                com = new Comment(comment, id, user);
                lisComments.add(com);
                mCommentAdapter.notifyDataSetChanged();

            }
        }

        @Override
        protected void onCancelled() {
        }
    }
}
