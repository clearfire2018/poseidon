package com.example.clearfire.poseidon.Event;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.clearfire.poseidon.Report.Map;
import com.example.clearfire.poseidon.Utils.Calendares;
import com.example.clearfire.poseidon.R;
import com.example.clearfire.poseidon.Utils.Requests;
import com.example.clearfire.poseidon.User;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;

import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;

/**
 * Created by henry on 16-05-2018.
 */

public class AddEvent extends AppCompatActivity {

    private AddEventTask mAddEventTask = null;
    private SendPhoto sendPhotoTask = null;

    private EditText mAddTitle;
    private EditText mAddDescription;
    private ImageView mAddImage;
    private ImageView mGetPhoto;
    private ImageView mDate;
    private PlaceAutocompleteFragment places;
    private Bitmap biti;

    private String user;
    private String tokenId;
    private String title;
    private String descrip;
    private String identifier;
    private String location;
    private String endDate;
    private String beginDate;
    private Context context;
    private Calendares c;


    public static final int PICK_IMAGE = 1;
    private static final String TAG = "AddEvent";
    public static final String PREFS_NAME = "AUTH";
    private SharedPreferences settings;

    private String baseUrl = "https://henrique-45707.appspot.com/gcs/henrique-45707.appspot.com/";
    private String baseUrlAdd = "https://henrique-45707.appspot.com/rest/events/register";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_event);

        context = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.AddEvento);
        setSupportActionBar(toolbar);

        ImageView cancelEvento = (ImageView) findViewById(R.id.cancelEvento);
        cancelEvento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddEvent.this, User.class);
                startActivity(intent);
                finish();
            }
        });

        mAddTitle = (EditText) findViewById(R.id.event_title);
        mAddDescription = (EditText) findViewById(R.id.add_event_description);
        mAddImage = (ImageView) findViewById(R.id.add_image_event);
        mGetPhoto = (ImageView) findViewById(R.id.addPhoto);


        places = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setCountry("PT")
                .build();

        places.setFilter(typeFilter);
        places.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                location = place.getName().toString();
                Log.v("vwrb", " " + place.getName() + "  " + place.getAddress());
                Toast.makeText(getApplicationContext(), place.getName(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(Status status) {

                Toast.makeText(getApplicationContext(), status.toString(), Toast.LENGTH_SHORT).show();

            }
        });

        mDate = (ImageView) findViewById(R.id.choose_date);

        mDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                c = new Calendares(AddEvent.this);
                c.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        Log.v("oooooooooooooo", " " + getInit() + "  " + getFinale());

                        beginDate = getInit();

                        endDate = getFinale();
                    }
                });
                c.show();
            }
        });


        mGetPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Selected Image"), PICK_IMAGE);
            }
        });


        settings = getSharedPreferences(PREFS_NAME, 0);
        user = settings.getString("username", "");
        tokenId = settings.getString("tokenID", "");

        Log.v("iiiiiiiiii", " " + beginDate + "  " + endDate);


    }

    public String getInit() {
        return beginDate;
    }

    public String getFinale() {
        return endDate;
    }

    public void setInit(String init) {
        this.beginDate = init;
    }

    public void setFinale(String finalDate) {
        this.endDate = finalDate;
    }

    private void attemptAddEvent() {
        if (mAddEventTask != null) {
            return;
        }

        mAddTitle.setError(null);
        mAddDescription.setError(null);

        title = mAddTitle.getText().toString();
        descrip = mAddDescription.getText().toString();


        if (beginDate == null) {
            Toast.makeText(getApplicationContext(), "Selecione uma data para o evento", Toast.LENGTH_SHORT).show();
        }

        View focusView = null;
        boolean cancel = false;

        if (TextUtils.isEmpty(descrip)) {
            mAddDescription.setError("Introduza uma descrição");
            focusView = mAddDescription;
            cancel = true;
        }

        if (descrip.length() > 200) {
            mAddDescription.setError("Introduza uma descrição mais curta");
            focusView = mAddDescription;
            cancel = true;
        }

        if (TextUtils.isEmpty(title)) {
            mAddTitle.setError("Introduza um titulo");
            focusView = mAddTitle;
            cancel = true;
        }


        if (title.length() > 30) {
            mAddDescription.setError("Introduza um titulo mais curto");
            focusView = mAddDescription;
            cancel = true;
        }

        if (TextUtils.isEmpty(location)) {
            Toast.makeText(getApplicationContext(), "Introduza uma localização", Toast.LENGTH_SHORT).show();
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {

            mAddEventTask = new AddEventTask(user, tokenId, title, descrip, location, beginDate, endDate);
            mAddEventTask.execute((Void) null);
            Toast.makeText(AddEvent.this, "Evento adicionado com sucesso", Toast.LENGTH_LONG).show();

            Intent intent = new Intent(AddEvent.this, User.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK && data.getData() != null) {
            Uri uri = data.getData();
            mGetPhoto.setVisibility(View.GONE);
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                biti = Bitmap.createScaledBitmap(bitmap, mAddImage.getWidth(), mAddImage.getHeight(), false);
                mAddImage.setImageBitmap(biti);


            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_pass, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.checkP:
                attemptAddEvent();
            default:
                return super.onContextItemSelected(item);
        }
    }

    /**
     * Represents an asynchronous add event task used to add an event
     * submited by the user.
     */
    public class AddEventTask extends AsyncTask<Void, Void, String> {

        AddEventTask(String username, String tokenID, String titlee, String description, String locationn, String begin, String finalDate) {
            user = username;
            tokenId = tokenID;
            title = titlee;
            descrip = description;
            location = locationn;
            beginDate = begin;
            endDate = finalDate;
        }


        //Cancel background network operation if we do not have network connectivity
        @Override
        protected void onPreExecute() {
            ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = conMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected()
                    || (networkInfo.getType() != ConnectivityManager.TYPE_WIFI &&
                    networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);
            }
        }

        @Override
        protected String doInBackground(Void... params) {

            try {
                //constroi objecto jason com credenciais e efetua pedido
                JSONObject credentialsAddEvent = new JSONObject();
                JSONObject credentialsAcess = new JSONObject();
                credentialsAcess.accumulate("username", user);
                credentialsAcess.accumulate("tokenID", tokenId);
                credentialsAddEvent.accumulate("acess", credentialsAcess);
                credentialsAddEvent.accumulate("name", title);
                credentialsAddEvent.accumulate("description", descrip);
                credentialsAddEvent.accumulate("beginDate", beginDate);
                credentialsAddEvent.accumulate("endDate", endDate);


                return Requests.doPost(new URL(baseUrlAdd), credentialsAddEvent);


            } catch (Exception e) {
                return null;
            }

        }

        @Override
        protected void onPostExecute(final String result) {
            mAddEventTask = null;
            if (result != null) {
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("id", result);
                editor.commit();
                finish();

                Log.v(TAG, result);

                sendPhotoTask = new SendPhoto(biti);
                sendPhotoTask.execute((Void) null);

            }

        }

        @Override
        protected void onCancelled() {

        }

    }

    /**
     * Represents an asynchronous send photo task used to upload a photo
     * submited by the user.
     */
    public class SendPhoto extends AsyncTask<Void, Void, String> {

        Bitmap mImage;


        SendPhoto(Bitmap image) {
            mImage = image;
        }


        //Cancel background network operation if we do not have network connectivity
        @Override
        protected void onPreExecute() {
            ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = conMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected()
                    || (networkInfo.getType() != ConnectivityManager.TYPE_WIFI &&
                    networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);
            }
        }

        @Override
        protected String doInBackground(Void... params) {

            try {

                identifier = settings.getString("id", "");

                return Requests.doPostImage(new URL(baseUrl + identifier), mImage);

            } catch (Exception e) {
                return e.toString();
            }

        }

        @Override
        protected void onPostExecute(final String result) {
            if (result != null) {

                Log.v(TAG, "Sucesso");

            }
        }

        @Override
        protected void onCancelled() {

        }

    }
}
