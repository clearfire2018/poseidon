package com.example.clearfire.poseidon.Event;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.example.clearfire.poseidon.R;
import com.example.clearfire.poseidon.Utils.Requests;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.net.URL;

public class Event implements Serializable {
    private String baseUrlImg = "https://henrique-45707.appspot.com/gcs/henrique-45707.appspot.com/";
    private String id;
    private String title;
    private String description;
    private String author;
    private String beginDate;
    private String endDate;
    private String location;
    private String city;
    private int numlike;
    private int numsign;
    private Context context;

    private static final String EVENT_ID = "name";
    private static final String EVENT_TITLE = "event_name";
    private static final String EVENT_DESCRIPTON = "event_description";
    private static final String EVENT_AUTHOR = "event_user";
    private static final String EVENT_NUM_LIKES = "event_numLikes";
    private static final String EVENT_NUM_SINGS = "event_numSignups";
    private static final String KEY = "key";
    private static final String PROPERTYMAP = "propertyMap";
    private static final String EVENT_BEGIN = "event_begin";
    private static final String EVENT_END = "event_end";
    private static final String EVENT_LOCATION = "event_location";
    private static final String EVENT_CITY = "event_city";
    private static final String EVENT_VERIFY = "event_isverified";

    private static final String TAG = "Event";

    private Bitmap biti;


    public Event(String id, String title, String description, String author,
                 int numlike, int numsign, String beginDate, String endDate, String location, String city) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.author = author;
        this.numlike = numlike;
        this.numsign = numsign;
        this.beginDate = beginDate;
        this.endDate = endDate;
        this.location = location;
        this.city = city;
    }

    public Event() {
        this.id = "";
        this.title = "";
        this.description = "";
        this.author = "";
        this.beginDate = "";
        this.endDate = "";
        this.location = "";
        this.city = "";
        this.numlike = 0;
        this.numsign = 0;
        this.context = null;

    }

    public Event(JSONObject json,  Context context) {
        this.id = "";
        this.title = "";
        this.description = "";
        this.beginDate = "";
        this.endDate = "";
        this.author = "";
        this.location = "";
        this.city = "";
        this.numlike = 0;
        this.numsign = 0;
        this.context  =context;

        convertJsonToEvent(json);

    }

    private void convertJsonToEvent(JSONObject json) {
        try {
            id = json.getJSONObject(KEY).getString(EVENT_ID);
            json = json.getJSONObject(PROPERTYMAP);
            title = json.getString(EVENT_TITLE);
            description = json.getString(EVENT_DESCRIPTON);
            author = json.getString(EVENT_AUTHOR);
            beginDate = json.getString(EVENT_BEGIN);
            endDate = json.getString(EVENT_END);
            location = json.getString(EVENT_LOCATION);
            city = json.getString(EVENT_CITY);
            numlike = json.getInt(EVENT_NUM_LIKES);
            numsign = json.getInt(EVENT_NUM_SINGS);
            GetPhoto getPhotoTask = new GetPhoto(this, id, context);
            getPhotoTask.execute((Void) null);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setAttribute(JSONObject json) {
        convertJsonToEvent(json);
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getAuthor() {
        return author;
    }

    public int getNumLikes() {
        return numlike;
    }

    public int getNumSigns() {
        return numsign;
    }

    public void setLike(int numlike) {
        this.numlike = numlike;
    }

    public void addSign() {
        numsign++;
    }

    public Bitmap getImage() {
        return biti;
    }

    public void setImage(Bitmap biti) {
        this.biti = biti;
    }

    public String getBeginDate() {
        return beginDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public String getLocation() {
        return location;
    }

    public String getCity() {
        return city;
    }

    /**
     * Represents an asynchronous get photo task used to get the photo of
     * a specific event to show to the user
     */
    public class GetPhoto extends AsyncTask<Void, Void, Bitmap> {

        private final Context context;
        private String id;
        private Event event;

        GetPhoto(Event event,String id, Context context) {
            this.event = event;
            this.id = id;
            this.context = context;
        }


        //Cancel background network operation if we do not have network connectivity
        @Override
        protected void onPreExecute() {
            ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = conMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected()
                    || (networkInfo.getType() != ConnectivityManager.TYPE_WIFI &&
                    networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);
            }
        }

        @Override
        protected Bitmap doInBackground(Void... params) {

            try {
                Log.v("mweovhevo", "  " + id);
                return Requests.doGetImage(new URL(baseUrlImg + id));

            } catch (Exception e) {
                return null;
            }

        }

        @Override
        protected void onPostExecute(final Bitmap result) {
            if (result != null) {
                Bitmap biti = Bitmap.createScaledBitmap(result, 300, 160, false);
                if (event != null)
                    event.setImage(biti);
               event.setImage(biti);
            }else
            event.setImage( Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.noimage), 250, 160, false));
        }

        @Override
        protected void onCancelled() {

        }
    }


}