package com.example.clearfire.poseidon.Event;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.clearfire.poseidon.Comment.Comment;
import com.example.clearfire.poseidon.Comment.ListComments;
import com.example.clearfire.poseidon.Utils.Calendares;
import com.example.clearfire.poseidon.R;
import com.example.clearfire.poseidon.Utils.Requests;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by henry on 17-05-2018.
 */

public class ImageEventAdapter extends ArrayAdapter<Event> {

    private String baseUrlImg = "https://henrique-45707.appspot.com/gcs/henrique-45707.appspot.com/";
    private String baseUrlId = "https://henrique-45707.appspot.com/rest/events/";
    private String baseUrlLike = "https://henrique-45707.appspot.com/rest/events/like/";
    private String baseUrlFollow = "https://henrique-45707.appspot.com/rest/events/signup/";
    public static final String PREFS_NAME = "AUTH";

    private Context context;
    private Bitmap defaultImage;
    private Map<Integer, Bitmap> loadImage;
    private SharedPreferences settings;
    private String user;
    private String tokenId;


    public ImageEventAdapter(Context context, int resource, List<Event> objects) {
        super(context, resource, objects);
        this.context = context;
        loadImage = new HashMap<>();

        settings = context.getSharedPreferences(PREFS_NAME, 0);
        user = settings.getString("username", "");
        tokenId = settings.getString("tokenID", "");
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = ((Activity) getContext()).getLayoutInflater().inflate(R.layout.single_event, parent, false);
        }

        ExpandableTextView expandableTextView = (ExpandableTextView) convertView.findViewById(R.id.expand_text_view);
        expandableTextView.setOnExpandStateChangeListener(new ExpandableTextView.OnExpandStateChangeListener() {
            @Override
            public void onExpandStateChanged(TextView textView, boolean isExpanded) {

            }
        });
        final int mPosition = position;
        Bitmap image = null;
        ImageView photoEvent = (ImageView) convertView.findViewById(R.id.setImageEvent);
        final ImageView eventlike = (ImageView) convertView.findViewById(R.id.show_event_like);


        ImageView eventfollow = (ImageView) convertView.findViewById(R.id.show_event_follow);
        //ImageView calendarEvent = (ImageView) convertView.findViewById(R.id.event_calendar);
        TextView titleEvent = (TextView) convertView.findViewById(R.id.setTitleEvent);
        TextView eventLoc = (TextView) convertView.findViewById(R.id.setEventLocation);
        final TextView showLikes = (TextView) convertView.findViewById(R.id.show_likes);
        final TextView showParticipants = (TextView) convertView.findViewById(R.id.show_event_participant);
        TextView showComments = (TextView) convertView.findViewById(R.id.commentEvent);
        TextView showBeginDate = (TextView) convertView.findViewById(R.id.showDateBegin);
        TextView showEndDate = (TextView) convertView.findViewById(R.id.showDateEnd);

        final Event event= getItem(position);


        titleEvent.setText(event.getTitle());


        Long dateLong = Long.parseLong(event.getBeginDate());
        Date dateD = new Date(dateLong);
        Calendar date = Calendar.getInstance();
        date.setTime(dateD);
        String begin = "" + date.get(date.DAY_OF_MONTH) + "-" + date.get(date.DAY_OF_WEEK) + "-" + date.get(date.YEAR);

        Long dateLon = Long.parseLong(event.getEndDate());
        Date dateE = new Date(dateLon);
        Calendar dates = Calendar.getInstance();
        date.setTime(dateE);
        String end = "" + dates.get(date.DAY_OF_MONTH) + "-" + dates.get(date.DAY_OF_WEEK) + "-" + dates.get(date.YEAR);


        expandableTextView.setText(event.getDescription());
        showLikes.setText(event.getNumLikes() + "");
        showParticipants.setText(event.getNumSigns() + "");
        eventLoc.setText(event.getLocation() + " - " + event.getCity());
        showBeginDate.setText("De: " + begin);
        showEndDate.setText("Até: " + end);


        showComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ListComments.class);
                intent.putExtra("idEvent",event.getId());
                context.startActivity(intent);
            }
        });

        eventlike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DoLike doLikeTask = new DoLike(event, showLikes);
                doLikeTask.execute((Void) null);
            }
        });

        eventfollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                event.addSign();
                Follow followTask = new Follow(event, showParticipants);
                followTask.execute((Void) null);
            }
        });

        photoEvent.setImageBitmap(event.getImage());

        return convertView;
    }

    /**
     * Represents an asynchronous get photo task used to get the photo of
     * a specific event to show to the user
     */
    public class GetPhoto extends AsyncTask<Void, Void, Bitmap> {

        private String id;
        private ImageView imgView;
        private Event event;
        private Bitmap biti;

        GetPhoto(ImageView imgView, Event event, Bitmap biti, String id) {
            this.event = event;
            this.id = id;
            this.imgView = imgView;
            this.biti = biti;
        }


        //Cancel background network operation if we do not have network connectivity
        @Override
        protected void onPreExecute() {
            ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = conMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected()
                    || (networkInfo.getType() != ConnectivityManager.TYPE_WIFI &&
                    networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);
            }
        }

        @Override
        protected Bitmap doInBackground(Void... params) {

            try {
                Log.v("mweovhevo", "  " + id);
                return Requests.doGetImage(new URL(baseUrlImg + id));

            } catch (Exception e) {
                return null;
            }

        }

        @Override
        protected void onPostExecute(final Bitmap result) {
            if (result != null) {
                biti = Bitmap.createScaledBitmap(result, 300, 160, false);
                if (event != null)
                    event.setImage(biti);
                imgView.setImageBitmap(biti);
            }
        }

        @Override
        protected void onCancelled() {

        }
    }

    /**
     * Represents an asynchronous to like a event
     */
    public class DoLike extends AsyncTask<Void, Void, String> {

        Event event;
        String id;
        TextView showLikes;

        int count;


        DoLike(Event event, TextView showLikes) {
            this.event = event;
            id = event.getId();
            this.showLikes = showLikes;
            count = event.getNumLikes();
            Log.v("owirhbrioh", "  " + count);
        }

        //Cancel background network operation if we do not have network connectivity
        @Override
        protected void onPreExecute() {

            ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = conMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected()
                    || (networkInfo.getType() != ConnectivityManager.TYPE_WIFI &&
                    networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);
            }
        }

        @Override
        protected String doInBackground(Void... params) {

            try {

                //constroi objecto jason com credenciais e efetua pedido
                JSONObject credentialsListEvent = new JSONObject();
                credentialsListEvent.accumulate("username", user);
                credentialsListEvent.accumulate("tokenID", tokenId);
                return Requests.doPost(new URL(baseUrlLike + id), credentialsListEvent);


            } catch (Exception e) {
                return null;
            }

        }

        @Override
        protected void onPostExecute(final String result) {
            if (result != null) {
                showLikes.setText(result);
                event.setLike(Integer.parseInt(result));
            }

        }

        @Override
        protected void onCancelled() {
        }
    }

    /**
     * Represents an asynchronous to follow a event
     */
    public class Follow extends AsyncTask<Void, Void, String> {

        private Event event;
        String id;
        TextView showParticipants;

        Follow(Event event, TextView showParticipants) {
            this.event = event;
            id = event.getId();
            this.showParticipants = showParticipants;
        }

        //Cancel background network operation if we do not have network connectivity
        @Override
        protected void onPreExecute() {

            ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = conMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected()
                    || (networkInfo.getType() != ConnectivityManager.TYPE_WIFI &&
                    networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);
            }
        }

        @Override
        protected String doInBackground(Void... params) {

            try {

                //constroi objecto jason com credenciais e efetua pedido
                JSONObject credentialsListEvent = new JSONObject();
                credentialsListEvent.accumulate("username", user);
                credentialsListEvent.accumulate("tokenID", tokenId);
                return Requests.doPost(new URL(baseUrlFollow + id), credentialsListEvent);


            } catch (Exception e) {
                return null;
            }

        }

        @Override
        protected void onPostExecute(final String result) {
            if (result != null) {
                showParticipants.setText(result);
            }
        }

        @Override
        protected void onCancelled() {
        }
    }


}
