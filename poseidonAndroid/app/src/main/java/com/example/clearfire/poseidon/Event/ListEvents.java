package com.example.clearfire.poseidon.Event;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.clearfire.poseidon.Report.Map;
import com.example.clearfire.poseidon.User;
import com.example.clearfire.poseidon.Utils.Cache;
import com.example.clearfire.poseidon.R;
import com.example.clearfire.poseidon.Utils.Requests;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;

/**
 * Created by henry on 16-05-2018.
 */
public class ListEvents extends AppCompatActivity {

    public static final String PREFS_NAME = "AUTH";
    private String baseUrl = "https://henrique-45707.appspot.com/rest/events/list/info";
    private static final String STATE_LIST = "State Adapter Data";


    private ListView mListEvent;
    private FloatingActionButton fab;
    private SharedPreferences settings;
    private JSONArray events;
    private Cache cache = null;
    private listAll listEventsTask = null;
    private ImageEventAdapter mImageEventAdapter;
    private ArrayList<Event> listOfEvents = new ArrayList<>();

    private String user;
    private String tokenId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bottomlistevent);
        settings = getSharedPreferences(PREFS_NAME, 0);
        user = settings.getString("username", "");
        tokenId = settings.getString("tokenID", "");
        mListEvent = (ListView) findViewById(R.id.EventList);

        try {
            Log.v("CACHE", "CACHEEEEEEEE");

            events = new JSONArray((String) Cache.readObject(this, "events"));
            Log.v("CACHE", events.toString() + "");
        } catch (Exception e) {
            e.printStackTrace();
        }

        listEventsTask = new listAll("");
        listEventsTask.execute((Void) null);

        final BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.navigationView);
        bottomNavigationView.setSelectedItemId(R.id.eventos);
        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {


                        switch (item.getItemId()) {
                            case R.id.eventos:
                                if (!(ListEvents.this instanceof ListEvents)) {
                                    Intent intent2 = new Intent(ListEvents.this, ListEvents.class);
                                    startActivity(intent2);
                                    finish();
                                }
                                break;

                            case R.id.reports:
                                Intent intent = new Intent(ListEvents.this, User.class);
                                startActivity(intent);
                                finish();
                                break;
                        }
                        return false;
                    }
                });

        ImageView but = (ImageView) findViewById(R.id.eventOrReport);
        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bottomNavigationView.getSelectedItemId() == R.id.reports) {
                    Intent intent = new Intent(ListEvents.this, Map.class);
                    startActivity(intent);

                } else {
                    Intent intent = new Intent(ListEvents.this, AddEvent.class);
                    startActivity(intent);
                }
            }
        });

        // Save the ListView state (= includes scroll position) as a Parceble
        Parcelable state = mListEvent.onSaveInstanceState();


        mListEvent.onRestoreInstanceState(state);

    }

    private void startAdapter() {

        try {
            cache.writeObject(ListEvents.this, "events", events.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }
        mImageEventAdapter = new ImageEventAdapter(this, R.layout.activity_events, listOfEvents);
        mListEvent.setAdapter(mImageEventAdapter);
        for (int i = 0; i < events.length(); i++) {

            try {
                mImageEventAdapter.add(new Event(events.getJSONObject(i), this));
                Log.v("woeheiou", "   " + listOfEvents);
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }


    /**
     * Represents an asynchronous list events task used to show all
     * the events to the user
     */
    public class listAll extends AsyncTask<Void, Void, String> {

        String cursor;

        listAll(String cursor) {
            this.cursor = cursor;
        }


        //Cancel background network operation if we do not have network connectivity
        @Override
        protected void onPreExecute() {

            ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = conMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected()
                    || (networkInfo.getType() != ConnectivityManager.TYPE_WIFI &&
                    networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);
            }
        }

        @Override
        protected String doInBackground(Void... params) {

            try {

                //constroi objecto jason com credenciais e efetua pedido
                JSONObject credentialsListEvent = new JSONObject();
                credentialsListEvent.accumulate("username", user);
                credentialsListEvent.accumulate("tokenID", tokenId);
                return Requests.doPost(new URL(baseUrl + "?cursor=" + cursor), credentialsListEvent);


            } catch (Exception e) {
                return null;
            }

        }

        @Override
        protected void onPostExecute(final String result) {
            if (result != null) {
                SharedPreferences.Editor editor = settings.edit();

                if (result != null) {

                    try {


                        JSONObject event = new JSONObject(result);
                        events = event.getJSONArray("events");
                        String newCursor = event.getString("cursor");

                        if (!cursor.equals(newCursor)) {
                            cursor = newCursor;
                            listEventsTask = new listAll(cursor);
                            listEventsTask.execute((Void) null);
                        }
                        Log.v("olaaaa   ", result);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    editor.putString("list_id", result);
                    editor.commit();
                    startAdapter();
                }

            }
        }

        @Override
        protected void onCancelled() {
        }
    }


}
