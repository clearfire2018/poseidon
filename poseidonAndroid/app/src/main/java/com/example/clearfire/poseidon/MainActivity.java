package com.example.clearfire.poseidon;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.clearfire.poseidon.Utils.Requests;
import com.example.clearfire.poseidon.Worker.MainWorker;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;


public class MainActivity extends AppCompatActivity {


    private UserLoginTask mUserLoginTask = null;
    private EditText mUsername;
    private EditText mPassword;
    private Button mSignIn;
    private Button mRegister;
    private View mProgressView;
    private View mLoginFormView;
    private boolean cancel = false;

    public static final String PREFS_NAME = "AUTH";
    private String baseUrl = "https://henrique-45707.appspot.com/rest/login/";
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mUsername = findViewById(R.id.editUsername);
        mPassword = findViewById(R.id.editPassword);

        mSignIn = findViewById(R.id.sing_in);

        mSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mRegister = findViewById(R.id.register);

        mRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Register.class);
                startActivity(intent);
            }
        });

        mProgressView = findViewById(R.id.login_progress);
        mLoginFormView = findViewById(R.id.username_container);

    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mUserLoginTask != null) {
            return;
        }

        //Reset errors
        mUsername.setError(null);
        mPassword.setError(null);

        String username = mUsername.getText().toString();
        String pass = mPassword.getText().toString();

        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(pass) && !isPasswordValid(pass)) {
            mPassword.setError(getString(R.string.error_invalid_password));
            focusView = mPassword;
            cancel = true;
        }

        // Check for a valid username.
        if (TextUtils.isEmpty(username)) {
            mUsername.setError(getString(R.string.error_field_required));
            focusView = mUsername;
            cancel = true;
        } else if (!isUserValid(username)) {
            mUsername.setError(getString(R.string.error_invalid_user));
            focusView = mUsername;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            if (focusView != null)
                focusView.requestFocus();

        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            mUserLoginTask = new UserLoginTask(username, pass);
            mUserLoginTask.execute((Void) null);

        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            /*mLoginFormView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });*/

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    private boolean isUserValid(String username) {
        //TODO: Replace this with your own logic
        return username.toString().length() > 0;
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.toString().length() >= 2;
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, String> {

        private final String user;
        private final String pass;

        UserLoginTask(String username, String password) {
            user = username;
            pass = password;
        }


        //Cancel background network operation if we do not have network connectivity
        @Override
        protected void onPreExecute() {
            showProgress(true);
            ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = conMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected()
                    || (networkInfo.getType() != ConnectivityManager.TYPE_WIFI &&
                    networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                showProgress(false);
                Toast.makeText(getApplicationContext(), "Ligue a internet", Toast.LENGTH_SHORT).show();
                cancel(true);
            }

        }

        @Override
        protected String doInBackground(Void... params) {

            try {
                //constroi objecto jason com credenciais e efetua pedido
                JSONObject credentials = new JSONObject();
                credentials.accumulate("username", user);
                credentials.accumulate("password", pass);
                return Requests.doPost(new URL(baseUrl), credentials);

            } catch (Exception e) {
                cancel = false;
                mUserLoginTask = null;
                return null;
            }

        }

        @Override
        protected void onPostExecute(final String result) {
            showProgress(false);
            if (result != null) {
                JSONObject token = null;
                try {
                    //Parsing do resultado
                    token = new JSONObject(result);

                    if (token != null) {

                        Toast.makeText(getApplicationContext(), "Bem vindo " + user, Toast.LENGTH_SHORT).show();

                        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("tokenID", token.getString("tokenID"));
                        editor.putString("username", token.getString("username"));
                        editor.commit();
                        finish();

                        JSONObject user = token.getJSONObject("user");

                        if(user.getString("role").equals("worker")){
                            Intent intent = new Intent(MainActivity.this, MainWorker.class);
                            startActivity(intent);
                            finish();

                        } else {

                            Intent intent = new Intent(MainActivity.this, User.class);
                            startActivity(intent);
                            finish();
                        }

                    }
                } catch (JSONException e) {
                    //Wrong data sent by the server
                    Log.e("Authentication", e.toString());
                }
            } else {
                Toast.makeText(getApplicationContext(), "utilizador ou password erradas", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onCancelled() {
            Log.v("CANCEL", "AQUIIIIIIIIIIIIIIIII!!!!");
            cancel = false;
            mUserLoginTask = null;
        }
    }
}
