package com.example.clearfire.poseidon.Marker;

import android.graphics.Bitmap;

import com.example.clearfire.poseidon.Report.Map;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by henry on 07-06-2018.
 */

public class InfoMarker {
    private double lng;
    private double lat;
    private String id;
    private String type;
    private Calendar date;
    private String description;
    private String state;
    private int numCheck;
    private int grau;

    private static final String INFO_ID = "report_id";
    private static final String INFO_TYPE = "report_category";
    private static final String INFO_DESCRIPTION = "report_description";
    private static final String INFO_DATE = "report_date";

    //ainda n esta feito base de dados
    private static final String INFO_STATE = "event_author";
    private static final String INFO_NUM_CHECK = "event_numlike";


    private static final String TAG = "INFO";

    private Bitmap biti;


    public InfoMarker(String id, String type, String description, String state, int numCheck,
                      Calendar date, double lat, double lng, int grau) {
        this.id = id;
        this.type = type;
        this.description = description;
        this.state = state;
        this.numCheck = numCheck;
        this.date = date;
        this.biti = null;
        this.lat = lat;
        this.lng = lng;
        this.grau = grau;
    }

    public InfoMarker(String id, String type, String description, String state, int numCheck, Bitmap biti,
                      Calendar date, double lat, double lng, int grau) {
        this.id = id;
        this.type = type;
        this.description = description;
        this.state = state;
        this.numCheck = numCheck;
        this.biti = biti;
        this.date = date;
        this.lat = lat;
        this.lng = lng;
        this.grau = grau;
    }

    public float getColor() {
        float color = 0.f;
        switch (Integer.parseInt(type)) {
            case Map.TYPE_FIRE:
                color = Map.MARKER_COLOR_FIRE;
                break;
            case Map.TYPE_PREVENTION:
                color = Map.MARKER_COLOR_PREVENTION;
                break;
            case Map.TYPE_TRASH:
                color = Map.MARKER_COLOR_TRASH;
                break;
        }
        return color;
    }

    public String getId() {
        return id;
    }

    public String getType() {

        if (type.equals("0")) {
            return "Fogo";
        } else if (type.equals("1")) {
            return "Prenvenção";
        }
        return "Lixo";
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public String getDescription() {
        return description;
    }

    public int getNumCheck() {
        return numCheck;
    }

    public String getState() {
        return state;
    }

    public Bitmap getImage() {
        return biti;
    }

    public Calendar getDate() {
        return date;
    }

    public int getGrau() {
        return grau;
    }

    public void setImage(Bitmap biti) {
        this.biti = biti;
    }

    public void setState(String state){
        this.state = state;
    }

}
