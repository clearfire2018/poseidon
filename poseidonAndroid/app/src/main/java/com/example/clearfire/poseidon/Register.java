package com.example.clearfire.poseidon;

import android.app.Activity;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.clearfire.poseidon.Utils.Requests;

import org.json.JSONObject;

import java.net.URL;

/**
 * Created by henry on 27-03-2018.
 */

public class Register extends AppCompatActivity {

    private String baseUrl = "https://henrique-45707.appspot.com/rest/register/";
    private static final String TAG = "Register";

    private EditText mUsername;
    private EditText mPassword;
    private EditText mEmail;
    private EditText mConfirmation;
    private Spinner mRegion;
    private String selectedRegion = "Aveiro";
    private RegisterTask registerTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mUsername = findViewById(R.id.username);
        mPassword = findViewById(R.id.password);
        mEmail = findViewById(R.id.email);
        mConfirmation = findViewById(R.id.confirm);
        mRegion = findViewById(R.id.spinner);
        mRegion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedRegion = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_pass, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.checkP:
                attempRegister();
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void attempRegister() {
        if (registerTask != null) {
            return;
        }
        //Reset errors
        mUsername.setError(null);
        mPassword.setError(null);
        mEmail.setError(null);
        mConfirmation.setError(null);

        String username = mUsername.getText().toString();
        String pass = mPassword.getText().toString();
        String email = mEmail.getText().toString();
        String confirmation = mConfirmation.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(pass) && isPasswordValid(pass)) {
            mPassword.setError(getString(R.string.error_invalid_password));
            focusView = mPassword;
            cancel = true;
        }

        if (!TextUtils.isEmpty(confirmation) && !isConfirmValid(confirmation, pass)) {
            mConfirmation.setError("Introduziu passwords diferentes");
            focusView = mConfirmation;
            cancel = true;
        }

        // Check for a valid username.
        if (TextUtils.isEmpty(username)) {
            mUsername.setError(getString(R.string.error_field_required));
            focusView = mUsername;
            cancel = true;
        } else if (!isUserValid(username)) {
            mUsername.setError(getString(R.string.error_invalid_user));
            focusView = mUsername;
            cancel = true;
        }

        //check for a valid email
        if (!isEmailValid(email)) {
            mEmail.setError(getString(R.string.error_invalid_email));
            focusView = mEmail;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            registerTask = new Register.RegisterTask(username, pass, email, selectedRegion);
            registerTask.execute((Void) null);
        }
    }

    private boolean isUserValid(String username) {
        //TODO: Replace this with your own logic
        return username.toString().length() > 0 && username.toString().length() <= 20;
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.toString().length() >= 4 && password.toString().length() <= 20;
    }

    private boolean isConfirmValid(String password, String pass2) {
        //TODO: Replace this with your own logic
        return password.equals(pass2);
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
    }

    /**
     * Represents an asynchronous post register task used to add a new user
     * to the database
     */
    public class RegisterTask extends AsyncTask<Void, Void, String> {

        private final String mUser;
        private final String mPass;
        private final String mEmail;
        private final String mRegion;

        RegisterTask(String username, String password, String email, String region) {
            mUser = username;
            mPass = password;
            mEmail = email;
            mRegion = region;
        }


        //Cancel background network operation if we do not have network connectivity
        @Override
        protected void onPreExecute() {
            ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = conMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected()
                    || (networkInfo.getType() != ConnectivityManager.TYPE_WIFI &&
                    networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);
                Toast.makeText(Register.this, "Sem conexão à Internet", Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected String doInBackground(Void... params) {

            try {
                //constroi objecto jason com credenciais e efetua pedido
                JSONObject credentials = new JSONObject();
                credentials.accumulate("username", mUser);
                credentials.accumulate("password", mPass);
                credentials.accumulate("email", mEmail);
                credentials.accumulate("city", mRegion);
                return Requests.doPost(new URL(baseUrl), credentials);


            } catch (Exception e) {
                return e.toString();
            }

        }

        @Override
        protected void onPostExecute(final String result) {
            if (result != null) {
                Intent intent = new Intent(Register.this, MainActivity.class);
                startActivity(intent);
                Toast.makeText(Register.this, "Registo efetuado com Sucesso", Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected void onCancelled() {

        }

    }
}
