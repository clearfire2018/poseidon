package com.example.clearfire.poseidon.Report;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.example.clearfire.poseidon.R;
import com.example.clearfire.poseidon.Utils.Requests;
import com.example.clearfire.poseidon.User;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class AddReport extends AppCompatActivity {
    ImageView mImage;
    public static final int PICK_IMAGE = 1;
    static final int REQUEST_IMAGE_CAPTURE = 2;

    private static final String TAG = "AddReport";
    private String baseUrlImg = "https://henrique-45707.appspot.com/gcs/henrique-45707.appspot.com/";
    private String baseUrl = "https://henrique-45707.appspot.com/rest/reports/register";

    private static final String SHARED_FOLDER = "shared";

    public static final String PREFS_NAME = "AUTH";
    private double latitude;
    private double longitude;
    private int type;
    private String description;
    private SharedPreferences settings;
    private String user;
    private String tokenId;
    private int height;
    private int width;
    private int grau = 1;
    private String terreno = "Pub";
    private String value;

    private EditText mDescriptionText;
    private ImageView mGaleria;
    private ImageView mCamera;
    private TextView mTypeReport;
    private Spinner mGrau;
    private RadioButton mPub;
    private RadioButton mPriv;
    private SeekBar mArea;
    private TextView min;

    private AddReportTask mAddReportTask;
    private SendPhoto sendPhotoTask = null;
    private  Uri fileUri;
    Bitmap biti = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        settings = getSharedPreferences(PREFS_NAME, 0);
        user = settings.getString("username", "");
        tokenId = settings.getString("tokenID", "");

        Bundle bd = getIntent().getExtras();
        latitude = bd.getDouble("lat");
        longitude = bd.getDouble("lng");
        type = bd.getInt("type");

        mArea  =findViewById(R.id.seekBar);
        min = findViewById(R.id.min);

        value = String.valueOf(mArea.getProgress());
        min.setText(value);

        mArea.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                value = String.valueOf(progress);
                min.setText(value);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mPub = findViewById(R.id.radioPub);
        mPriv = findViewById(R.id.radioPriv);

        mPub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPub.setChecked(true);
                mPriv.setChecked(false);
                terreno = "Pub";
            }
        });

        mPriv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPub.setChecked(false);
                mPriv.setChecked(true);
                terreno = "Priv";
            }
        });

        mDescriptionText = (EditText) findViewById(R.id.descriptionText);
        mImage = (ImageView) findViewById(R.id.imageView);
        mGaleria = (ImageView) findViewById(R.id.galeria);
        mCamera = (ImageView) findViewById(R.id.camera);


        mTypeReport = (TextView) findViewById(R.id.typeOfReport);

        if(type == 0)
        mTypeReport.setText("Fogo");
        else if(type == 1){
            mTypeReport.setText("Prevenção");
        } else {
            mTypeReport.setText("Lixo");
        }
        mGrau = (Spinner) findViewById(R.id.grauReport);


        mGrau.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                if(position == 0){
                    Toast.makeText(getApplicationContext(), "Selecione um grau" , Toast.LENGTH_SHORT).show();
                } else {
                    grau = Integer.parseInt(parent.getItemAtPosition(position).toString());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });

        mGaleria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Selected Image"), PICK_IMAGE);
            }
        });

        mCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onLaunchCamera(view);

            }
        });

    }
    public Uri getOutputMediaFileUri() {
        return Uri.fromFile(getOutputMediaFile());
    }

    private static File getOutputMediaFile() {

        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "DeKa");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.e("Monitoring", "Oops! Failed create Monitoring directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_DeKa_" + timeStamp + ".jpg");

        return mediaFile;
    }
    public void onLaunchCamera(View view) {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri = getOutputMediaFileUri();
        intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
    }

    // Returns the File for a photo stored on disk given the fileName
    public File getPhotoFileUri(String fileName) {
        // Get safe storage directory for photos
        // Use `getExternalFilesDir` on Context to access package-specific directories.
        // This way, we don't need to request external read/write runtime permissions.
        File mediaStorageDir = new File(getExternalFilesDir(null), SHARED_FOLDER);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()){
            Log.d(SHARED_FOLDER, "failed to create directory");
            return null;
        }

        // Return the file target for the photo based on filename
        File file = new File(mediaStorageDir.getPath() + File.separator + fileName);

        return file;
    }

    public Bitmap rotateBitmapOrientation(String photoFilePath) {
        // Create and configure BitmapFactory
        Bitmap bm =  BitmapFactory.decodeFile(photoFilePath);

        // Read EXIF Data
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(photoFilePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
        int orientation = orientString != null ? Integer.parseInt(orientString) : ExifInterface.ORIENTATION_NORMAL;
        int rotationAngle = 0;
        if (orientation == ExifInterface.ORIENTATION_NORMAL) {
            bm= Bitmap.createScaledBitmap(bm, width,height,false);
        }
        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
            rotationAngle = 90;
            bm= Bitmap.createScaledBitmap(bm, height,width,false);
        }
        if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {rotationAngle = 180;bm= Bitmap.createScaledBitmap(bm, width,height,false);}
        if (orientation == ExifInterface.ORIENTATION_ROTATE_270){ rotationAngle = 270; bm= Bitmap.createScaledBitmap(bm, height,width,false);}

        // Rotate Bitmap
        Matrix matrix = new Matrix();
        matrix.postRotate(rotationAngle);


       return Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("onActivityResult", "requestCode " + requestCode + ", resultCode " + resultCode);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_IMAGE_CAPTURE) {
                try {
                    Log.e("CAMERA", fileUri.getPath());
                    biti = rotateBitmapOrientation(fileUri.getPath());
                    mImage.setImageBitmap(biti);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == PICK_IMAGE && resultCode == RESULT_OK && data.getData() != null && data.getExtras() != null) {
                fileUri = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), fileUri);
                    biti = Bitmap.createScaledBitmap(bitmap, width, height, false);
                    bitmap = null;
                    mImage.setImageBitmap(biti);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_pass, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch ( item.getItemId() ) {

            case R.id.checkP:
                attemptAddReport();
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void attemptAddReport() {

        if (mAddReportTask != null) {
            return;
        }

        mDescriptionText.setError(null);
        description = mDescriptionText.getText().toString();


        boolean cancel = false;
        View focusView = null;

        if(TextUtils.isEmpty(description)) {
                mDescriptionText.setError("Introduza uma descrição");
                focusView = mDescriptionText;
                cancel = true;
        }

        if (description.length() > 200) {
            mDescriptionText.setError("Introduza uma descrição mais curta");
            focusView = mDescriptionText;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        }else {
            mAddReportTask = new AddReportTask(user, tokenId, description, type, latitude, longitude, terreno, value, grau);
            mAddReportTask.execute((Void) null);
            Intent intent = new Intent(this, User.class);
            startActivity(intent);
            finish();
        }
    }

    /**
     * Represents an asynchronous send photo task used to send the report photo
     * to the database
     */
    public class SendPhoto extends AsyncTask<Void, Void, String> {

        Bitmap mImage;
        String id;

        SendPhoto(Bitmap image, String id) {
            mImage = image;
            this.id = id;
        }


        //Cancel background network operation if we do not have network connectivity
        @Override
        protected void onPreExecute() {
            ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = conMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected()
                    || (networkInfo.getType() != ConnectivityManager.TYPE_WIFI &&
                    networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);
            }
        }

        @Override
        protected String doInBackground(Void... params) {

            try {
                //constroi objecto jason com credenciais e efetua pedido


                return Requests.doPostImage(new URL(baseUrlImg+ id), mImage);


            } catch (Exception e) {
                return e.toString();
            }

        }

        @Override
        protected void onPostExecute(final String result) {
            if (result != null) {
                Log.v(TAG, "Sucesso");
            }
        }

        @Override
        protected void onCancelled() {

        }

    }

    /**
     * Represents an asynchronous post report task used to add a new report
     * to the database
     */
    public class AddReportTask extends AsyncTask<Void, Void, String> {

        String description;
        String username;
        String tokenID;
        int type;
        double latitude;
        double longitude;
        String area;
        String radius;
        int grau;

        AddReportTask(String username, String tokenID, String description, int type, double latitude, double longitude,
                      String area, String radio, int grau) {
            this.username = username;
            this.tokenID = tokenID;
            this.description = description;
            this.type = type;
            this.latitude = latitude;
            this.longitude = longitude;
            this.area = area;
            this.radius = radio;
            this.grau = grau;
        }


        //Cancel background network operation if we do not have network connectivity
        @Override
        protected void onPreExecute() {
            ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = conMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected()
                    || (networkInfo.getType() != ConnectivityManager.TYPE_WIFI &&
                    networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);
            }
        }

        @Override
        protected String doInBackground(Void... params) {

            try {
                //constroi objecto jason com credenciais e efetua pedido
                JSONObject credentialsAddReport = new JSONObject();
                JSONObject credentialsAcess = new JSONObject();
                JSONObject credentialsMarker = new JSONObject();

                Log.e("AddReport", "user:" + username);
                Log.e("AddReport", "user:" + tokenID);
                Log.e("AddReport", "user:" + latitude);
                Log.e("AddReport", "user:" + longitude);
                Log.e("AddReport", "user:" + type);
                Log.e("AddReport", "user:" + description);

                credentialsAcess.accumulate("username", this.username);
                credentialsAcess.accumulate("tokenID", this.tokenID);
                credentialsAddReport.accumulate("acess", credentialsAcess);


                credentialsMarker.accumulate("latitude", this.latitude);
                credentialsMarker.accumulate("longitude", this.longitude);
                credentialsAddReport.accumulate("marker", credentialsMarker);

                credentialsAddReport.accumulate("category", this.type);
                credentialsAddReport.accumulate("description", this.description);
                credentialsAddReport.accumulate("radius", this.radius);
                credentialsAddReport.accumulate("publicPrivate", this.area);
                credentialsAddReport.accumulate("level", this.grau);

                return Requests.doPost(new URL(baseUrl), credentialsAddReport);


            } catch (Exception e) {
                return null;
            }

        }

        @Override
        protected void onPostExecute(final String result) {
            mAddReportTask = null;
            if (result != null) {


                Log.v(TAG, result);

                sendPhotoTask = new SendPhoto(biti, result);
                sendPhotoTask.execute((Void) null);

            }

        }

        @Override
        protected void onCancelled() {

        }

    }
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        // Save UI state changes to the savedInstanceState.
        // This bundle will be passed to onCreate if the process is
        // killed and restarted.

        savedInstanceState.putParcelable("path", fileUri);
        savedInstanceState.putInt("imgHeigth", height);
        savedInstanceState.putInt("imgWidth", width);
        // etc.
        Log.e("SAVE", fileUri.toString());
        Log.e("SAVE", height +":"+ width);
        super.onSaveInstanceState(savedInstanceState);
    }

//onRestoreInstanceState

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {

        super.onRestoreInstanceState(savedInstanceState);

        // Restore UI state from the savedInstanceState.
        // This bundle has also been passed to onCreate.

        fileUri = (Uri)savedInstanceState.getParcelable("path");
        height = savedInstanceState.getInt("imgHeigth");
        width = savedInstanceState.getInt("imgWidth");
        Log.e("Rest", fileUri.toString());
        Log.e("Rest", height +":"+ width);
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            Log.e("Resume", height +":"+ width);
            biti = rotateBitmapOrientation(fileUri.getPath());
            mImage.setImageBitmap(biti);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus){
            height = mImage.getHeight();
            width = mImage.getWidth();
            Log.e("ONCREATE", height +":"+ width);

        }
    }
}
