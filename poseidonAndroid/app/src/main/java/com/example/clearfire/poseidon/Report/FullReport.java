package com.example.clearfire.poseidon.Report;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.clearfire.poseidon.Event.ImageEventAdapter;
import com.example.clearfire.poseidon.Marker.InfoMarker;
import com.example.clearfire.poseidon.R;
import com.example.clearfire.poseidon.Utils.Requests;

import org.json.JSONObject;

import java.net.URL;

/**
 * Created by henry on 08-06-2018.
 */

public class FullReport extends AppCompatActivity {

    private ImageView mPhot;
    private TextView mTitle;
    private ImageView mState;
    private ImageView mState2;
    private ImageView mState3;
    private TextView mDescrip;
    private TextView mNumb;
    private TextView mGrau;
    private FrameLayout frame;
    private FloatingActionButton check;

    private String user;
    private String tokenId;
    private String id;


    public static final String PREFS_NAME = "AUTH";
    private String baseUrlCheckReport = "https://henrique-45707.appspot.com/rest/reports/confirm/";
    private String baseUrlGetCheck = "https://henrique-45707.appspot.com/rest/reports/confirm/";
    private String baseUrlImg = "https://henrique-45707.appspot.com/gcs/henrique-45707.appspot.com/";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fullreport);

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        user = settings.getString("username", "");
        tokenId = settings.getString("tokenID", "");

        Bundle bd = getIntent().getExtras();

         id = bd.getString("id");
         int grauu = bd.getInt("grau");

         mNumb = findViewById(R.id.numcheck);

         mGrau = findViewById(R.id.gravidades);
         mGrau.setText(grauu + "");

        GetNumberChecks getNumberChecks = new GetNumberChecks(id);
        getNumberChecks.execute((Void) null);

        check = findViewById(R.id.check);
        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptCheckReport(id);
            }
        });

        mPhot = (ImageView) findViewById(R.id.imgreport);
        mTitle = (TextView) findViewById(R.id.title);
        mDescrip = (TextView) findViewById(R.id.descReport);

        frame = findViewById(R.id.frame);
        mState = frame.findViewById(R.id.nadabom);
        mState2 = frame.findViewById(R.id.bom);
        mState3 = frame.findViewById(R.id.ameno);

        int state = Integer.parseInt(bd.getString("state"));

        mState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Não Resolvido",Toast.LENGTH_SHORT).show();
            }
        });

        if(state == 1){
            mState.setVisibility(View.INVISIBLE);
            mState3.setVisibility(View.VISIBLE);
            mState3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getApplicationContext(),"Em Resolução",Toast.LENGTH_SHORT).show();
                }
            });
        }

        if(state == 2){
            mState.setVisibility(View.INVISIBLE);
            mState2.setVisibility(View.VISIBLE);
            mState2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getApplicationContext(),"Resolvido",Toast.LENGTH_SHORT).show();
                }
            });
        }

        Drawable d = getResources().getDrawable(R.drawable.noimage);

        String title = bd.getString("title");
        String description = bd.getString("desc");
        byte[] imgByte = bd.getByteArray("image");

        if(imgByte == null) {
           GetPhoto getPhotoTask = new GetPhoto(mPhot, id);
           getPhotoTask.execute((Void) null);

        }
        mPhot.setImageDrawable(d);
        mDescrip.setText(description);
        mTitle.setText(title);

    }

    private void attemptCheckReport(String idReport) {

        CheckReportRequest checkReportRequest = new CheckReportRequest(user, tokenId, idReport);
        checkReportRequest.execute((Void) null);
    }

    /**
     * Represents an asynchronous check report task used to confirm that
     * the report exist
     */
    public class CheckReportRequest extends AsyncTask<Void, Void, String> {
        String idReport;

        CheckReportRequest(String username, String token, String idReport) {
            user = username;
            tokenId = token;
            this.idReport = idReport;
        }


        //Cancel background network operation if we do not have network connectivity
        @Override
        protected void onPreExecute() {
            ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = conMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected()
                    || (networkInfo.getType() != ConnectivityManager.TYPE_WIFI &&
                    networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);
            }
        }

        @Override
        protected String doInBackground(Void... params) {

            try {
                //constroi objecto jason com credenciais e efetua pedido
                JSONObject credentialsLogout = new JSONObject();
                credentialsLogout.accumulate("username", user);
                credentialsLogout.accumulate("tokenID", tokenId);


                return Requests.doPost(new URL(baseUrlCheckReport+ idReport), credentialsLogout);


            } catch (Exception e) {
                return null;
            }

        }

        @Override
        protected void onPostExecute(final String result) {
            if (result != null) {
                GetNumberChecks getNumberChecks = new GetNumberChecks(id);
                getNumberChecks.execute((Void) null);
                Log.v("CheckReport", "Done");
                Toast.makeText(getApplicationContext(),"Confirmação efetuada com sucesso",Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onCancelled() {

        }

    }

    /**
     * Represents an asynchronous get number of checks task
     */
    public class GetNumberChecks extends AsyncTask<Void, Void, String> {

        String idReport;

        GetNumberChecks(String idReport) {
            this.idReport = idReport;
        }


        //Cancel background network operation if we do not have network connectivity
        @Override
        protected void onPreExecute() {
            ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = conMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected()
                    || (networkInfo.getType() != ConnectivityManager.TYPE_WIFI &&
                    networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);
            }
        }

        @Override
        protected String doInBackground(Void... params) {

            try {
                return Requests.doGet(new URL(baseUrlGetCheck+ idReport + "/num"));
            } catch (Exception e) {
                return null;
            }

        }

        @Override
        protected void onPostExecute(final String result) {
            if (result != null) {
                mNumb.setText(result);
            }
        }

        @Override
        protected void onCancelled() {

        }

    }

    /**
     * Represents an asynchronous get photo task used to get the photo of
     * a specific event to show to the user
     */
    public class GetPhoto extends AsyncTask<Void, Void, Bitmap> {

        private String id;
        private ImageView imgView;

        GetPhoto(ImageView imgView, String id) {
            this.id = id;
            this.imgView = imgView;
        }


        //Cancel background network operation if we do not have network connectivity
        @Override
        protected void onPreExecute() {
            ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = conMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected()
                    || (networkInfo.getType() != ConnectivityManager.TYPE_WIFI &&
                    networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);
            }
        }

        @Override
        protected Bitmap doInBackground(Void... params) {

            try {
                Log.v("mweovhevo", "  " + id);
                return Requests.doGetImage(new URL(baseUrlImg + id));

            } catch (Exception e) {
                return null;
            }

        }

        @Override
        protected void onPostExecute(final Bitmap result) {
            if (result != null) {
                Bitmap biti = Bitmap.createScaledBitmap(result, 300, 160, false);
                    mPhot.setImageBitmap(biti);

            }
        }

        @Override
        protected void onCancelled() {

        }
    }
}
