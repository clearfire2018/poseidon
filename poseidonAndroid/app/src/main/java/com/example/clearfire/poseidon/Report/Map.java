package com.example.clearfire.poseidon.Report;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.example.clearfire.poseidon.User;
import com.example.clearfire.poseidon.Utils.PermissionUtils;
import com.example.clearfire.poseidon.R;
import com.example.clearfire.poseidon.Utils.Utils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by henry on 11-04-2018.
 */

public class Map extends AppCompatActivity implements
        OnMyLocationButtonClickListener,
        OnMyLocationClickListener,
        OnMapReadyCallback,
        ActivityCompat.OnRequestPermissionsResultCallback {

    /**
     * Request code for location permission request.
     *
     * @see #onRequestPermissionsResult(int, String[], int[])
     */
    public static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    public static final float MARKER_COLOR_FIRE = BitmapDescriptorFactory.HUE_RED;
    public static final float MARKER_COLOR_PREVENTION = BitmapDescriptorFactory.HUE_MAGENTA;
    public static final float MARKER_COLOR_TRASH = BitmapDescriptorFactory.HUE_GREEN;


    /**
     * Flag indicating whether a requested permission has been denied after returning in
     * {@link #onRequestPermissionsResult(int, String[], int[])}.
     */
    private boolean mPermissionDenied = false;
    private GoogleMap mMap;
    private SupportMapFragment mapFragment;
    private Toolbar mToolbar;
    private boolean placeMarker;
    private Marker report;
    private FloatingActionButton mCheckButton;
    private ConstraintLayout cTrash;
    private ConstraintLayout cPrevention;
    private ConstraintLayout cFire;
    private float markerColor;
    private Animation mHideLayout;
    private Animation mShowLayout;
    public static final int TYPE_FIRE = 0;
    public static final int TYPE_PREVENTION = 1;
    public static final int TYPE_TRASH = 2;
    private Location lo;

    public int typeMarker;

    private void actionOnButtons(boolean toVisible) {
        if (toVisible) {
            cFire.setVisibility(View.VISIBLE);
            cPrevention.setVisibility(View.VISIBLE);
            cTrash.setVisibility(View.VISIBLE);

            cFire.startAnimation(mShowLayout);
            cPrevention.startAnimation(mShowLayout);
            cTrash.startAnimation(mShowLayout);

        } else {
            cFire.startAnimation(mHideLayout);
            cTrash.startAnimation(mHideLayout);
            cPrevention.startAnimation(mHideLayout);

            cFire.setVisibility(View.GONE);
            cPrevention.setVisibility(View.GONE);
            cTrash.setVisibility(View.GONE);

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        Toolbar toolbar = (Toolbar) findViewById(R.id.AddReport);
        setSupportActionBar(toolbar);

        ImageView cancelEvento = (ImageView) findViewById(R.id.cancelReport);
        cancelEvento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Map.this, User.class);
                startActivity(intent);
                finish();
            }
        });


        placeMarker = false;
        mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mapFragment.getView().setClickable(false);

        FloatingActionButton mAddTrash = (FloatingActionButton) findViewById(R.id.addTrash);
        FloatingActionButton mAddPrevention = (FloatingActionButton) findViewById(R.id.addPrevention);
        FloatingActionButton mAddFire = (FloatingActionButton) findViewById(R.id.addFire);
        mCheckButton = (FloatingActionButton) findViewById(R.id.checkButton);

        final FloatingActionButton mAddButton = (FloatingActionButton) findViewById(R.id.addButton);
        final Animation mShowButton = AnimationUtils.loadAnimation(this, R.anim.show_button);
        final Animation mHideButton = AnimationUtils.loadAnimation(this, R.anim.hide_button);
        mHideLayout = AnimationUtils.loadAnimation(this, R.anim.hide_layout);
        mShowLayout = AnimationUtils.loadAnimation(this, R.anim.show_layout);
        cTrash = (ConstraintLayout) findViewById(R.id.layoutTrash);
        cPrevention = (ConstraintLayout) findViewById(R.id.layoutPrevention);
        cFire = (ConstraintLayout) findViewById(R.id.layoutFire);


        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cFire.getVisibility() == View.VISIBLE && cTrash.getVisibility() == View.VISIBLE && cPrevention.getVisibility() == View.VISIBLE) {
                    mAddButton.startAnimation(mHideButton);

                    actionOnButtons(false);
                    placeMarker = false;
                    if (report != null)
                        report.remove();
                    report = null;
                    mCheckButton.setVisibility(View.GONE);

                } else {
                    mAddButton.startAnimation(mShowButton);
                    actionOnButtons(true);

                }
            }

        });


        mAddTrash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                placeMarker = true;
                markerColor = MARKER_COLOR_TRASH;
                typeMarker = TYPE_TRASH;
                try {
                    enableMyLocation();
                    lo = mMap.getMyLocation();
                    if (lo != null) {
                        if (report == null) {
                            makeMarker(new LatLng(lo.getLatitude(), lo.getLongitude()));
                        }
                    } else
                        Toast.makeText(Map.this, "Turn GPS to a better experience", Toast.LENGTH_LONG).show();

                } catch (Exception e) {
                    Toast.makeText(Map.this, "Turn GPS to a better experience", Toast.LENGTH_LONG).show();
                } finally {
                    if (report != null) {
                        report.setIcon(BitmapDescriptorFactory.defaultMarker(markerColor));
                    }
                }

            }
        });
        mCheckButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent reportIntent = new Intent(Map.this, AddReport.class);
                reportIntent.putExtra("type", typeMarker);
                reportIntent.putExtra("lat", report.getPosition().latitude);
                reportIntent.putExtra("lng", report.getPosition().longitude);
                startActivity(reportIntent);

            }
        });
        mAddPrevention.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                placeMarker = true;
                markerColor = MARKER_COLOR_PREVENTION;
                typeMarker = TYPE_PREVENTION;
                try {
                    enableMyLocation();
                    lo = mMap.getMyLocation();
                    if (lo != null) {
                        if (report == null) {
                            makeMarker(new LatLng(lo.getLatitude(), lo.getLongitude()));
                        }
                    } else
                        Toast.makeText(Map.this, "Turn GPS to a better experience", Toast.LENGTH_LONG).show();

                } catch (Exception e) {
                    Toast.makeText(Map.this, "Turn GPS to a better experience", Toast.LENGTH_LONG).show();
                } finally {
                    if (report != null) {
                        report.setIcon(BitmapDescriptorFactory.defaultMarker(markerColor));
                    }
                }
            }
        });

        mAddFire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                placeMarker = true;
                markerColor = MARKER_COLOR_FIRE;
                typeMarker = TYPE_FIRE;
                try {
                    enableMyLocation();
                    lo = mMap.getMyLocation();
                    if (lo != null) {
                        if (report == null) {
                            makeMarker(new LatLng(lo.getLatitude(), lo.getLongitude()));
                        }
                    } else
                        Toast.makeText(Map.this, "Turn GPS to a better experience", Toast.LENGTH_LONG).show();

                } catch (Exception e) {
                    Toast.makeText(Map.this, "Turn GPS to a better experience", Toast.LENGTH_LONG).show();
                } finally {
                    if (report != null) {
                        report.setIcon(BitmapDescriptorFactory.defaultMarker(markerColor));
                    }
                }
            }
        });


    }



    private void makeMarker(LatLng latLng) {
        report = mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .draggable(true)
                .icon(BitmapDescriptorFactory.defaultMarker(markerColor)));
        mCheckButton.setVisibility(View.VISIBLE);

    }


    public static LatLngBounds PORTUGAL = new LatLngBounds(
            new LatLng(36.746936, -10.684079), new LatLng(42.259166, -5.981752));


    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
        mMap.setMinZoomPreference(6.7f);
        mMap.setLatLngBoundsForCameraTarget(PORTUGAL);

        //mMap.moveCamera();
        List<LatLng> hole = new LinkedList<>();
        hole.addAll(Utils.getPortugalPoly().getPoints());
        Polygon polygon = map.addPolygon(new PolygonOptions()
                .add(new LatLng(50, -50),
                        new LatLng(50, 45),
                        new LatLng(-50, 50),
                        new LatLng(-50, -50),
                        new LatLng(50, -50)

                ).strokeColor(0x007F7F7F)
                .fillColor(0x00DCDCDC)
                .addHole(hole)
                .clickable(true));

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                if (report != null)
                    report.remove();
                if (placeMarker)
                    makeMarker(latLng);

            }
        });
        mMap.setOnMyLocationButtonClickListener(this);
        enableMyLocation();
        Location lo = mMap.getMyLocation();
        try {

            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(lo.getLatitude(), lo.getLongitude()), 10.f));
        } catch (Exception e) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(PORTUGAL.getCenter(), 6.7f));
            onMyLocationButtonClick();
        }


    }

    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        try {
            Location lo = mMap.getMyLocation();
            if (lo != null) {
                if (report != null) {
                    report.setPosition(new LatLng(lo.getLatitude(), lo.getLongitude()));
                }
            }
        }catch (Exception e) {
            Toast.makeText(Map.this, "Turn GPS to a better experience", Toast.LENGTH_LONG).show();
            //Toast.makeText(this, "Turn on the location", Toast.LENGTH_SHORT).show();
            // Return false so that we don't consume the event and the default behavior still occurs
            // (the camera animates to the user's current position).

        }
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
        Toast.makeText(this, "Current location:\n" + location, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            enableMyLocation();
        } else {
            // Display the missing permission error dialog when the fragments resume.
            mPermissionDenied = true;
        }
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.
            showMissingPermissionError();
            mPermissionDenied = false;
        }
    }

    /**
     * Displays a dialog with error message explaining that the location permission is missing.
     */
    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getSupportFragmentManager(), "dialog");
    }

}
