package com.example.clearfire.poseidon.Settings;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.clearfire.poseidon.R;
import com.example.clearfire.poseidon.Utils.Requests;

import org.json.JSONObject;

import java.net.URL;

/**
 * Created by henry on 13-05-2018.
 */

public class ChangePass extends AppCompatActivity {

    private String baseUrl = "https://henrique-45707.appspot.com/rest/user/change-password";
    private static final String TAG = "Settings";
    public static final String PREFS_NAME = "AUTH";

    private ChangePassTask changePassTask = null;

    private EditText mCurrentPass;
    private EditText mNewPass;
    private EditText mConfirmation;
    private ImageView mImageCancel;
    private Button mSubmitChange;
    private String mUser;
    private Toolbar mToolbar;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.changepass);

        mCurrentPass = (EditText) findViewById(R.id.currentPass);
        mNewPass = (EditText) findViewById(R.id.newPass);
        mConfirmation = (EditText) findViewById(R.id.confirmNewPass);

        mToolbar = (Toolbar) findViewById(R.id.MenuPass);
        setSupportActionBar(mToolbar);


        mImageCancel = (ImageView) findViewById(R.id.cancel);
        mImageCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ChangePass.this, Settings.class);
                startActivity(intent);
                finish();
            }
        });

        /*mSubmitChange = findViewById(R.id.submitPass);
        mSubmitChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attempChangePass();
            }
        });*/

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        mUser = settings.getString("username", "");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_pass, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.checkP:
                attempChangePass();
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void attempChangePass() {
        if (changePassTask != null) {
            return;
        }
        //Reset errors
        mCurrentPass.setError(null);
        mNewPass.setError(null);
        mConfirmation.setError(null);


        String current = mCurrentPass.getText().toString();
        String newPass = mNewPass.getText().toString();
        String confirm = mConfirmation.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(current) || TextUtils.isEmpty(newPass) || TextUtils.isEmpty(confirm)) {
            Toast.makeText(getApplicationContext(), "Preencha todos os campos", Toast.LENGTH_SHORT).show();
            cancel = true;
        }

        if (!newPass.equals(confirm)) {
            mConfirmation.setError("Password diferente");
            focusView = mConfirmation;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            if (focusView != null)
                focusView.requestFocus();

        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            changePassTask = new ChangePassTask(mUser, current, newPass);
            changePassTask.execute((Void) null);
        }
    }

    /**
     * Represents an asynchronous change passwor task used to change the currentPass
     * of the user to a new one.
     */
    public class ChangePassTask extends AsyncTask<Void, Void, String> {

        private final String userC;
        private final String currentPass;
        private final String newPass;


        ChangePassTask(String user, String currentP, String newP) {
            userC = user;
            currentPass = currentP;
            newPass = newP;

        }


        //Cancel background network operation if we do not have network connectivity
        @Override
        protected void onPreExecute() {
            ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = conMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected()
                    || (networkInfo.getType() != ConnectivityManager.TYPE_WIFI &&
                    networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);
            }
        }

        @Override
        protected String doInBackground(Void... params) {

            try {
                //constroi objecto jason com credenciais e efetua pedido
                JSONObject credentials = new JSONObject();
                credentials.accumulate("username", userC);
                credentials.accumulate("oldPassword", currentPass);
                credentials.accumulate("newPassword", newPass);


                return Requests.doPost(new URL(baseUrl), credentials);


            } catch (Exception e) {
                return e.toString();
            }

        }

        @Override
        protected void onPostExecute(final String result) {
            if (result != null) {
                Intent intent = new Intent(ChangePass.this, Settings.class);
                startActivity(intent);
                finish();
            }
        }

        @Override
        protected void onCancelled() {

        }

    }
}
