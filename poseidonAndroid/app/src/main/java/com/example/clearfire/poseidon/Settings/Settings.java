package com.example.clearfire.poseidon.Settings;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.TextView;

import com.example.clearfire.poseidon.MainActivity;
import com.example.clearfire.poseidon.R;
import com.example.clearfire.poseidon.Utils.Requests;

import org.json.JSONObject;

import java.net.URL;


/**
 * Created by henry on 11-05-2018.
 */

public class Settings extends AppCompatActivity {


    private static final String TAG = "Settings";
    public static final String PREFS_NAME = "AUTH";
    private String logoutUrl = "https://henrique-45707.appspot.com/rest/login/logout";

    private AttemptLogout mLogoutTask;

    private TextView mPass;
    private CardView mlogout;

    private SharedPreferences settings;
    private String user;
    private String tokenId;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_conta);

        settings = getSharedPreferences(PREFS_NAME, 0);
        user = settings.getString("username", "");
        tokenId = settings.getString("tokenID", "");

        mPass = findViewById(R.id.alterarPass);
        mPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Settings.this, ChangePass.class);
                startActivity(intent);
            }
        });

        mlogout = findViewById(R.id.logout);
        mlogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptLogout();
            }
        });


    }

    private void attemptLogout() {

        mLogoutTask = new AttemptLogout(user, tokenId);
        mLogoutTask.execute((Void) null);
    }

    /**
     * Represents an asynchronous logout task used to logout
     * the user.
     */
    public class AttemptLogout extends AsyncTask<Void, Void, String> {

        AttemptLogout(String username, String token) {
            user = username;
            tokenId = token;
        }


        //Cancel background network operation if we do not have network connectivity
        @Override
        protected void onPreExecute() {
            ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = conMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected()
                    || (networkInfo.getType() != ConnectivityManager.TYPE_WIFI &&
                    networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);
            }
        }

        @Override
        protected String doInBackground(Void... params) {

            try {
                //constroi objecto jason com credenciais e efetua pedido
                JSONObject credentialsLogout = new JSONObject();
                credentialsLogout.accumulate("username", user);
                credentialsLogout.accumulate("tokenID", tokenId);

                return Requests.doDelete(new URL(logoutUrl), credentialsLogout);


            } catch (Exception e) {
                return null;
            }

        }

        @Override
        protected void onPostExecute(final String result) {
            mLogoutTask = null;
            if (result != null) {
                SharedPreferences.Editor editor = settings.edit();
                editor.remove("username");
                editor.remove("tokenID");
                editor.commit();
                Intent intent = new Intent(Settings.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }

        @Override
        protected void onCancelled() {

        }

    }

}
