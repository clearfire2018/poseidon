package com.example.clearfire.poseidon;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat.OnRequestPermissionsResultCallback;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.example.clearfire.poseidon.Event.AddEvent;
import com.example.clearfire.poseidon.Event.ListEvents;
import com.example.clearfire.poseidon.Marker.InfoMarker;
import com.example.clearfire.poseidon.Report.FullReport;
import com.example.clearfire.poseidon.Report.Map;
import com.example.clearfire.poseidon.Settings.Settings;
import com.example.clearfire.poseidon.Utils.Manual;
import com.example.clearfire.poseidon.Utils.MyItem;
import com.example.clearfire.poseidon.Utils.PermissionUtils;
import com.example.clearfire.poseidon.Utils.Requests;
import com.example.clearfire.poseidon.Utils.Utils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.maps.android.clustering.ClusterManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static com.example.clearfire.poseidon.Report.Map.LOCATION_PERMISSION_REQUEST_CODE;
import static com.example.clearfire.poseidon.Report.Map.PORTUGAL;

/**
 * Created by henry on 28-03-2018.
 */

public class User extends AppCompatActivity implements
        OnMapReadyCallback,
        GoogleMap.OnMyLocationButtonClickListener,
        OnRequestPermissionsResultCallback

{
    private AttemptListReports mListReportsTask;
    private GoogleMap mMap;
    private InfoMarker info;

    private String user;
    private String tokenId;
    private String desc;
    private String type;
    private String id;
    private int grau;

    private Calendar dates;
    private Context context;
    private FrameLayout frame;


    private static final String TAG = "User";
    public static final String PREFS_NAME = "AUTH";

    private ClusterManager<MyItem> mClusterManager;
    private String listReportsUrl = "https://henrique-45707.appspot.com/rest/reports/listAll";


    private Bitmap bitEvent = null;
    private SupportMapFragment mapFragment;
    private boolean mPermissionDenied;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.toolbar_coordinate);
        context = this;
        mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mapFragment.getView().setClickable(false);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        ImageView settingsUser = findViewById(R.id.settings);
        settingsUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(User.this, Settings.class);
                startActivity(intent);
            }
        });

        ImageView manual = findViewById(R.id.manual);
        manual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(User.this, Manual.class);
                startActivity(intent);
            }
        });

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        user = settings.getString("username", "");
        tokenId = settings.getString("tokenID", "");


        final BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.navigationView);

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.reports:
                                if (!(User.this instanceof User)) {
                                    Intent intent = new Intent(User.this, User.class);
                                    startActivity(intent);
                                    finish();
                                }
                                break;
                            case R.id.eventos:

                                Intent intent2 = new Intent(User.this, ListEvents.class);
                                startActivity(intent2);
                                finish();
                                break;
                        }
                        return true;
                    }
                });

        ImageView but = (ImageView) findViewById(R.id.eventOrReport);
        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bottomNavigationView.getSelectedItemId() == R.id.eventos) {
                    Intent intent = new Intent(User.this, AddEvent.class);
                    startActivity(intent);

                } else {
                    Intent intent = new Intent(User.this, Map.class);
                    startActivity(intent);
                }
            }
        });
    }


    private void attemptListReports() {

        mListReportsTask = new AttemptListReports(user, tokenId, "");
        mListReportsTask.execute((Void) null);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        attemptListReports();
        mMap.setMinZoomPreference(6.7f);
        mMap.setLatLngBoundsForCameraTarget(PORTUGAL);
        List<LatLng> hole = new LinkedList<>();
        hole.addAll(Utils.getPortugalPoly().getPoints());
        Polygon polygon = mMap.addPolygon(new PolygonOptions()
                .add(new LatLng(50, -50),
                        new LatLng(50, 45),
                        new LatLng(-50, 50),
                        new LatLng(-50, -50),
                        new LatLng(50, -50)

                ).strokeColor(0x007F7F7F)
                .fillColor(0x00DCDCDC)
                .addHole(hole)
                .clickable(true));
        mClusterManager = new ClusterManager<>(this, mMap);
        mMap.setOnMyLocationButtonClickListener(this);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(PORTUGAL.getCenter(), 6.7f));

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                // custom dialog
                InfoMarker infoMarker = ((InfoMarker) marker.getTag());
                Intent intent = new Intent(User.this, FullReport.class);
                intent.putExtra("title", infoMarker.getType());
                intent.putExtra("desc", infoMarker.getDescription());
                intent.putExtra("state", infoMarker.getState());
                intent.putExtra("id", infoMarker.getId());
                intent.putExtra("grau", infoMarker.getGrau());
                ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                if (infoMarker.getImage() != null) {
                    infoMarker.getImage().compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                    intent.putExtra("image", outStream.toByteArray());
                }

                startActivity(intent);
                return false;
            }
        });

        enableMyLocation();
    }


    private void makeMarker(InfoMarker info) {

        Marker m = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(info.getLat(), info.getLng()))
                .draggable(true)
                .icon(BitmapDescriptorFactory.defaultMarker(info.getColor()))
        );
        m.setTag(info);

    }

    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            enableMyLocation();
        } else {
            // Display the missing permission error dialog when the fragments resume.
            mPermissionDenied = true;
        }
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.
            showMissingPermissionError();
            mPermissionDenied = false;
        }
    }

    /**
     * Displays a dialog with error message explaining that the location permission is missing.
     */
    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getSupportFragmentManager(), "dialog");
    }

    /**
     * Represents an asynchronous list reports task used to show all
     * the reports to the user limited by a cursor to get x reports by time
     */
    public class AttemptListReports extends AsyncTask<Void, Void, String> {
        String username;
        String token;
        String cursor;
        private static final String PROPERTYMAP = "propertyMap";

        AttemptListReports(String username, String token, String cursor) {
            this.username = username;
            this.token = token;
            this.cursor = cursor;
        }


        //Cancel background network operation if we do not have network connectivity
        @Override
        protected void onPreExecute() {
            ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = conMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected()
                    || (networkInfo.getType() != ConnectivityManager.TYPE_WIFI &&
                    networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);
            }
        }

        @Override
        protected String doInBackground(Void... params) {

            try {
                //constroi objecto jason com credenciais e efetua pedido
                JSONObject credentialsLogout = new JSONObject();
                credentialsLogout.accumulate("username", user);
                credentialsLogout.accumulate("tokenID", tokenId);

                return Requests.doPost(new URL(listReportsUrl + "?cursor=" + cursor), credentialsLogout);


            } catch (Exception e) {
                return null;
            }

        }

        @Override
        protected void onPostExecute(final String result) {
            mListReportsTask = null;
            if (result != null) {
                JSONArray data = null;
                double lat;
                double lng;

                JSONObject marker;
                try {
                    JSONObject reports = new JSONObject(result);


                    data = reports.getJSONArray("reports");
                    for (int i = 0; i < data.length(); i++) {
                        marker = data.getJSONObject(i);

                        id = marker.getJSONObject("key").getString("name");
                        marker = marker.getJSONObject(PROPERTYMAP);

                        lat = marker.getDouble("report_latitude");
                        lng = marker.getDouble("report_longitude");


                        desc = marker.getString("report_description");

                        String date = marker.getString("report_date");

                        Long dateLong = Long.parseLong(date);
                        Date dateD = new Date(dateLong);

                        dates = Calendar.getInstance();
                        dates.setTime(dateD);

                        grau = marker.getInt("report_level");

                        String state = marker.getString("report_state");

                        type = marker.getString("report_category");

                        info = new InfoMarker(id, type, desc, state, 0, dates, lat, lng, grau);
                        makeMarker(info);

                        String newCursor = reports.getString("cursor");

                        if (!cursor.equals(newCursor)) {
                            cursor = newCursor;
                            mListReportsTask = new AttemptListReports(user, tokenId, cursor);
                            mListReportsTask.execute((Void) null);
                        }

                    }
                } catch (Exception e) {
                    Log.v("MARKER", result);
                }
            }
        }

        @Override
        protected void onCancelled() {

        }

    }
}
