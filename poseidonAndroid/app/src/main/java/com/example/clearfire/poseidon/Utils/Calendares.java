package com.example.clearfire.poseidon.Utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
;
import android.widget.ImageView;


import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.exceptions.OutOfDateRangeException;
import com.example.clearfire.poseidon.Event.AddEvent;
import com.example.clearfire.poseidon.Event.Event;
import com.example.clearfire.poseidon.R;

import java.util.Calendar;
import java.util.List;

/**
 * Created by henry on 07-06-2018.
 */


public class Calendares extends Dialog {

    private ImageView mcheck;
    private CalendarView calendarView;

    public static final String PREFS_NAME = "AUTH";

    public AddEvent c;

    private String initDate;
    private String finalDate;

    public Calendares(AddEvent a) {
        super(a);
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choosedate);

        mcheck = (ImageView) findViewById(R.id.checkDate);
        mcheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Calendar> dates = calendarView.getSelectedDates();
                c.setFinale(finalDate = dates.get(dates.size() - 1).getTime().toString());
                c.setInit(initDate = dates.get(0).getTime().toString());
                dismiss();

            }
        });

        Calendar today = Calendar.getInstance();
        Calendar min = Calendar.getInstance();
        calendarView = (CalendarView) findViewById(R.id.calendarView);
        try {
            min.add(Calendar.DATE, -1);
            calendarView.setMinimumDate(min);
            calendarView.setDate(today);

        } catch (OutOfDateRangeException e) {
            e.printStackTrace();
        }

    }

}
