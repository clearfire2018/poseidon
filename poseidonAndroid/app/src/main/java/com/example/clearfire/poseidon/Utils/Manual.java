package com.example.clearfire.poseidon.Utils;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.clearfire.poseidon.R;

/**
 * Created by henry on 03-05-2018.
 */

public class Manual extends AppCompatActivity {

    private CardView mCard;
    private CardView mCardi;
    private Button mFire;
    private Button mPrevent;
    private ImageView mActiveFire;
    private ImageView mBeforeFire;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.emergencia);

        mFire = (Button) findViewById(R.id.emergenci);
        mPrevent = (Button) findViewById(R.id.prevencion);

        mCard = (CardView) findViewById(R.id.emer);
        mCardi = (CardView) findViewById(R.id.prev);

        final ConstraintLayout fire = (ConstraintLayout) findViewById(R.id.manuall);
        final View child = getLayoutInflater().inflate(R.layout.activity_manual, null);

        mFire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fire.addView(child);
                mBeforeFire = (ImageView) findViewById(R.id.beforeFire);
                mBeforeFire.setVisibility(View.GONE);
                mActiveFire = (ImageView) findViewById(R.id.activeFire);
                mActiveFire.setVisibility(View.VISIBLE);
                mCard.setVisibility(View.GONE);
                mCardi.setVisibility(View.GONE);
            }
        });

        mPrevent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fire.addView(child);
                mActiveFire = (ImageView) findViewById(R.id.activeFire);
                mActiveFire.setVisibility(View.GONE);
                mBeforeFire = (ImageView) findViewById(R.id.beforeFire);
                mBeforeFire.setVisibility(View.VISIBLE);
                mCard.setVisibility(View.GONE);
                mCardi.setVisibility(View.GONE);
            }
        });
    }

}
