package com.example.clearfire.poseidon.Utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by henry on 18-04-2018.
 */

public class Requests {

    private static final String TAG = "My Activity";

    //Post para objetos jason
    public static String doPost(URL url, JSONObject data) throws IOException {
        InputStream stream = null;
        OutputStream out = null;
        HttpURLConnection connection = null;
        String result = null;

        try {
            connection = (HttpURLConnection) url.openConnection();

            //Timeout for reading the input stream
            connection.setReadTimeout(10000);
            //Timeout for connection.connect()
            connection.setConnectTimeout(10000);

            connection.setRequestMethod("POST");
            //Already true by default but setting just in case; need to be true since this request
            //is carrying an input (response) body
            connection.setDoInput(true);
            connection.setChunkedStreamingMode(0);

            connection.setRequestProperty("Accept-Charset", "UTF-8");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Content-type", "application/json");

            //Open a communications link
            out = new BufferedOutputStream(connection.getOutputStream());
            out.write(data.toString().getBytes());
            out.flush();
            int responseCode = connection.getResponseCode();
            Log.v(TAG, "response:   " + responseCode);
            if (responseCode != HttpURLConnection.HTTP_OK) {
                throw new IOException("HTTP error code: " + responseCode);
            }

            //Retrieve the response body as an InputStream
            stream = connection.getInputStream();
            if (stream != null) {
                result = readStream(stream, 1024);

            }
        } finally {
            //Close streams and disconnect HTTP connection
            if (out != null) out.close();
            if (stream != null) stream.close();
            if (connection != null) connection.disconnect();
        }

        return result;
    }

    //Post para a cloud de imagens
    public static String doPostImage(URL url, Bitmap image) throws IOException {
        InputStream stream = null;
        OutputStream out = null;
        HttpURLConnection connection = null;
        String result = null;

        try {
            connection = (HttpURLConnection) url.openConnection();

            //Timeout for reading the input stream
            connection.setReadTimeout(10000);
            //Timeout for connection.connect()
            connection.setConnectTimeout(10000);

            connection.setRequestMethod("POST");
            //Already true by default but setting just in case; need to be true since this request
            //is carrying an input (response) body
            connection.setDoInput(true);
            connection.setChunkedStreamingMode(0);

            connection.setRequestProperty("Accept-Charset", "UTF-8");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Content-type", "image/jpeg");

            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.JPEG, 100, outStream);

            //Open a communications link
            out = new BufferedOutputStream(connection.getOutputStream());
            out.write(outStream.toByteArray());
            out.flush();
            int responseCode = connection.getResponseCode();
            Log.v(TAG, "response:   " + responseCode);
            if (responseCode != HttpURLConnection.HTTP_OK) {
                throw new IOException("HTTP error code: " + responseCode);
            }

            //Retrieve the response body as an InputStream
            stream = connection.getInputStream();
            if (stream != null) {
                result = readStream(stream, 1024);

            }
        } finally {
            //Close streams and disconnect HTTP connection
            if (out != null) out.close();
            if (stream != null) stream.close();
            if (connection != null) connection.disconnect();
        }

        return result;
    }

    //Get para obter imagem pretendida
    public static Bitmap doGetImage(URL url) throws IOException {
        InputStream stream = null;
        HttpURLConnection connection = null;
        Bitmap result = null;


        try {
            connection = (HttpURLConnection) url.openConnection();

            //Timeout for reading the input stream
            connection.setReadTimeout(10000);
            //Timeout for connection.connect()
            connection.setConnectTimeout(10000);

            connection.setRequestMethod("GET");
            //Already true by default but setting just in case; need to be true since this request
            //is carrying an input (response) body
            connection.setDoInput(true);
            connection.setChunkedStreamingMode(0);

            connection.setRequestProperty("Accept-Charset", "UTF-8");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Content-type", "image/jpeg");

            int responseCode = connection.getResponseCode();
            Log.v(TAG, "response:   " + responseCode);
            if (responseCode != HttpURLConnection.HTTP_OK) {
                throw new IOException("HTTP error code: " + responseCode);
            }

            //Retrieve the response body as an InputStream
            stream = connection.getInputStream();
            if (stream != null) {
                result = BitmapFactory.decodeStream(stream);
            }


        } finally {
            //Close streams and disconnect HTTP connection
            if (stream != null) stream.close();
            if (connection != null) connection.disconnect();
        }

        return result;
    }

    //Get de objetos jason
    public static String doGet(URL url) throws IOException {
        InputStream stream = null;
        HttpURLConnection connection = null;
        String result = null;

        try {
            connection = (HttpURLConnection) url.openConnection();

            //Timeout for reading the input stream
            connection.setReadTimeout(10000);
            //Timeout for connection.connect()
            connection.setConnectTimeout(10000);

            connection.setRequestMethod("GET");
            //Already true by default but setting just in case; need to be true since this request
            //is carrying an input (response) body
            connection.setDoInput(true);
            connection.setChunkedStreamingMode(0);

            connection.setRequestProperty("Accept-Charset", "UTF-8");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Content-type", "application/json");

            //Open a communications link
            int responseCode = connection.getResponseCode();
            Log.v(TAG, "response:   " + responseCode);
            if (responseCode != HttpURLConnection.HTTP_OK) {
                throw new IOException("HTTP error code: " + responseCode);
            }

            //Retrieve the response body as an InputStream
            stream = connection.getInputStream();
            if (stream != null) {
                result = readStream(stream, 1024);

            }
        } finally {
            //Close streams and disconnect HTTP connection
            if (stream != null) stream.close();
            if (connection != null) connection.disconnect();
        }

        return result;
    }

    //DELETE de um objeto jason
    public static String doDelete(URL url, JSONObject data) throws IOException {
        OutputStream out = null;
        InputStream stream = null;
        HttpURLConnection connection = null;
        String result = null;

        try {
            connection = (HttpURLConnection) url.openConnection();

            //Timeout for reading the input stream
            connection.setReadTimeout(10000);
            //Timeout for connection.connect()
            connection.setConnectTimeout(10000);

            connection.setRequestMethod("DELETE");
            //Already true by default but setting just in case; need to be true since this request
            //is carrying an input (response) body
            connection.setDoInput(true);
            connection.setChunkedStreamingMode(0);

            connection.setRequestProperty("Accept-Charset", "UTF-8");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Content-type", "application/json");

            //Open a communications link
            //Open a communications link
            out = new BufferedOutputStream(connection.getOutputStream());
            out.write(data.toString().getBytes());
            out.flush();

            int responseCode = connection.getResponseCode();
            Log.v(TAG, "response:   " + responseCode);
            if (responseCode != HttpURLConnection.HTTP_OK) {
                throw new IOException("HTTP error code: " + responseCode);
            }

            stream = connection.getInputStream();
            if (stream != null) {
                result = readStream(stream, 1024);
            }


        } finally {
            //Close streams and disconnect HTTP connection
            if (out != null) out.close();
            if (stream != null) stream.close();
            if (connection != null) connection.disconnect();
        }

        return result;
    }

    //Metodo para ler o input de uma stream vinda do server
    public static String readStream(InputStream stream, int bytes) {
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder(bytes);

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(stream));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }

}
