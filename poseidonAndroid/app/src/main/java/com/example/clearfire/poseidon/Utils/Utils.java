package com.example.clearfire.poseidon.Utils;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolygonOptions;

public class Utils {
    public static PolygonOptions getPortugalPoly() {
        return new PolygonOptions()
                .add(new LatLng(41.879992, -8.889210),
                        new LatLng(42.127485, -8.551205),
                        new LatLng(42.182604, -8.197852),
                        new LatLng(42.045232, -8.04147),
                        new LatLng(42.016100, -8.067399),
                        new LatLng(41.906787, -8.180491),
                        new LatLng(41.828041, -8.049341),
                        new LatLng(41.945534, -7.898468),
                        new LatLng(42.014734, -6.595232),
                        new LatLng(41.635037, -6.167353),
                        new LatLng(41.544650, -6.164086),
                        new LatLng(40.975019, -6.846734),
                        new LatLng(40.338262, -6.721290),
                        new LatLng(40.132553, -6.909100),
                        new LatLng(40.051344, -6.814379),
                        new LatLng(39.642592, -7.008721),
                        new LatLng(39.624984, -7.438233),
                        new LatLng(39.040151, -6.891620),
                        new LatLng(38.474714, -7.224778),
                        new LatLng(38.223684, -7.074530),
                        new LatLng(38.254469, -6.898152),
                        new LatLng(38.002675, -6.970010),
                        new LatLng(37.956333, -7.198648),
                        new LatLng(37.517216, -7.446883),
                        new LatLng(37.169256, -7.361960),
                        new LatLng(36.934648, -7.891094),
                        new LatLng(37.039008, -8.341837),
                        new LatLng(37.065075, -8.616202),
                        new LatLng(36.955531, -8.942828),
                        new LatLng(37.049436, -9.053880),
                        new LatLng(37.532759, -8.838308),
                        new LatLng(37.940879, -8.910165),
                        new LatLng(38.326250, -8.838308),
                        new LatLng(38.387720, -9.262921),
                        new LatLng(38.627976, -9.262921),
                        new LatLng(38.689190, -9.596079),
                        new LatLng(39.424709, -9.393571),
                        new LatLng(40.042612, -9.014685),
                        new LatLng(40.981250, -8.714190),
                        new LatLng(41.755826, -8.923230),
                        new LatLng(41.879992, -8.889210)


                );
    }

}
