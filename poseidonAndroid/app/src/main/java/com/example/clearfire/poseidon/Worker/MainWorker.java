package com.example.clearfire.poseidon.Worker;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;


import com.example.clearfire.poseidon.MainActivity;
import com.example.clearfire.poseidon.Marker.InfoMarker;
import com.example.clearfire.poseidon.R;
import com.example.clearfire.poseidon.Utils.Requests;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by henry on 07-07-2018.
 */

public class MainWorker extends AppCompatActivity {

    private ListView mShowAll;

    private WorkerAdapter mWorkerAdapter;
    List<InfoMarker> lisUnsolvedMarker = new ArrayList<>();

    private InfoMarker info;
    private listUnsolved listUnsolvedTask = null;
    private SharedPreferences settings;

    private String logoutUrl = "https://henrique-45707.appspot.com/rest/login/logout";
    private AttemptLogout mLogoutTask;

    private String baseUrl = "https://henrique-45707.appspot.com/rest/reports/listUnsolvedState";
    public static final String PREFS_NAME = "AUTH";

    private String user;
    private String tokenId;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_worker);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);

        mShowAll = findViewById(R.id.showallunsolved);

        settings = getSharedPreferences(PREFS_NAME, 0);
        user = settings.getString("username", "");
        tokenId = settings.getString("tokenID", "");

        listUnsolvedTask = new listUnsolved("");
        listUnsolvedTask.execute((Void) null);

        ImageView show = findViewById(R.id.showMap);
        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainWorker.this, Map_unsolved.class);
                startActivity(intent);
            }
        });

        // Save the ListView state (= includes scroll position) as a Parceble
        Parcelable state = mShowAll.onSaveInstanceState();
        mShowAll.onRestoreInstanceState(state);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_simples, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.Logout:
                attemptLogout();

            default:
                return super.onOptionsItemSelected(item);
        }
    }
                /**
                 * Represents an asynchronous list events task used to show all
                 * the events to the user
                 */
    public class listUnsolved extends AsyncTask<Void, Void, String> {

        String cursor;
        String id;
        String desc;
        int grau;
        String type;
        Calendar dates;

        private static final String PROPERTYMAP = "propertyMap";

        listUnsolved(String cursor) {
            this.cursor = cursor;
        }


        //Cancel background network operation if we do not have network connectivity
        @Override
        protected void onPreExecute() {

            ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = conMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected()
                    || (networkInfo.getType() != ConnectivityManager.TYPE_WIFI &&
                    networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);
            }
        }

        @Override
        protected String doInBackground(Void... params) {

            try {

                //constroi objecto jason com credenciais e efetua pedido
                JSONObject credentialsListEvent = new JSONObject();
                credentialsListEvent.accumulate("username", user);
                credentialsListEvent.accumulate("tokenID", tokenId);
                return Requests.doPost(new URL(baseUrl  + "?cursor=" + cursor), credentialsListEvent);


            } catch (Exception e) {
                return null;
            }

        }

        @Override
        protected void onPostExecute(final String result) {
            if (result != null) {
                SharedPreferences.Editor editor = settings.edit();

                if (result != null) {

                    JSONArray data = null;
                    double lat;
                    double lng;

                    JSONObject marker;

                    try {

                        JSONObject report = new JSONObject(result);

                        data = report.getJSONArray("reports");
                        for (int i = 0; i < data.length(); i++) {
                            marker = data.getJSONObject(i);

                            id = marker.getJSONObject("key").getString("name");
                            marker = marker.getJSONObject(PROPERTYMAP);

                            lat = marker.getDouble("report_latitude");
                            lng = marker.getDouble("report_longitude");

                            desc = marker.getString("report_description");

                            grau = marker.getInt("report_level");

                            String state = marker.getString("report_state");

                            String date = marker.getString("report_date");

                            Long dateLong = Long.parseLong(date);
                            Date dateD = new Date(dateLong);

                            dates = Calendar.getInstance();
                            dates.setTime(dateD);

                            type = marker.getString("report_category");

                            info = new InfoMarker(id, type, desc, state, 0, dates, lat, lng, grau);
                            lisUnsolvedMarker.add(info);

                            String newCursor = report.getString("cursor");

                            if (!cursor.equals(newCursor)) {
                                cursor = newCursor;
                                listUnsolvedTask = new listUnsolved(cursor);
                                listUnsolvedTask.execute((Void) null);
                            }
                        }
                        Log.v("olaaaa   ", result);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    startAdapter();
                }

            }
        }

        @Override
        protected void onCancelled() {
        }
    }

    private void startAdapter() {

        mWorkerAdapter = new WorkerAdapter(this, R.layout.show_unsolved, lisUnsolvedMarker);
        mShowAll.setAdapter(mWorkerAdapter);

    }

    private void attemptLogout() {

        mLogoutTask = new AttemptLogout(user, tokenId);
        mLogoutTask.execute((Void) null);
    }

    /**
     * Represents an asynchronous logout task used to logout
     * the user.
     */
    public class AttemptLogout extends AsyncTask<Void, Void, String> {

        AttemptLogout(String username, String token) {
            user = username;
            tokenId = token;
        }


        //Cancel background network operation if we do not have network connectivity
        @Override
        protected void onPreExecute() {
            ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = conMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected()
                    || (networkInfo.getType() != ConnectivityManager.TYPE_WIFI &&
                    networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                cancel(true);
            }
        }

        @Override
        protected String doInBackground(Void... params) {

            try {
                //constroi objecto jason com credenciais e efetua pedido
                JSONObject credentialsLogout = new JSONObject();
                credentialsLogout.accumulate("username", user);
                credentialsLogout.accumulate("tokenID", tokenId);

                return Requests.doDelete(new URL(logoutUrl), credentialsLogout);


            } catch (Exception e) {
                return null;
            }

        }

        @Override
        protected void onPostExecute(final String result) {
            mLogoutTask = null;
            if (result != null) {
                SharedPreferences.Editor editor = settings.edit();
                editor.remove("username");
                editor.remove("tokenID");
                editor.commit();
                Intent intent = new Intent(MainWorker.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }

        @Override
        protected void onCancelled() {

        }

    }
}


