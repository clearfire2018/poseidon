package com.example.clearfire.poseidon.Worker;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.clearfire.poseidon.Marker.InfoMarker;
import com.example.clearfire.poseidon.R;
import com.example.clearfire.poseidon.Utils.Requests;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import org.json.JSONObject;

import java.net.URL;
import java.util.List;

/**
 * Created by henry on 07-07-2018.
 */

public class WorkerAdapter extends ArrayAdapter<InfoMarker> {

    private Context context;
    private String baseUrlChangeState = "https://henrique-45707.appspot.com/rest/reports/";
    public static final String PREFS_NAME = "AUTH";
    private SharedPreferences settings;

    private String user;
    private String tokenId;


    private ChangeState mChangeStateTask;

    public WorkerAdapter(Context context, int resource, List<InfoMarker> objects) {
        super(context, resource, objects);
        this.context = context;
        settings = context.getSharedPreferences(PREFS_NAME, 0);
        user = settings.getString("username", "");
        tokenId = settings.getString("tokenID", "");
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = ((Activity) getContext()).getLayoutInflater().inflate(R.layout.show_unsolved, parent, false);
        }

        final InfoMarker marker = getItem(position);

        TextView mTitle = convertView.findViewById(R.id.tit);
        TextView mGravidade = convertView.findViewById(R.id.gra);

        mTitle.setText(marker.getType());
        mGravidade.setText(marker.getGrau() + "");

        ExpandableTextView mDesc = convertView.findViewById(R.id.showUnsolvedDesc);
        mDesc.setText(marker.getDescription());
        mDesc.setOnExpandStateChangeListener(new ExpandableTextView.OnExpandStateChangeListener() {
            @Override
            public void onExpandStateChanged(TextView textView, boolean isExpanded) {

            }
        });

        final String ides = marker.getId().toString();
        final String[] state = new String[1];

       final TextView mResult = convertView.findViewById(R.id.grav);

       if(marker.getState().equals("0"))
           state[0] = "Não Resolvido";
       else if(marker.getState().equals("1")){
           state[0] = "Em Resolução";
       } else {
           state[0] = "Resolvido";
       }
        mResult.setText(state[0]);

        ImageView seta = convertView.findViewById(R.id.seta);

        seta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.seta_states);
                final RadioButton naoResol = dialog.findViewById(R.id.naoResol);
                final RadioButton emResol = dialog.findViewById(R.id.emResol);
                final RadioButton resol = dialog.findViewById(R.id.Resol);
                final String[] choosen = new String[1];

                if(marker.getState().equals("0")){
                    naoResol.setChecked(true);
                    emResol.setChecked(false);
                    resol.setChecked(false);
                    choosen[0] = "0";

                } else if (marker.getState().equals("1")){
                    emResol.setChecked(true);
                    naoResol.setChecked(false);
                    resol.setChecked(false);
                    choosen[0] = "1";

                } else{
                    choosen[0] = "2";
                }


                naoResol.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        naoResol.setChecked(true);
                        emResol.setChecked(false);
                        resol.setChecked(false);
                        choosen[0] = "0";
                    }
                });

                emResol.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        emResol.setChecked(true);
                        naoResol.setChecked(false);
                        resol.setChecked(false);
                        choosen[0] = "1";
                    }
                });

                resol.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        resol.setChecked(true);
                        naoResol.setChecked(false);
                        emResol.setChecked(false);
                        choosen[0] = "2";
                    }
                });

                FloatingActionButton check = dialog.findViewById(R.id.check_change);
                check.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(choosen[0].equals("0")){
                            mResult.setText("Não Resolvido");
                        } else if(choosen[0].equals("1")){
                            mResult.setText("Em Resolução");
                        } else{
                            mResult.setText("Resolvido");
                        }
                        mChangeStateTask = new ChangeState(ides,choosen[0]);
                        mChangeStateTask.execute((Void) null);
                        dialog.dismiss();

                    }
                });
                dialog.show();
            }
        });
        return convertView;
    }


    /**
     * Represents an asynchronous to like a event
     */
    public class ChangeState extends AsyncTask<Void, Void, String> {


        String id;
        String state;

        ChangeState(String id, String state) {
           this.id = id;
           this.state = state;
        }

        //Cancel background network operation if we do not have network connectivity
        @Override
        protected void onPreExecute() {

            ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Activity.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = conMgr.getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnected()
                    || (networkInfo.getType() != ConnectivityManager.TYPE_WIFI &&
                    networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {
                Toast.makeText(context, "Sem conexão à Internet", Toast.LENGTH_SHORT).show();
                cancel(true);
            }
        }

        @Override
        protected String doInBackground(Void... params) {

            try {

                //constroi objecto jason com credenciais e efetua pedido
                JSONObject credentialsListEvent = new JSONObject();
                credentialsListEvent.accumulate("username", user);
                credentialsListEvent.accumulate("tokenID", tokenId);
                Log.v("iowehi", " " + baseUrlChangeState + id + "/change-state/" + state);
                return Requests.doPost(new URL(baseUrlChangeState + id + "/change-state/" + state), credentialsListEvent);


            } catch (Exception e) {
                return null;
            }

        }

        @Override
        protected void onPostExecute(final String result) {
            if (result != null) {
                Toast.makeText(context, "Estado mudado com sucesso", Toast.LENGTH_SHORT).show();

            }
        }

        @Override
        protected void onCancelled() {
        }
    }


}
